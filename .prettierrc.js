/**
 * @type {import('prettier').Config}
 */
const config = {
	semi: false,
	tabWidth: 3,
	useTabs: true,
	printWidth: 180,
	singleQuote: true,
	arrowParens: 'avoid',
	jsxSingleQuote: true,
	plugins: [require.resolve('prettier-plugin-sort-imports'), require.resolve('prettier-plugin-js-sort')],
	sortingMethod: 'lineLength',
	newlineBetweenTypes: true,
	stripNewlines: true,
	importTypeOrder: ['NPMPackages', 'localImports'],
}

module.exports = config
