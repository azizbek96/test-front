const { src, dest, task, parallel, series } = require('gulp')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { ProvidePlugin, ProgressPlugin, HotModuleReplacementPlugin } = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpackStream = require('webpack-stream')
const DotEnv = require('dotenv-webpack')
const gulpIf = require('gulp-if')
const path = require('path')
const del = require('del')
const url = require('url')
const fs = require('fs')
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')
const browserSync = require('browser-sync')
const glob = require('glob')
const DevServer = require('webpack-dev-server')

const __taskname = process.title.split(' ')[1] ?? 'default'

const paths = {
	src: {
		fonts: [],
		images: [],
		js: './src/index.js',
		scss: ['./src/assets/scss/theme.scss'],
	},
	dist: {
		js: './dist/js',
		scss: './dist/css',
		fonts: './dist/fonts',
		images: './dist/images',
	},
	watch: {
		js: './src/**/*.{js,jsx,ts,tsx}',
		fonts: './src/assets/fonts/**/*.*',
		scss: './src/assets/scss/**/*.scss',
		images: './src/assets/images/**/*.*',
	},
}

const getEntries = (globs, isReadable = false, regexp, filter = () => true) =>
	new Promise(resolve => {
		/**
		 * @type {{file:string,path:string}[]}
		 */
		const entries = []
		let globsArr = []

		if (Array.isArray(globs)) {
			globsArr = globs.map(g => glob.sync(g)).flat()
		} else {
			globsArr = glob.sync(globs)
		}

		const files = [...new Set(globsArr)]

		files.forEach(file => {
			if (isReadable) {
				const data = fs.readFileSync(file, 'utf8') ?? ''
				const match = data.replace(/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))/gi, '').match(regexp) ?? []

				entries.push(
					...match.map(a => {
						return { file, path: a }
					})
				)
			} else {
				entries.push({ file, path: file })
			}
		})

		resolve(
			entries
				.map(({ path: str, file }) => {
					if (isReadable) {
						const strArr = str
							.split("'")
							.filter(a => /^.+(\/).+$/i.test(a))
							.map(a => {
								return a.match(/^[^?^#]+/g)[0]
							})

						str = strArr.map(url => {
							if (/^\.\.?\/.+$/.test(url)) {
								return path.join(path.dirname(file), url)
							}

							let packagePath = ''

							try {
								packagePath = require.resolve(url)
							} catch (error) {
								return ''
							}

							return path.relative(__dirname, packagePath)
						})
					}

					return str
				})
				.flat()
				.filter(filter)
		)
	})

getEntries(paths.watch.js, true, /import\s?.+('.+')$/gim).then(v => {
	paths.src.scss.push(...v.filter(a => /\.s?css$/.test(a)))
	paths.src.images.push(...v.filter(a => /\.(svg|jpg|png)$/.test(a)))
})

getEntries(paths.watch.scss, true, /url\('(\..+)'\)/gi, f => !/^.+fonts\//.test(f) && /\.(svg|png|jpg)$/.test(f)).then(v => {
	paths.src.images.push(...v)
})

getEntries(paths.watch.scss, true, /url\('(.+)'\)/gi, f => /^.+fonts\/.+$/gi.test(f)).then(v => {
	paths.src.fonts.push(...v)
})

const isDevTask = __taskname === 'dev'
const hasScss = fs.existsSync(paths.dist.scss)
const hasFonts = fs.existsSync(paths.dist.fonts)
const hasImages = fs.existsSync(paths.dist.images)

const server = browserSync.create()

/**
 * @type {import('webpack').Configuration}
 */
const webpackScssConf = {
	mode: 'development',
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /.s?css$/,
				use: [MiniCssExtractPlugin.loader, { loader: 'css-loader', options: { url: false, sourceMap: false } }, 'resolve-url-loader', 'sass-loader'],
			},
			{
				test: /\.(png|svg|jpeg|jpg|eot|woff|woff2|ttf)$/,
				type: 'asset/resource',
				generator: {
					filename: '[name][ext]',
				},
			},
		],
	},
	// prettier-ignore
	plugins: [
		new webpackStream.webpack.ProgressPlugin({ profile: true }),
		new MiniCssExtractPlugin({ filename: 'style.css' })
	],
}

const scss = () => {
	return src(paths.src.scss).pipe(webpackStream(webpackScssConf)).pipe(dest(paths.dist.scss)).pipe(gulpIf(isDevTask, server.stream()))
}

const scssTask = series(
	() => del(paths.dist.scss),
	scss,
	() => del(['./dist/css/main.js', './dist/css/main.js.map'])
)

task('scss', scssTask)

// default

const jsClean = () => {
	return del(paths.dist.js)
}

/**
 * @type {import('webpack').Configuration}
 */
const jsConfig = {
	mode: 'development',
	devtool: 'source-map',
	watch: isDevTask || process.argv.includes('--watch'),
	module: {
		rules: [
			{
				exclude: /node_modules/,
				test: /\.(ts|tsx|js|jsx)$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-react', '@babel/preset-typescript'],
					},
				},
			},
			{
				type: 'asset/inline',
				test: /\.(png|svg|jpeg|jpg)$/,
				generator: {
					dataUrl: function () {
						return '/images/' + path.parse(arguments[1].filename).base
					},
				},
			},
			{
				type: 'asset/inline',
				test: /fonts.+\.(eot|woff|woff2|ttf|svg)$/,
				generator: {
					dataUrl: function () {
						return '/fonts/' + path.parse(arguments[1].filename).base
					},
				},
			},
			{
				test: /\.s?css$/,
				type: 'asset/inline',
				generator: {
					dataUrl: function () {
						return ''
					},
				},
			},
		],
	},
	resolve: {
		mainFiles: ['index'],
		enforceExtension: false,
		extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
	},
	output: { publicPath: '/js/', filename: 'index.js' },
	plugins: [
		new DotEnv({ path: './.env' }),
		new ProgressPlugin({ profile: true }),
		new HtmlWebpackPlugin({ template: './public/index.html', inject: 'body', filename: '../index.html' }),
		new ProvidePlugin({
			React: 'react',
		}),
	].filter(Boolean),
	optimization: {
		splitChunks: {
			minSize: 0,
			chunks: 'all',
			usedExports: true,
			maxInitialRequests: Infinity,
			filename: '[name].[contenthash:5].js',
			cacheGroups: {
				react: {
					priority: 100,
					name: 'react',
					test: /node_modules\/(react-dom|react)\//,
				},
				reactRouter: {
					priority: 99,
					name: 'react-router',
					test: /node_modules\/(react-router|react-router-dom)\//,
				},
				reduxSaga: {
					priority: 98,
					name: 'redux-saga',
					test: /node_modules\/@redux-saga\//,
				},
				reactstrap: {
					priority: 97,
					name: 'reactstrap',
					test: /node_modules\/reactstrap\//,
				},
				reactBootstrapTable: {
					priority: 96,
					name: 'react-bootstrap-table',
					test: /node_modules\/react-bootstrap-table(-next|2-editor|2-overlay|2-paginator|2-toolkit)\//,
				},
				lodash: {
					priority: 95,
					name: 'lodash',
					test: /node_modules\/lodash\//,
				},
				dateFns: {
					priority: 94,
					name: 'date-fns',
					test: /node_modules\/date-fns\//,
				},
				moment: {
					priority: 93,
					name: 'moment',
					test: /node_modules\/moment\//,
				},
				framerMotion: {
					priority: 92,
					name: 'framer-motion',
					test: /node_modules\/framer-motion\//,
				},
				src: {
					priority: 91,
					name: 'source',
					test: /src/,
				},
				reactPackages: {
					priority: 90,
					test: /node_modules\/react-/,
					name: 'react-packages',
				},
				default: {
					name: 'chunk',
					test: /node_modules/,
					reuseExistingChunk: true,
				},
			},
		},
	},
}

const jsSrc = () => {
	return src(paths.src.js).pipe(webpackStream(jsConfig)).pipe(dest(paths.dist.js)).pipe(gulpIf(isDevTask, server.stream()))
}

const jsTask = series(jsClean, jsSrc)

task('js', jsTask)

// images

const imagesClean = () => {
	return del(paths.dist.images)
}

const imagesSrc = async () => {
	return src(paths.src.images).pipe(dest(paths.dist.images))
}

const imagesTask = series(imagesClean, imagesSrc)

task('images', imagesTask)

// fonts

const fontsClean = () => {
	return del(paths.dist.fonts)
}

const fontsSrc = () => {
	return src(paths.src.fonts).pipe(dest(paths.dist.fonts))
}

const fontsTask = series(fontsClean, fontsSrc)

task('fonts', fontsTask)

// server

const serverSrc = () => {
	return server.init({
		port: 3333,
		open: false,
		server: {
			baseDir: './dist',
			middleware: [
				function (req, res, next) {
					const parsed = url.parse(req.url)
					const fileName = parsed.href.split(parsed.search).join('')
					const fileExists = fs.existsSync(path.join('dist', fileName))

					if (!fileExists && fileName.indexOf('browser-sync-client') < 0) {
						req.url = '/index.html'
					}
					return next()
				},
			],
		},
	})
}

task('serve', serverSrc)

// dev

const devTask = parallel(jsTask, ...[!hasScss && scssTask, !hasFonts && fontsTask, !hasImages && imagesTask].filter(Boolean), serverSrc)

task('dev', devTask)
