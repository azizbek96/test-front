// import React from "react"

import {
	chatData,
	decEarningData,
	janEarningData,
	novEarningData,
	octEarningData,
	decTopSellingData,
	janTopSellingData,
	novTopSellingData,
	octTopSellingData,
} from './dashboard-saas'
import { shops, orders, cartData, comments, customerData, discountData, productsData, recentProducts, productComments } from './ecommerce'
import { sentmails, draftmails, inboxmails, trashmails, starredmails, importantmails } from './mails'
import { events, calenderDefaultCategories } from './calender'
import { weekData, yearData, monthData } from './dashboard'
import { chats, groups, contacts, messages } from './chat'
import { users, userProfile } from './contacts'
import { wallet, cryptoOrders } from './crypto'
import { invoiceList } from './invoices'
import { projects } from './projects'
import { tasks } from './tasks'

export {
	chats,
	shops,
	tasks,
	users,
	events,
	groups,
	orders,
	wallet,
	contacts,
	messages,
	cartData,
	comments,
	projects,
	yearData,
	weekData,
	chatData,
	sentmails,
	monthData,
	inboxmails,
	draftmails,
	trashmails,
	invoiceList,
	userProfile,
	productsData,
	discountData,
	customerData,
	starredmails,
	cryptoOrders,
	recentProducts,
	importantmails,
	janEarningData,
	decEarningData,
	novEarningData,
	octEarningData,
	productComments,
	janTopSellingData,
	decTopSellingData,
	novTopSellingData,
	octTopSellingData,
	calenderDefaultCategories,
}
