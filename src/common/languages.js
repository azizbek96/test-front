import russia from '../assets/images/flags/ru.png'
import usFlag from '../assets/images/flags/us.png'
import uzb from '../assets/images/flags/uz.png'

const languages = {
	uz: {
		label: 'Uzbek',
		flag: uzb,
	},
	rs: {
		label: 'Russian',
		flag: russia,
	},
	en: {
		label: 'English',
		flag: usFlag,
	},
}

export default languages
