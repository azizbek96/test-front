import HighLighter from 'react-highlight-words'
import AsyncSelect from 'react-select/async'
import { components } from 'react-select'
import React, { useState } from 'react'

const AsyncSearch = ({ name, ...props }) => {
	const [searchText, setSearchText] = useState('')

	return (
		<>
			<AsyncSelect
				{...props}
				name={name}
				isSearchable
				cacheOptions
				menuPosition='fixed'
				menuPlacement='auto'
				inputValue={searchText}
				closeMenuOnSelect={false}
				classNamePrefix='select2-selection'
				onInputChange={searchValue => setSearchText(searchValue)}
				components={{
					Option: props => {
						return (
							<components.Option {...props}>
								<HighLighter searchWords={[searchText]} textToHighlight={props.label} highlightClassName='select-highlight' />
							</components.Option>
						)
					},
				}}
			/>
		</>
	)
}

export default AsyncSearch
