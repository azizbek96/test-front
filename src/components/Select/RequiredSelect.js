import PropTypes from 'prop-types'
import Select from 'react-select'
import React from 'react'

import attentionSvg from '../../assets/images/attention.svg'

const getInvalidStyles = invalid =>
	invalid
		? {
				control: (base, { isFocused }) => ({
					...base,
					borderColor: '#f46a6a !important',
					boxShadow: isFocused && '0 0 0 0.15rem rgb(244 106 106 / 25%) !important',
				}),
				indicatorSeparator: base => ({
					...base,
					backgroundColor: '#f46a6a !important',
				}),
				menuPortal: base => ({ ...base, zIndex: 99999 }),
				dropdownIndicator: base => ({
					...base,
					'svg path': {
						display: 'none',
					},
					svg: {
						backgroundSize: '17px',
						backgroundPosition: 'center',
						backgroundRepeat: 'no-repeat',
						backgroundImage: `url(${attentionSvg})`,
					},
				}),
		  }
		: {
				dropdownIndicator: base => ({
					...base,
					'&:hover': {
						color: '#999999',
						cursor: 'pointer',
					},
				}),
				control: base => ({ ...base, cursor: 'pointer' }),
				menuPortal: base => {
					return { ...base, zIndex: 99999 }
				},
		  }

const RequiredSelect = ({ id, invalid = false, errorMessage, ...props }) => {
	return (
		<>
			<Select
				{...props}
				menuPosition='fixed'
				menuPlacement='auto'
				styles={getInvalidStyles(invalid)}
				classNamePrefix='select2-selection'
				menuPortalTarget={document.querySelector('.modal-body')}
			/>
			{invalid && <div className='custom-form-feedback'>{errorMessage}</div>}
		</>
	)
}

RequiredSelect.propTypes = {
	id: PropTypes.any,
	invalid: PropTypes.bool,
	options: PropTypes.array,
	errorMessage: PropTypes.string,
}

export default RequiredSelect
