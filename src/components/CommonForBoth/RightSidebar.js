import { Col, Row } from 'reactstrap'
import { Link } from 'react-router-dom'
//SimpleBar
import SimpleBar from 'simplebar-react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'

import {
	changeLayout,
	changePreloader,
	changeLayoutWidth,
	changeSidebarType,
	changeTopbarTheme,
	changeSidebarTheme,
	showRightSidebarAction,
	changeSidebarThemeImage,
} from '../../store/actions'
//constants
import { layoutTypes, layoutWidthTypes, leftSidebarTypes, topBarThemeTypes, leftSideBarThemeTypes } from '../../constants/layout'


//Import images
import bgimg1 from '../../assets/images/sidebar/img1.jpg'
import bgimg2 from '../../assets/images/sidebar/img2.jpg'
import bgimg3 from '../../assets/images/sidebar/img3.jpg'
import bgimg4 from '../../assets/images/sidebar/img4.jpg'

const RightSidebar = props => {
	const onCloseRightBar = () => {
		const { onClose } = props
		if (onClose) {
			onClose()
		}
	}

	return (
		<>
			<div id='right-bar' className='right-bar'>
				<SimpleBar style={{ height: '900px' }}>
					<div data-simplebar className='h-100'>
						<div className='rightbar-title px-3 py-4'>
							<Link
								to='#'
								className='right-bar-toggle float-end'
								onClick={e => {
									e.preventDefault()
									props.showRightSidebarAction(false)
								}}
							>
								<i className='mdi mdi-close noti-icon' />
							</Link>
							<h5 className='m-0'>Settings</h5>
						</div>

						<hr className='my-0' />

						<div className='p-4'>
							<div className='radio-toolbar'>
								<span className='mb-2 d-block'>Layouts</span>
								<input
									type='radio'
									name='radioFruit'
									id='radioVertical'
									value={layoutTypes.VERTICAL}
									checked={props.layoutType === layoutTypes.VERTICAL}
									onChange={e => {
										if (e.target.checked) {
											props.changeLayout(e.target.value)
										}
									}}
								/>
								<label className='me-1' htmlFor='radioVertical'>
									Vertical
								</label>
								<input
									type='radio'
									name='radioFruit'
									id='radioHorizontal'
									value={layoutTypes.HORIZONTAL}
									checked={props.layoutType === layoutTypes.HORIZONTAL}
									onChange={e => {
										if (e.target.checked) {
											props.changeLayout(e.target.value)
										}
									}}
								/>
								<label htmlFor='radioHorizontal'>Horizontal</label>
							</div>

							<hr className='mt-1' />

							<div className='radio-toolbar'>
								<span id='radio-title' className='mb-2 d-block'>
									Layout Width
								</span>
								<input
									type='radio'
									id='radioFluid'
									name='radioWidth'
									value={layoutWidthTypes.FLUID}
									checked={props.layoutWidth === layoutWidthTypes.FLUID}
									onChange={e => {
										if (e.target.checked) {
											props.changeLayoutWidth(e.target.value)
										}
									}}
								/>
								<label className='me-1' htmlFor='radioFluid'>
									Fluid
								</label>
								<input
									type='radio'
									id='radioBoxed'
									name='radioWidth'
									value={layoutWidthTypes.BOXED}
									checked={props.layoutWidth === layoutWidthTypes.BOXED}
									onChange={e => {
										if (e.target.checked) {
											props.changeLayoutWidth(e.target.value)
										}
									}}
								/>
								<label className='me-2' htmlFor='radioBoxed'>
									Boxed
								</label>
								<input
									type='radio'
									id='radioscrollable'
									name='radioscrollable'
									value={layoutWidthTypes.SCROLLABLE}
									checked={props.layoutWidth === layoutWidthTypes.SCROLLABLE}
									onChange={e => {
										if (e.target.checked) {
											props.changeLayoutWidth(e.target.value)
										}
									}}
								/>
								<label htmlFor='radioscrollable'>Scrollable</label>
							</div>
							<hr className='mt-1' />

							<div className='radio-toolbar'>
								<span id='radio-title' className='mb-2 d-block'>
									Topbar Theme
								</span>
								<input
									type='radio'
									name='radioTheme'
									id='radioThemeLight'
									value={topBarThemeTypes.LIGHT}
									checked={props.topbarTheme === topBarThemeTypes.LIGHT}
									onChange={e => {
										if (e.target.checked) {
											props.changeTopbarTheme(e.target.value)
										}
									}}
								/>
								<label className='me-1' htmlFor='radioThemeLight'>
									Light
								</label>
								<input
									type='radio'
									name='radioTheme'
									id='radioThemeDark'
									value={topBarThemeTypes.DARK}
									checked={props.topbarTheme === topBarThemeTypes.DARK}
									onChange={e => {
										if (e.target.checked) {
											props.changeTopbarTheme(e.target.value)
										}
									}}
								/>
								<label className='me-1' htmlFor='radioThemeDark'>
									Dark
								</label>
								{props.layoutType === 'vertical' ? null : (
									<>
										<input
											type='radio'
											name='radioTheme'
											id='radioThemeColored'
											value={topBarThemeTypes.COLORED}
											checked={props.topbarTheme === topBarThemeTypes.COLORED}
											onChange={e => {
												if (e.target.checked) {
													props.changeTopbarTheme(e.target.value)
												}
											}}
										/>
										<label className='me-1' htmlFor='radioThemeColored'>
											Colored
										</label>{' '}
									</>
								)}
							</div>

							{props.layoutType === 'vertical' ? (
								<>
									<hr className='mt-1' />
									<div className='radio-toolbar'>
										<span id='radio-title' className='mb-2 d-block'>
											Left Sidebar Type{' '}
										</span>
										<input
											type='radio'
											name='sidebarType'
											id='sidebarDefault'
											value={leftSidebarTypes.DEFAULT}
											checked={props.leftSideBarType === leftSidebarTypes.DEFAULT}
											onChange={e => {
												if (e.target.checked) {
													props.changeSidebarType(e.target.value)
												}
											}}
										/>
										<label className='me-1' htmlFor='sidebarDefault'>
											Default
										</label>
										<input
											type='radio'
											name='sidebarType'
											id='sidebarCompact'
											value={leftSidebarTypes.COMPACT}
											checked={props.leftSideBarType === leftSidebarTypes.COMPACT}
											onChange={e => {
												if (e.target.checked) {
													props.changeSidebarType(e.target.value)
												}
											}}
										/>
										<label className='me-1' htmlFor='sidebarCompact'>
											Compact
										</label>
										<input
											type='radio'
											id='sidebarIcon'
											name='sidebarType'
											value={leftSidebarTypes.ICON}
											checked={props.leftSideBarType === leftSidebarTypes.ICON}
											onChange={e => {
												if (e.target.checked) {
													props.changeSidebarType(e.target.value)
												}
											}}
										/>
										<label className='me-1' htmlFor='sidebarIcon'>
											Icon
										</label>
									</div>

									<hr className='mt-1' />

									<div className='radio-toolbar coloropt-radio'>
										<span id='radio-title' className='mb-2 d-block'>
											Left Sidebar Color Options
										</span>
										<Row>
											<Col>
												<input
													type='radio'
													name='leftsidebarTheme'
													id='leftsidebarThemelight'
													value={leftSideBarThemeTypes.LIGHT}
													checked={props.leftSideBarTheme === leftSideBarThemeTypes.LIGHT}
													onChange={e => {
														if (e.target.checked) {
															props.changeSidebarTheme(e.target.value)
														}
													}}
												/>
												<label htmlFor='leftsidebarThemelight' className='bg-light rounded-circle wh-30 me-1'></label>

												<input
													type='radio'
													name='leftsidebarTheme'
													id='leftsidebarThemedark'
													value={leftSideBarThemeTypes.DARK}
													checked={props.leftSideBarTheme === leftSideBarThemeTypes.DARK}
													onChange={e => {
														if (e.target.checked) {
															props.changeSidebarTheme(e.target.value)
														}
													}}
												/>
												<label htmlFor='leftsidebarThemedark' className='bg-dark rounded-circle wh-30 me-1'></label>

												<input
													type='radio'
													name='leftsidebarTheme'
													id='leftsidebarThemecolored'
													value={leftSideBarThemeTypes.COLORED}
													checked={props.leftSideBarTheme === leftSideBarThemeTypes.COLORED}
													onChange={e => {
														if (e.target.checked) {
															props.changeSidebarTheme(e.target.value)
														}
													}}
												/>
												<label htmlFor='leftsidebarThemecolored' className='bg-colored rounded-circle wh-30 me-1'></label>
											</Col>
										</Row>
										<Row>
											<Col>
												<input
													type='radio'
													name='leftsidebarTheme'
													id='leftsidebarThemewinter'
													value={leftSideBarThemeTypes.WINTER}
													checked={props.leftSideBarTheme === leftSideBarThemeTypes.WINTER}
													onChange={e => {
														if (e.target.checked) {
															props.changeSidebarTheme(e.target.value)
														}
													}}
												/>
												<label htmlFor='leftsidebarThemewinter' className='gradient-winter rounded-circle wh-30 me-1'></label>

												<input
													type='radio'
													name='leftsidebarTheme'
													id='leftsidebarThemeladylip'
													value={leftSideBarThemeTypes.LADYLIP}
													checked={props.leftSideBarTheme === leftSideBarThemeTypes.LADYLIP}
													onChange={e => {
														if (e.target.checked) {
															props.changeSidebarTheme(e.target.value)
														}
													}}
												/>
												<label htmlFor='leftsidebarThemeladylip' className='gradient-lady-lip rounded-circle wh-30 me-1'></label>

												<input
													type='radio'
													name='leftsidebarTheme'
													id='leftsidebarThemeplumplate'
													value={leftSideBarThemeTypes.PLUMPLATE}
													checked={props.leftSideBarTheme === leftSideBarThemeTypes.PLUMPLATE}
													onChange={e => {
														if (e.target.checked) {
															props.changeSidebarTheme(e.target.value)
														}
													}}
												/>
												<label htmlFor='leftsidebarThemeplumplate' className='gradient-plum-plate rounded-circle wh-30 me-1'></label>

												<input
													type='radio'
													name='leftsidebarTheme'
													id='leftsidebarThemestrongbliss'
													value={leftSideBarThemeTypes.STRONGBLISS}
													checked={props.leftSideBarTheme === leftSideBarThemeTypes.STRONGBLISS}
													onChange={e => {
														if (e.target.checked) {
															props.changeSidebarTheme(e.target.value)
														}
													}}
												/>
												<label htmlFor='leftsidebarThemestrongbliss' className='gradient-strong-bliss rounded-circle wh-30 me-1'></label>
												<input
													type='radio'
													name='leftsidebarTheme'
													id='leftsidebarThemesgreatwhale'
													value={leftSideBarThemeTypes.GREATWHALE}
													checked={props.leftSideBarTheme === leftSideBarThemeTypes.GREATWHALE}
													onChange={e => {
														if (e.target.checked) {
															props.changeSidebarTheme(e.target.value)
														}
													}}
												/>
												<label htmlFor='leftsidebarThemesgreatwhale' className='gradient-strong-great-whale rounded-circle wh-30 me-1'></label>
											</Col>
										</Row>
									</div>

								</>
							) : null}
						</div>
					</div>
				</SimpleBar>
			</div>
			<div className='rightbar-overlay'></div>
		</>
	)
}

RightSidebar.propTypes = {
	changeLayout: PropTypes.func,
	changeLayoutWidth: PropTypes.func,
	changePreloader: PropTypes.func,
	changeSidebarTheme: PropTypes.func,
	changeSidebarThemeImage: PropTypes.func,
	changeSidebarType: PropTypes.func,
	changeTopbarTheme: PropTypes.func,
	isPreloader: PropTypes.any,
	layoutType: PropTypes.any,
	layoutWidth: PropTypes.any,
	leftSideBarTheme: PropTypes.any,
	leftSideBarThemeImage: PropTypes.any,
	leftSideBarType: PropTypes.any,
	showRightSidebarAction: PropTypes.func,
	topbarTheme: PropTypes.any,
	onClose: PropTypes.func,
}

const mapStateToProps = state => {
	return { ...state.Layout }
}

export default connect(mapStateToProps, {
	changeLayout,
	changeSidebarTheme,
	changeSidebarThemeImage,
	changeSidebarType,
	changeLayoutWidth,
	changeTopbarTheme,
	changePreloader,
	showRightSidebarAction,
})(RightSidebar)
