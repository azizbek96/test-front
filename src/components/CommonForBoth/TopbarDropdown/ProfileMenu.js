// users
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap'
//i18n
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import React, {useEffect, useState} from 'react'

// Redux

import user1 from '../../../assets/images/users/avatar-6.jpg'

const ProfileMenu = () => {
	// Declare a new state variable, which we'll call "menu"
	const [menu, setMenu] = useState(false)
	const [username, setusername] = useState('Admin')

	const [t] = useTranslation()
	useEffect(() => {
		setusername(localStorage.getItem('UserName'));
	}, [localStorage]);

	return (
		<>
			<Dropdown isOpen={menu} className='d-inline-block' toggle={() => setMenu(!menu)}>
				<DropdownToggle tag='button' className='btn header-item ' id='page-header-user-dropdown'>
					<img src={user1} alt='Header Avatar' className='rounded-circle header-profile-user' />
					<span className='d-none d-xl-inline-block ms-2 me-1'>{username}</span>
					<i className='mdi mdi-chevron-down d-none d-xl-inline-block' />
				</DropdownToggle>
				<DropdownMenu className='dropdown-menu-end'>
					<DropdownItem tag='a' href='/profile'>
						{' '}
						<i className='bx bx-user font-size-16 align-middle me-1' />
						{t('Profile')}{' '}
					</DropdownItem>
					<div className='dropdown-divider' />
					<Link to='/logout' className='dropdown-item'>
						<i className='bx bx-power-off font-size-16 align-middle me-1 text-danger' />
						<span>{t('Logout')}</span>
					</Link>
				</DropdownMenu>
			</Dropdown>
		</>
	)
}

export default ProfileMenu
