import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap'
import React, { useState, useEffect } from 'react'
import { withTranslation } from 'react-i18next'
import { get, map } from 'lodash'

import languages from '../../../common/languages'
//i18n
import i18n from '../../../i18n'

const LanguageDropdown = () => {
	// Declare a new state variable, which we'll call "menu"
	const [selectedLang, setSelectedLang] = useState('')
	const [menu, setMenu] = useState(false)

	useEffect(() => {
		const currentLanguage = localStorage.getItem('I18N_LANGUAGE')
		setSelectedLang(currentLanguage)
	}, [])

	const changeLanguageAction = lang => {
		//set language as i18n
		i18n.changeLanguage(lang)
		localStorage.setItem('I18N_LANGUAGE', lang)
		setSelectedLang(lang)
	}

	const toggle = () => {
		setMenu(!menu)
	}

	return (
		<>
			<Dropdown isOpen={menu} toggle={toggle} className='d-inline-block'>
				<DropdownToggle tag='button' className='btn header-item '>
					<img alt='Skote' height='24' className='me-1' src={get(languages, `${selectedLang}.flag`)} />
				</DropdownToggle>
				<DropdownMenu className='language-switch dropdown-menu-end'>
					{map(Object.keys(languages), key => (
						<DropdownItem key={key} onClick={() => changeLanguageAction(key)} className={`notify-item ${selectedLang === key ? 'active' : 'none'}`}>
							<img alt='Skote' height='24' className='me-2' src={get(languages, `${key}.flag`)} />
							<span className='align-middle'>{get(languages, `${key}.label`)}</span>
						</DropdownItem>
					))}
				</DropdownMenu>
			</Dropdown>
		</>
	)
}

export default withTranslation()(LanguageDropdown)
