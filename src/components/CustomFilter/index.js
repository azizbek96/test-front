import React, { useState } from 'react'

import { FormDatePicker } from '../DatePicker'

let v = ''

/**
 *
 * @param {Function} onFilter
 * @param column
 * @return {JSX.Element}
 * @constructor
 */
const CustomFilter = ({ onFilter }) => {
	const [dateValue, setDateValue] = useState(v)

	return (
		<div className='d-inline-flex gap-2'>
			<FormDatePicker.WithYear
				selected={dateValue}
				isClearable={!!dateValue}
				placeholderText='Select date'
				popperProps={{ strategy: 'fixed' }}
				customInput={<FormDatePicker.CustomInput invalid={false} />}
				onChange={date => {
					v = date
					setDateValue(date)
					onFilter({ date })
				}}
			/>
		</div>
	)
}

export default CustomFilter
