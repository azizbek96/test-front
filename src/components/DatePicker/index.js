import DatePicker, { registerLocale } from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.min.css'
import { Input } from 'reactstrap'
import { forwardRef } from 'react'

import Cyrl from 'date-fns/locale/uz-Cyrl/'

registerLocale('uz-Cyrl', Cyrl)

/**
 *
 * @param {import('react-datepicker').ReactDatePickerProps} props
 * @return {JSX.Element}
 */
const WithYear = props => {
	return <DatePicker {...props} locale='uz-Cyrl' showYearDropdown showMonthDropdown dropdownMode='select' dateFormat='dd-MM-yyyy' />
}

// eslint-disable-next-line react/display-name
const CustomInput = forwardRef(
	/*eslint-disable react/prop-types*/
	(
		{ value, onBlur, invalid, onClick, onChange, placeholder, errorMessage, ...props },
		ref
		/*eslint-enable react/prop-types*/
	) => {
		return (
			<>
				<Input ref={ref} {...props} value={value} onBlur={onBlur} onClick={onClick} invalid={invalid} onChange={onChange} placeholder={placeholder} />
			</>
		)
	}
)

const FormDatePicker = {
	WithYear,
	CustomInput,
}

export { FormDatePicker }
