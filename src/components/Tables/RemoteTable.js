import paginationFactory, { PaginationProvider, PaginationListStandalone } from 'react-bootstrap-table2-paginator'
import textFilter from 'react-bootstrap-table2-filter/lib/src/components/text'
import React, { memo, useMemo, useEffect, useCallback } from 'react'
import ToolkitProvider from 'react-bootstrap-table2-toolkit'
import overlayFactory from 'react-bootstrap-table2-overlay'
import BootstrapTable from 'react-bootstrap-table-next'
import HighLighter from 'react-highlight-words'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import { Col, Row } from 'reactstrap'
import PropTypes from 'prop-types'

import empty from '../../assets/images/empty.svg'

const EmptyIndicator = () => {
	const [t] = useTranslation()
	return (
		<div className='text-center'>
			<img src={empty} width='500' alt={t('common:empty')} />
		</div>
	)
}

/**
 *
 * @param data
 * @param columns
 * @param keyField
 * @param {import('react-bootstrap-table-next').BootstrapTableProps} tableProps
 * @param pagination
 * @param loadingState
 * @param actionCallback
 * @param {Function} [filterAction]
 * @param {object} remote
 * @param {Function} [sortAction]
 * @return {JSX.Element}
 */
const Table = ({ data, columns, keyField, tableProps, pagination, loadingState, remote = true, actionCallback }) => {
	const dispatch = useDispatch()

	/**
	 *
	 * @type {import('react-bootstrap-table-next').BootstrapTableProps}
	 */
	const loaderProps = useMemo(
		() => ({
			remote: remote,
			loading: loadingState,
			overlay: overlayFactory({
				spinner: true,
				styles: {
					overlay: base => {
						return {
							...base,
							margin: '0 !important',
							height: '100% !important',
							background: 'rgba(66,66,66,0.5)',
						}
					},
				},
			}),
			noDataIndication: () => <EmptyIndicator />,
		}),
		[loadingState]
	)

	/**
	 *
	 * @type {import('react-bootstrap-table-next').TableChangeHandler}
	 */
	const onTableChange = useCallback((_type, state) => {
		dispatch(actionCallback(state.page, state.sortField, state.sortOrder, state.filters))
	}, [])

	useEffect(() => {
		columns = columns.map(a => {
			if (a.filter) {
				if (a.filter.Filter === textFilter) {
					a.formatExtraData = {
						filterVal: () => document.getElementById('text-filter-column-' + a.dataField).value,
					}

					a.formatter = (cell, _row, _rowIndex, formatExtraData) => {
						return (
							<>
								<HighLighter textToHighlight={cell ?? ''} highlightClassName='select-highlight' searchWords={[formatExtraData.filterVal()]}>
									{cell}
								</HighLighter>
							</>
						)
					}
				}
			}

			return a
		})
	}, [])

	return (
		<PaginationProvider data={data} keyField='id' pagination={paginationFactory({ custom: true, ...pagination })}>
			{({ paginationProps, paginationTableProps }) => {
				return (
					<ToolkitProvider bootstrap4 data={data} keyField='id' columns={columns}>
						{toolkitProps => {
							return (
								<>
									<Row>
										<Col xl='12'>
											<div className='table-responsive'>
												<BootstrapTable
													data={data}
													condensed
													striped
													{...tableProps}
													{...loaderProps}
													bordered={false}
													columns={columns}
													filterPosition='top'
													headerClasses='align-top'
													{...paginationTableProps}
													{...toolkitProps.baseProps}
													keyField={keyField ?? 'id'}
													onTableChange={onTableChange}
													classes={'table align-middle table-nowrap table-check'}
												/>
											</div>
										</Col>
									</Row>
									{!_.isEmpty(pagination) && pagination?.totalCount !== 0 && Number(pagination?.pageCount) !== 1 && (
										<div className='pagination pagination-rounded justify-content-end'>
											<PaginationListStandalone {...paginationProps} />
										</div>
									)}
								</>
							)
						}}
					</ToolkitProvider>
				)
			}}
		</PaginationProvider>
	)
}

Table.propTypes = {
	keyField: PropTypes.string,
	pagination: PropTypes.object,
	tableProps: PropTypes.object,
	loadingState: PropTypes.bool,
	data: PropTypes.array.isRequired,
	columns: PropTypes.array.isRequired,
	actionCallback: PropTypes.func.isRequired,
}

export default memo(Table)
