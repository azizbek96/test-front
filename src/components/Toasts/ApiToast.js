import { Flip, toast, ToastContainer } from 'react-toastify'
import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

const ApiToast = ({ errors }) => {
	useEffect(() => {
		// prettier-ignore
		[...new Set(errors)].forEach((error, index) => {
			toast.error(error?.toString(), {
				toastId: 'api-error-toast-' + index
			})
		})
	}, [errors])

	return (
		<ToastContainer
			rtl={false}
			pauseOnHover
			closeOnClick
			theme='colored'
			autoClose={5000}
			pauseOnFocusLoss
			transition={Flip}
			newestOnTop={false}
			position='top-right'
			hideProgressBar={false}
		/>
	)
}

ApiToast.propTypes = {
	errors: PropTypes.array.isRequired,
}

export default ApiToast
