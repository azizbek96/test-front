import { Flip, toast, ToastContainer } from 'react-toastify'
import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

const FormSuccessToast = ({ messages }) => {
	useEffect(() => {
		;[...new Set(messages)].forEach((message, index) => {
			toast.success(message.message, {
				toastId: 'api-success-toast-',
			})
		})
	}, [messages])
	return (
		<ToastContainer
			rtl={false}
			pauseOnHover
			closeOnClick
			theme='colored'
			autoClose={5000}
			pauseOnFocusLoss
			transition={Flip}
			newestOnTop={false}
			position='top-right'
			hideProgressBar={false}
		/>
	)
}

FormSuccessToast.propTypes = {
	messages: PropTypes.array.isRequired,
}

export default FormSuccessToast
