import React, { useRef, useMemo } from 'react'
import { Spinner } from 'reactstrap'
import PropTypes from 'prop-types'

import './style.scss'

const FormLoader = ({ loading }) => {
	/**
	 * @type {React.MutableRefObject<HTMLDivElement>}
	 */
	const formLoadingRef = useRef()

	const getVisibleHeight = useMemo(() => {
		if (formLoadingRef.current) {
			const height = window.innerHeight
			const elementHeight = formLoadingRef.current.offsetHeight
			const { top, bottom } = formLoadingRef.current.getBoundingClientRect()

			return Math.max(0, top > 0 ? Math.min(elementHeight, height - top) : Math.min(bottom, height))
		}
	}, [formLoadingRef.current])

	return loading === true ? (
		<div ref={formLoadingRef} className='form-loading'>
			<div className='spinner-wrapper' style={{ maxHeight: getVisibleHeight }}>
				<Spinner />
			</div>
		</div>
	) : (
		<></>
	)
}

FormLoader.propTypes = {
	loading: PropTypes.bool.isRequired,
}

export default FormLoader
