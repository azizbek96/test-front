import PropTypes from 'prop-types'
import React from 'react'

const NonAuthLayout = props => {
	return <>{props.children}</>
}

NonAuthLayout.propTypes = {
	children: PropTypes.node,
}

export default NonAuthLayout
