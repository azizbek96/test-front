import { motion, MotionConfig, AnimatePresence } from 'framer-motion'
import React, { memo, PureComponent } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import TreeContext from '../../pages/TestSpeciality/context'
import Icons from './icons'

const styles = {
	tree: {
		overflow: 'hidden',
		position: 'relative',
		whiteSpace: 'nowrap',
		verticalAlign: 'middle',
		textOverflow: 'ellipsis',
		padding: '4px 0px 0px 0px',
	},
	toggle: {
		width: '1em',
		height: '1em',
		marginRight: 10,
		cursor: 'pointer',
		verticalAlign: 'middle',
	},
	type: {
		fontSize: '0.6em',
		fontFamily: 'monospace',
		verticalAlign: 'middle',
		textTransform: 'uppercase',
	},
	contents: {
		marginLeft: '6px',
		padding: '4px 0px 0px 14px',
		willChange: 'transform, opacity, height',
		borderLeft: '1px dashed rgba(255,255,255,0.4)',
	},
}

/**
 *
 * @param {React.FocusEvent<HTMLButtonElement>} e
 * @return Boolean
 */
const checkClickedButton = e => {
	const classLists = ['.department-action-btn', '.department-employees-card', '.employees-department-actions']

	let ret = true

	for (let i = 0; i < classLists.length; i++) {
		if (e.relatedTarget?.closest(classLists[i])) {
			ret = false
			break
		}
	}

	return ret
}

// eslint-disable-next-line react/display-name
class Tree extends PureComponent {
	static contextType = TreeContext
	static propTypes = {
		id: PropTypes.any,
		type: PropTypes.any,
		open: PropTypes.bool,
		clear: PropTypes.func,
		style: PropTypes.object,
		visible: PropTypes.bool,
		canHide: PropTypes.bool,
		content: PropTypes.node,
		onClick: PropTypes.func,
		watcher: PropTypes.func,
		disabled: PropTypes.bool,
		children: PropTypes.node,
		isActive: PropTypes.bool,
		parent_id: PropTypes.any,
		springConfig: PropTypes.func,
	}
	static defaultProps = {
		open: false,
		visible: true,
		canHide: false,
		isActive: false,
		disabled: false,
	}

	constructor(props) {
		super(props)
		this.state = {
			open: props.open,
			immediate: false,
			visible: props.visible,
		}
	}

	activeHandler = () => {
		this.props.onClick &&
			this.props.onClick({
				id: this.props.id,
				open: this.state.open,
				name: this.props.content,
				parent_id: this.props.parent_id,
			})
		this.toggle()
	}

	toggle = (value = !this.state.open) => {
		if (this.props.children) {
			this.setState(
				() => ({
					immediate: false,
					open: value,
				}),
				() => {
					this.props.watcher?.({
						id: this.props.id,
						open: this.state.open,
					})
				}
			)
		}
	}

	toggleVisibility = () => {
		this.setState(
			state => ({ visible: !state.visible, immediate: true })
			// () => this.props.onClick && this.props.onClick(this.state.visible)
		)
	}

	UNSAFE_componentWillReceiveProps(props) {
		this.setState(() => {
			return ['open', 'visible'].reduce((acc, val) => (this.props[val] !== props[val] ? { ...acc, [val]: props[val] } : acc), {})
		})
	}

	render() {
		const { isLoading } = this.context
		const { open, visible } = this.state
		const { type, style, content, canHide, children, isActive } = this.props
		const Icon = Icons[`${children ? (open ? 'Minus' : 'Plus') : 'Close'}SquareO`]

		return (
			<div className='treeview' style={{ ...styles.tree, ...style }}>
				<button
					tabIndex={1}
					type='button'
					onFocus={this.activeHandler}
					// onDoubleClick={this.toggle}
					onClick={this.activeHandler}
					disabled={this.props.disabled}
					id={'treebutton-' + this.props.id}
					onBlur={e => {
						if (checkClickedButton(e)) {
							this.props.clear()
						}
					}}
					className={classNames('treeview-content border-0 px-1', {
						'bg-primary text-white': isActive /*&&
							document.activeElement.classList.contains(
								'treeview-content'
							),*/,
					})}
					style={{
						color: 'inherit',
						borderRadius: '5px',
						transition: 'background 0.5s ease 0s',
						background: !isLoading && 'transparent',
						animation: isLoading && 'placeholder-glow 2s ease-in-out infinite',
					}}
					onKeyUp={e => {
						if (e.keyCode === 13) {
							this.toggle()
						} else if (/arrow/i.test(e.key)) {
							const buttons = document.querySelectorAll('.treeview-content')

							const child = document.querySelector('.treeview-content.bg-primary .treeview-child')
							if (e.keyCode === 40) {
								if (_.last(buttons) === e.target) {
									buttons[0].focus()
								} else {
									buttons[_.findLastIndex(buttons, f => f.classList.contains('bg-primary')) + 1].focus()
								}
							} else if (e.keyCode === 38) {
								if (buttons[0] === e.target) {
									_.last(buttons).focus()
								} else {
									buttons[_.findLastIndex(buttons, f => f.classList.contains('bg-primary')) - 1].focus()
								}
							} else if (e.keyCode === 39) {
								if (child !== e.target) {
									this.toggle(true)
								}
							} else {
								if (child) {
									this.toggle(false)
								}
							}
						}
					}}
				>
					<Icon
						className='toggle'
						style={{
							...styles.toggle,
							opacity: children ? 1 : 0.3,
						}}
					/>
					<span
						style={{
							...styles.type,
							marginRight: type ? 10 : 0,
						}}
					>
						{type}
					</span>
					{canHide && (
						<Icons.EyeO
							onClick={this.toggleVisibility}
							style={{
								...styles.toggle,
								opacity: visible ? 1 : 0.4,
							}}
						/>
					)}
					<span
						className='treeview-child'
						style={{
							cursor: 'pointer',
							userSelect: 'none',
							verticalAlign: 'middle',
						}}
					>
						{content}
					</span>
				</button>
				<MotionConfig transition={{ duration: 0.2 }}>
					<AnimatePresence initial={false}>
						{open && (
							<motion.div
								style={styles.contents}
								exit={{
									height: 0,
									opacity: 0,
									transform: 'translate3d(25px,0,0)',
								}}
								initial={{
									height: 0,
									opacity: 0,
									transform: 'translate3d(25px,0,0)',
								}}
								animate={{
									opacity: open ? 1 : 0,
									height: open ? 'auto' : 0,
									transform: open ? 'translate3d(0px,0,0)' : 'translate3d(25px,0,0)',
								}}
							>
								{children}
							</motion.div>
						)}
					</AnimatePresence>
				</MotionConfig>
			</div>
		)
	}
}

export default memo(Tree)
