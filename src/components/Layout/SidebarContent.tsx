import { NavLink, Link, useLocation } from 'react-router-dom'
import { motion, AnimatePresence } from 'framer-motion'
import { useDispatch, useSelector } from 'react-redux'
import React, { memo, useRef, useEffect } from 'react'
import SimpleBar from 'simplebar-react'
import classNames from 'classnames'
import { isEmpty } from 'lodash'

import { appMenuActiveSetAction } from '../../store/app/actions'
import { Menu } from '../../store/app/reducer'
import { RootState } from '../../store'
import './sidebar-content.scss'

interface SelectedSelector {
	menu: Menu[]
	activeMenu: string[]
}

const SidebarContent = () => {
	const dispatch = useDispatch()
	const location = useLocation()
	const ref = useRef<SimpleBar>()
	const { menu, activeMenu } = useSelector<RootState, SelectedSelector>(s => ({
		menu: s.App.menu,
		activeMenu: s.App.activeMenu,
	}))

	useEffect(() => {
		ref.current?.recalculate()
	}, [])

	useEffect(() => {
		menu.forEach((m, i) => {
			const activeFromUrl = m.content.findIndex(c => {
				const paths = location.pathname.split('/').filter(a => !isEmpty(a))

				return new RegExp(`(${paths.join('|')})`).test(c.to)
			})

			if (activeFromUrl > -1) {
				dispatch(appMenuActiveSetAction([...activeMenu, `active-menu-${i}`]))
			}
		})
	}, [menu])

	return (
		// @ts-ignore
		<SimpleBar ref={ref} className='w-100'>
			<div className='sidebar-menu'>
				{Array.isArray(menu) &&
					menu.map((item, i) => {
						const isActive = activeMenu.includes(`active-menu-${i}`)

						return (
							<div key={item.label + i} id={'sidebar-menu__group' + i} className='sidebar-menu__group'>
								<a
									className='sidebar-menu__title'
									onClick={() => {
										const payload = isActive ? activeMenu.filter(item => item !== `active-menu-${i}`) : [...activeMenu, `active-menu-${i}`]

										dispatch(appMenuActiveSetAction(payload))
									}}
								>
									<span className='sidebar-menu__title-label'>{item.label}</span>
									<motion.i
										transition={{ duration: 0.3 }}
										exit={{ rotate: isActive ? '0deg' : '90deg' }}
										initial={{ rotate: isActive ? '0deg' : '90deg' }}
										animate={{ rotate: isActive ? '0deg' : '90deg' }}
										className='sidebar-menu__title-arrow mdi mdi-chevron-down'
									/>
								</a>
								<AnimatePresence initial={false}>
									{isActive && (
										<motion.ul
											animate='open'
											exit='collapsed'
											initial='collapsed'
											className='sidebar-menu__list'
											transition={{ duration: 0.3 }}
											variants={{
												open: { height: 'auto' },
												collapsed: { height: 0 },
											}}
										>
											{Array.isArray(item.content) &&
												item.content.map((subItem, j) => {
													return (
														<li key={subItem.label + j} className='sidebar-menu__list-item'>
															<NavLink to={subItem.to} activeClassName='active-link' className='sidebar-menu__list-link'>
																<i className={classNames('sidebar-menu__list-icon', subItem.icon)} />
																<span className='sidebar-menu__list-label'>{subItem.label}</span>
															</NavLink>
														</li>
													)
												})}
										</motion.ul>
									)}
								</AnimatePresence>
							</div>
						)
					})}
			</div>
		</SimpleBar>
	)
}

export default memo(SidebarContent)
