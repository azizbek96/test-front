import { useDispatch, useSelector } from 'react-redux'
import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import classNames from 'classnames'

// import NotificationDropdown from '../CommonForBoth/TopbarDropdown/NotificationDropdown'
import LanguageDropdown from '../CommonForBoth/TopbarDropdown/LanguageDropdown'
import ProfileMenu from '../CommonForBoth/TopbarDropdown/ProfileMenu'
import logoLightSvg from '../../assets/images/logo-light.svg'
import { showRightSidebarAction } from '../../store/actions'

const Header = () => {
	const [search, setSearch] = useState(false)

	const [t] = useTranslation()
	const dispatch = useDispatch()
	const { showRightSidebar } = useSelector(s => s.Layout)

	const [fullScreen, setFullScreen] = useState(false)

	const toggleFullscreen = () => {
		if (!document.fullscreenElement && /* alternative standard method */ !document.mozFullScreenElement && !document.webkitFullscreenElement) {
			// current working methods
			if (document.documentElement.requestFullscreen) {
				document.documentElement.requestFullscreen()
			} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen()
			} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT)
			}

			setFullScreen(true)
		} else {
			if (document.cancelFullScreen) {
				document.cancelFullScreen()
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen()
			} else if (document.webkitCancelFullScreen) {
				document.webkitCancelFullScreen()
			}

			setFullScreen(false)
		}
	}

	const tToggle = () => {
		const { body } = document
		if (window.screen.width <= 998) {
			body.classList.toggle('sidebar-enable')
		} else {
			body.classList.toggle('vertical-collapsed')
			body.classList.toggle('sidebar-enable')
		}
	}

	return (
		<>
			<header id='page-topbar'>
				<div className='navbar-header'>
					<div className='d-flex'>
						<div className='navbar-brand-box d-lg-none d-md-block'>
							<Link to='/' className='logo logo-dark'>
								<span className='logo-sm'>
									TEST
								</span>
							</Link>

							<Link to='/' className='logo logo-light'>
								<span className='logo-sm'>
									TEST
								</span>
							</Link>
						</div>

						<button
							type='button'
							id='vertical-menu-btn'
							onClick={() => {
								tToggle()
							}}
							className='btn btn-sm px-3 font-size-16 header-item '
						>
							<i className='fa fa-fw fa-bars' />
						</button>

						{/*<form autoComplete='off' className='app-search d-none d-lg-block'>*/}
						{/*	<div className='position-relative'>*/}
						{/*		<input type='text' className='form-control' placeholder={t('Search') + '...'} />*/}
						{/*		<span className='bx bx-search-alt' />*/}
						{/*	</div>*/}
						{/*</form>*/}
					</div>
					<div className='d-flex'>
						<div className='dropdown d-inline-block d-lg-none ms-2'>
							<button
								type='button'
								id='page-header-search-dropdown'
								className='btn header-item noti-icon '
								onClick={() => {
									setSearch(!search)
								}}
							>
								<i className='mdi mdi-magnify' />
							</button>
							<div aria-labelledby='page-header-search-dropdown' className={classNames('dropdown-menu dropdown-menu-lg dropdown-menu-end p-0', { show: search })}>
								<form className='p-3' autoComplete='off'>
									<div className='form-group m-0'>
										<div className='input-group'>
											<input type='text' className='form-control' placeholder='Search ...' aria-label="Recipient's username" />
											<div className='input-group-append'>
												<button type='submit' className='btn btn-primary'>
													<i className='mdi mdi-magnify' />
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>

						<LanguageDropdown />

						<div className='dropdown d-none d-lg-inline-block ms-1'>
							<button type='button' data-toggle='fullscreen' onClick={toggleFullscreen} className='btn header-item noti-icon'>
								<i className={classNames('bx', fullScreen ? 'bx-exit-fullscreen' : 'bx-fullscreen')} />
							</button>
						</div>

						{/*<NotificationDropdown />*/}
						<ProfileMenu />

						<div
							className='dropdown d-inline-block'
							onClick={() => {
								dispatch(showRightSidebarAction(!showRightSidebar))
							}}
						>
							<button type='button' className='btn header-item noti-icon right-bar-toggle '>
								<i className='bx bx-cog bx-spin' />
							</button>
						</div>
					</div>
				</div>
			</header>
		</>
	)
}

export default Header
