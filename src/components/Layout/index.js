import { useDispatch, useSelector } from 'react-redux'
import { useLocation } from 'react-router-dom'
import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

import { changeLayout, changeLayoutWidth, changeSidebarType, changeTopbarTheme, changeSidebarTheme, showRightSidebarAction, changeSidebarThemeImage } from '../../store/actions'
import RightSidebar from '../CommonForBoth/RightSidebar'
import Sidebar from './Sidebar'
// Layout Related Components
import Header from './Header'

const Layout = props => {
	const dispatch = useDispatch()

	const { layoutWidth, topbarTheme, leftSideBarType, showRightSidebar, leftSideBarTheme, leftSideBarThemeImage } = useSelector(state => ({
		topbarTheme: state.Layout.topbarTheme,
		layoutWidth: state.Layout.layoutWidth,
		leftSideBarType: state.Layout.leftSideBarType,
		showRightSidebar: state.Layout.showRightSidebar,
		leftSideBarTheme: state.Layout.leftSideBarTheme,
		leftSideBarThemeImage: state.Layout.leftSideBarThemeImage,
	}))

	const location = useLocation()

	const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

	const toggleMenuCallback = () => {
		if (leftSideBarType === 'default') {
			dispatch(changeSidebarType('condensed', isMobile))
		} else if (leftSideBarType === 'condensed') {
			dispatch(changeSidebarType('default', isMobile))
		}
	}

	//hides right sidebar on body click
	const hideRightbar = event => {
		var rightbar = document.getElementById('right-bar')
		//if clicked in inside right bar, then do nothing
		if (rightbar && rightbar.contains(event.target)) {
			return
		} else {
			//if clicked in outside of rightbar then fire action for hide rightbar
			dispatch(showRightSidebarAction(false))
		}
	}

	useEffect(() => {
		window.scrollTo(0, 0)
		dispatch(changeLayout('vertical'))
	}, [])

	useEffect(() => {
		if (leftSideBarTheme) {
			dispatch(changeSidebarTheme(leftSideBarTheme))
		}
	}, [leftSideBarTheme])

	useEffect(() => {
		if (leftSideBarThemeImage) {
			dispatch(changeSidebarThemeImage(leftSideBarThemeImage))
		}
	}, [leftSideBarThemeImage])

	useEffect(() => {
		if (layoutWidth) {
			dispatch(changeLayoutWidth(layoutWidth))
		}
	}, [layoutWidth])

	useEffect(() => {
		if (leftSideBarType) {
			dispatch(changeSidebarType(leftSideBarType))
		}
	}, [leftSideBarType])

	useEffect(() => {
		if (topbarTheme) {
			dispatch(changeTopbarTheme(topbarTheme))
		}
	}, [topbarTheme])

	return (
		<>
			<div id='layout-wrapper'>
				<Header toggleMenuCallback={toggleMenuCallback} />
				<Sidebar isMobile={isMobile} type={leftSideBarType} theme={leftSideBarTheme} />
				<div className='main-content'>
					{/* eslint-disable-next-line react/prop-types */}
					{location.pathname === '/employees' ? <div className='page-content'>{props.children}</div> : props.children}
				</div>
			</div>
			{showRightSidebar && <RightSidebar />}
		</>
	)
}

Layout.propTypes = {
	children: PropTypes.object,
}

export default Layout
