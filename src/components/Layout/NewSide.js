// import PropTypes from 'prop-types';
import SimpleBar from 'simplebar-react'
import MetisMenu from 'metismenujs'
import { Link, withRouter, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import React, { useRef, useEffect } from 'react'
import { useSelector } from 'react-redux'

const NewSide = () => {
	const { menu } = useSelector(s => ({
		menu: s.App.menu,
		activeMenu: s.App.activeMenu,
	}))

	const [t] = useTranslation()
	const { pathname } = useLocation()
	const ref = useRef(null)
	useEffect(() => {
		const initMenu = () => {
			new MetisMenu('#side-menu')
			let matchingMenuItem = null
			const ul = document.getElementById('side-menu')
			const items = ul.getElementsByTagName('a')
			for (let i = 0; i < items.length; ++i) {
				if (pathname === items[i].pathname) {
					matchingMenuItem = items[i]
					break
				}
			}
			if (matchingMenuItem) {
				activateParentDropdown(matchingMenuItem)
			}
		}
		initMenu()
	}, [pathname])

	useEffect(() => {
		ref.current.recalculate()
	}, [])

	const scrollElement = item => {
		if (item) {
			const currentPosition = item.offsetTop
			if (currentPosition > window.innerHeight) {
				if (this.refDiv.current) this.refDiv.current.getScrollElement().scrollTop = currentPosition - 300
			}
		}
	}
	const activateParentDropdown = item => {
		item.classList.add('active')
		const parent = item.parentElement
		const parent2El = parent.childNodes[1]
		if (parent2El && parent2El.id !== 'side-menu') {
			parent2El.classList.add('mm-show')
		}

		if (parent) {
			parent.classList.add('mm-active')
			const parent2 = parent.parentElement

			if (parent2) {
				parent2.classList.add('mm-show') // ul tag

				const parent3 = parent2.parentElement // li tag

				if (parent3) {
					parent3.classList.add('mm-active') // li
					parent3.childNodes[0].classList.add('mm-active') //a
					const parent4 = parent3.parentElement // ul
					if (parent4) {
						parent4.classList.add('mm-show') // ul
						const parent5 = parent4.parentElement
						if (parent5) {
							parent5.classList.add('mm-show') // li
							parent5.childNodes[0].classList.add('mm-active') // a tag
						}
					}
				}
			}
			scrollElement(item)
			return false
		}
		scrollElement(item)
		return false
	}
	return (
		<>
			<SimpleBar ref={ref} className='h-100'>
				<div id='sidebar-menu'>
					<ul id='side-menu' className='metismenu list-unstyled'>
						{menu.map((item, index) => (
							<li key={index} className={item.active ? 'mm-active' : ''}>
								<Link to='/#' className='has-arrow'>
									<i className={item.icon} />
									<span>{t(item.label)}</span>
								</Link>
								{item.content && item.content.length > 0 && (
									<ul>
										{item.content.map((child, index) => (
											<li key={index} className={child.active ? 'mm-active' : ''}>
												<Link to={child.to}>
													<i className={child.icon} />
													<span className='nav-text'>{t(child.label)}</span>
												</Link>
											</li>
										))}
									</ul>
								)}
							</li>
						))}
					</ul>
				</div>
			</SimpleBar>
		</>
	)
}

export default NewSide
