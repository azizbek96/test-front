import BootstrapTable, { ColumnDescription } from 'react-bootstrap-table-next'
// @ts-ignore
import cellEditFactory from 'react-bootstrap-table2-editor'
import React, { FC, useMemo, Fragment } from 'react'
import { Input, Button } from 'reactstrap'
import { useFormikContext } from 'formik'
import { useTranslation } from 'react-i18next'

import { IPermissionItems, IPermissionsForm } from './types'

const PermissionsForm: FC<{
	push: CallableFunction
	remove: CallableFunction
}> = ({ push, remove }): JSX.Element => {
	const formik = useFormikContext<IPermissionsForm>()

	const onAdd = () => {
		push({
			name: '',
			description: '',
			// @ts-ignore
			key: formik.values.permissions.length + 1,
		})
	}

	const [t] = useTranslation()

	const roles = useMemo(() => {
		return formik.values.permissions
	}, [formik.values.permissions])

	const columns = useMemo<ColumnDescription<IPermissionItems>[]>(() => {
		return [
			{
				text: t('Name'),
				editable: true,
				dataField: 'name',
				formatExtraData: {
					// @ts-ignore
					value: (index: number) => formik.values.permissions[index]?.name,
					invalid: (index: number) =>
						// @ts-ignore
						formik.touched?.permissions?.[index]?.name &&
						// @ts-ignore
						formik.errors?.permissions?.[index]?.name,
				},
				formatter: (cell, row, rowIndex, formatExtraData) => {
					const fieldValue = formatExtraData.value(rowIndex)
					const fieldName = `permissions[${rowIndex}].name`
					return (
						<Fragment key={'name_' + rowIndex}>
							<Input
								name={fieldName}
								value={fieldValue}
								invalid={formatExtraData.invalid(rowIndex)}
								onChange={({ target }) => {
									formik.setFieldValue(fieldName, target.value ?? '')
								}}
							/>
						</Fragment>
					)
				},
			},
			{
				editable: true,
				text: t('Description'),
				dataField: 'description',
				formatExtraData: {
					value: (index: number) =>
						// @ts-ignore
						formik.values.permissions[index]?.description,
					invalid: (index: number) =>
						// @ts-ignore
						formik.touched?.permissions?.[index]?.description &&
						// @ts-ignore
						formik.errors?.permissions?.[index]?.description,
				},
				formatter: (cell, row, rowIndex, formatExtraData) => {
					const fieldValue = formatExtraData.value(rowIndex)
					const fieldName = `permissions[${rowIndex}].description`
					return (
						<Fragment key={'description_' + rowIndex}>
							<Input
								name={fieldName}
								value={fieldValue}
								invalid={formatExtraData.invalid(rowIndex)}
								onChange={({ target }) => {
									formik.setFieldValue(fieldName, target.value ?? '')
								}}
							/>
						</Fragment>
					)
				},
			},
			{
				dataField: '',
				text: t('Actions'),
				editable: false,
				isDummyField: true,
				formatter: (cell, row, rowIndex) => {
					return (
						<>
							<Button size='sm' type='button' color='danger' onClick={() => remove(rowIndex)}>
								<i className='fas fa-trash-alt' />
							</Button>
						</>
					)
				},
			},
		]
	}, [roles])

	return (
		<>
			<div className='add-role-actions mb-2 text-end'>
				<Button size='sm' type='button' color='success' onClick={onAdd}>
					<i className='fas fa-plus' />
				</Button>
			</div>
			<BootstrapTable
				keyField='key'
				columns={columns}
				// @ts-ignore
				data={formik.values.permissions}
				cellEdit={cellEditFactory({
					mode: 'click',
					blurToSave: true,
				})}
			/>
		</>
	)
}

export default PermissionsForm
