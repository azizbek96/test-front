import React, { FC, useMemo, CSSProperties, useLayoutEffect } from 'react'
// @ts-ignore
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter'
import { ColumnDescription } from 'react-bootstrap-table-next'
import { useDispatch, useSelector } from 'react-redux'
import Sweetalert from 'react-bootstrap-sweetalert'
import { Card, Button, CardBody } from 'reactstrap'

import {
	userPermissionsFetchAction,
	userPermissionsCreateStartAction,
	userPermissionsDeleteStartAction,
	userPermissionsUpdateStartAction,
} from '../../store/userPermissions/actions'
import type { IPagination, IUserPermissions, IPermissionsRole } from '../../store/userPermissions'
import Table from '../../components/Tables/RemoteTable'
import ModalForm from './ModalForm'
import { useTranslation } from 'react-i18next'

const UserPermissions: FC = (): JSX.Element => {
	const [deleteAlert, setDeleteAlert] = React.useState<any>(false)
	const TextFilter = textFilter({
		delay: 300,
	})
	const dispatch = useDispatch()

	useLayoutEffect(() => {
		dispatch(userPermissionsFetchAction('all'))
	}, [])
	const [t] = useTranslation()

	const { permissions, pagination, tableLoading, deletingRow, permissionsRole } = useSelector<
		{
			UserPermissions: IUserPermissions
		},
		{
			permissions: any
			formLoading: boolean
			tableLoading: boolean
			deletingRow: string | any
			pagination: IPagination | object
			permissionsRole: any
		}
	>(s => ({
		pagination: s.UserPermissions.pagination,
		permissions: s.UserPermissions.permissions,
		permissionsRole: s.UserPermissions.permissionsRole,
		formLoading: s.UserPermissions.formLoading,
		tableLoading: s.UserPermissions.tableLoading,
		deletingRow: s.UserPermissions.deletingRow,
	}))
	const handleUpdate = (name: string) => {
		dispatch(userPermissionsUpdateStartAction(name))
	}

	const handleDelete = (name: string) => {
		dispatch(userPermissionsDeleteStartAction(name))
	}

	const columns = useMemo<ColumnDescription[]>(() => {
		return [
			{
				text: '#',
				align: 'center',
				dataField: 'order',
				isDummyField: true,
				headerAlign: 'center',
				style: { width: '50px' },
				formatter: (cell, row, rowIndex) => rowIndex + 1,
			},
			{
				sort: true,
				text: t('Role'),
				dataField: 'name',
				filter: TextFilter,
			},
			{
				sort: true,
				text: t('Category'),
				dataField: 'category',
				filter: TextFilter,
			},
			{
				sort: true,
				text: t('Description'),
				dataField: 'description',
				filter: TextFilter,
			},
			{
				text: t('Actions'),
				dataField: 'actions',
				isDummyField: true,
				formatter: (cell, row) => {
					return (
						<>
							<div className='d-inline-flex gap-3'>
								<div className='d-inline-flex gap-3'>
									{
										permissionsRole?.update?(
											<Button color='success' className='employees-action-btn' onClick={() => handleUpdate(row.name)}>
												<i className='mdi mdi-pencil font-size-16' />
											</Button>
										):''
									}
									{
										permissionsRole?.delete?(
											<Button color='danger' className='employees-action-btn' onClick={() => setDeleteAlert(row.name)}>
												<i className='mdi mdi-delete font-size-16' />
											</Button>
										):''
									}
								</div>
							</div>
						</>
					)
				},
			},
		]
	}, [permissions, pagination, permissionsRole])

	return (
		<>
			<div className='page-content'>
				<ModalForm />
				<Card>
					<CardBody>
						<div className='d-flex mb-4 align-items-center justify-content-between'>
							<div className='h4 m-0 card-title'>{t('User Permissionsy')}</div>
							{permissionsRole?.create?(
								<Button type='button' color='success' onClick={() => dispatch(userPermissionsCreateStartAction())}>
									<i className='fas fa-plus' />
								</Button>
							):''}
						</div>
						{deleteAlert && (
							<Sweetalert
								info
								showCancel
								closeOnClickOutside
								title={t('Are you sure')}
								cancelBtnCssClass='btn btn-danger text-white'
								onCancel={() => {
									setDeleteAlert(false)
								}}
								onConfirm={() => {
									deleteAlert(deleteAlert)
									setDeleteAlert(false)
								}}
							/>
						)}
						<Table
							remote={true}
							keyField='key'
							columns={columns}
							data={permissions}
							pagination={pagination}
							loadingState={tableLoading}
							actionCallback={userPermissionsFetchAction}
							tableProps={{
								filter: filterFactory(),
								rowStyle: (row: any) => {
									const styles: CSSProperties = {}

									if (row.name === deletingRow) {
										styles.backgroundColor = 'rgba(66,66,66,0.5)'
									}

									return styles
								},
							}}
						/>
					</CardBody>
				</Card>
			</div>
		</>
	)
}

export default UserPermissions
