type IPermissionItems = Array<{ name: string; description: string; key: number }>

interface IPermissionsForm {
	name: string
	category: string
	description?: string
	permissions?: IPermissionItems
}

export type { IPermissionsForm, IPermissionItems }
