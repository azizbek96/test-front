import { Form, Modal, Label, Input, Button, FormGroup, ModalBody, ModalHeader, FormFeedback } from 'reactstrap'
import { useFormik, FieldArray, FormikProvider } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import React, { FC, useState, useEffect } from 'react'
import { find, isEmpty } from 'lodash'
import * as Yup from 'yup'

import { userPermissionsModalSetAction, userPermissionsCreateSendAction, userPermissionsUpdateSendAction } from '../../store/userPermissions/actions'
import { IUserPermissions } from '../../store/userPermissions'
import AsyncSearch from '../../components/Select/AsyncSearch'
import FormLoader from '../../components/Loaders/FormLoader'
import PermissionsForm from './PermissionsForm'
import { IPermissionsForm } from './types'

const ModalForm: FC = (): JSX.Element => {
	const dispatch = useDispatch()

	const [t] = useTranslation()

	const { isOpen, categories, isUpdating, formLoading, updatingValues } = useSelector<
		{ UserPermissions: IUserPermissions },
		{
			isOpen: boolean
			isUpdating: boolean
			categories: object[]
			formLoading: boolean
			updatingValues: IPermissionsForm
		}
	>(s => ({
		isOpen: s.UserPermissions.modal,
		categories: s.UserPermissions.category,
		formLoading: s.UserPermissions.formLoading,
		// @ts-ignore
		updatingValues: s.UserPermissions.updatingValues,
		isUpdating: !isEmpty(s.UserPermissions.updatingValues),
	}))

	const formik = useFormik<IPermissionsForm>({
		enableReinitialize: false,
		initialValues: isUpdating
			? {
					name: '',
					category: '',
					description: '',
			}
			: {
					name: '',
					category: '',
					permissions: [{ description: '', name: '', key: 1 }],
			},
		validationSchema: Yup.object(
			isUpdating
				? {
						name: Yup.string().required(),
						category: Yup.string().required(),
						description: Yup.string().required(),
				}
				: {
						name: Yup.string().required(),
						category: Yup.string().required(),
						permissions: Yup.array(
							Yup.object({
								description: Yup.string(),
								name: Yup.string().required(),
							})
						),
				}
		),
		onSubmit: values => {
			const action = isUpdating ? userPermissionsUpdateSendAction : userPermissionsCreateSendAction

			dispatch(action(values))
		},
	})

	useEffect(() => {
		if (!isEmpty(updatingValues)) {
			formik.setFieldValue('name', updatingValues.name)
			formik.setFieldValue('category', updatingValues.category)
			formik.setFieldValue('description', updatingValues.description)
		}
	}, [updatingValues])

	const [categoryLoading, setCategoryLoading] = useState<boolean>(false)
	const [searchTimeOut, setSearchTimeOut] = useState<any>(false)

	const searchCategory = (searchText: string): Promise<any> => {
		return new Promise((resolve, reject) => {
			try {
				if (searchTimeOut) clearTimeout(searchTimeOut)
				setCategoryLoading(true)

				const cb = (value: string) => {
					setCategoryLoading(false)
				}

				if (!isEmpty(searchText)) {
					setSearchTimeOut(setTimeout(cb, 300, searchText))
				} else {
					return resolve([])
				}
			} catch (error) {
				return reject(error)
			}
		})
	}

	return (
		<>
			<Modal
				centered
				size='lg'
				isOpen={isOpen}
				// style={{ maxWidth: '650px' }}
				onClosed={() => {
					formik.resetForm()
				}}
			>
				<FormLoader loading={formLoading} />
				<ModalHeader toggle={() => dispatch(userPermissionsModalSetAction(false))}>{t(' Roll')}</ModalHeader>
				<ModalBody>
					<Form
						autoComplete='off'
						onSubmit={e => {
							e.preventDefault()
							formik.handleSubmit(e)
						}}
					>
						<FormikProvider value={formik}>
							<FormGroup>
								<Label htmlFor='role-name'>{t('Roll name')}</Label>
								<Input
									type='text'
									name='name'
									id='role-name'
									value={formik.values.name}
									onBlur={formik.handleBlur}
									onChange={formik.handleChange}
									placeholder={t('Enter Roll name')}
									invalid={!!(formik.touched.name && formik.errors.name)}
								/>
								{formik.touched.name && formik.errors.name && <FormFeedback>{formik.errors.name}</FormFeedback>}
							</FormGroup>
							<FormGroup>
								{isUpdating ? (
									<>
										<Label htmlFor='role-description'>{t('Description')}</Label>
										<Input
											type='textarea'
											name='description'
											id='role-description'
											onBlur={formik.handleBlur}
											onChange={formik.handleChange}
											value={formik.values.description}
											placeholder={t('Enter Description')}
											invalid={!!(formik.touched.description && formik.errors.description)}
										/>
										{formik.touched.description && formik.errors.description && <FormFeedback>{formik.errors.description}</FormFeedback>}
									</>
								) : (
									<FieldArray name='permissions'>
										{({ push, remove }) => {
											return <PermissionsForm push={push} remove={remove} />
										}}
									</FieldArray>
								)}
							</FormGroup>
							<FormGroup>
								<Label htmlFor='category-select'>{t('Category')}</Label>
								<AsyncSearch
									name='category'
									id='category-select'
									closeMenuOnSelect={true}
									isLoading={categoryLoading}
									defaultOptions={categories}
									loadOptions={searchCategory}
									errorMessage={formik.errors.category}
									invalid={!!(formik.touched.category && formik.errors.category)}
									value={find<any>(categories, c => c.value == formik.values.category)}
									onBlur={() => {
										formik.setFieldTouched('category', true)
									}}
									onChange={(selected: any) => {
										formik.setFieldValue('category', selected.value)
									}}
								/>
								{formik.touched.category && formik.errors.category && <FormFeedback>{formik.errors.category}</FormFeedback>}
							</FormGroup>
						</FormikProvider>
						<Button type='submit' color='success'>
							{t('Save')}
						</Button>
					</Form>
				</ModalBody>
			</Modal>
		</>
	)
}

export default ModalForm
