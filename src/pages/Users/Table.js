import React, { memo, useMemo, useState, useLayoutEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Sweetalert from 'react-bootstrap-sweetalert'
import { useTranslation } from 'react-i18next'
import { Button } from 'reactstrap'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter'

import { usersDelete, getUsersList, usersStartUpdate,setModal } from '../../store/users/actions'
import Table from '../../components/Tables/RemoteTable'
import {NavLink} from "react-router-dom";

const UsersTable = () => {
	const TextFilter = textFilter({
		delay: 300,
	})
	const dispatch = useDispatch()

	useLayoutEffect(() => {
		dispatch(getUsersList(1))
	}, [])

	const {
		users,
		pagination,
		permissions,
		statusList,
		isLoading: loadingState,
	} = useSelector(s => ({
		users: s.Users.users,
		isLoading: s.Users.isLoading,
		pagination: s.Users.pagination,
		permissions: s.Users.permissions,
		statusList: s.Users.statusList,
	}))

	const [deleteAlert, setDeleteAlert] = useState(false)

	const deleteHandler = id => {
		dispatch(usersDelete(id))
	}

	const updateHandler = id => {
		dispatch(usersStartUpdate(id))
		dispatch(setModal(true))
	}

	const [t] = useTranslation()

	/**
	 * @type {import('react-bootstrap-table-next').ColumnDescription[]}
	 */
	const columns = useMemo(() => {
		return [
			{
				text: '#',
				dataField: '#',
				align: 'center',
				isDummyField: true,
				headerAlign: 'center',
				style: { width: '50px' },
				formatter() {
					return arguments[2] + 1
				},
			},
			{
				sort: true,
				text: t('Username'),
				dataField: 'username',
				filter: TextFilter,
			},
			{
				sort: true,
				text: t('Employee'),
				dataField: 'hr_employee_id',
				filter: TextFilter,
			},
			{
				sort: true,
				text: t('Status'),
				dataField: 'status',
				formatter: cell => {
					return (
						<>
							<span
								className={classNames('badge', 'font-size-12', {
									'bg-danger': cell === 0,
									'bg-success': cell === 1,
									'bg-warning': cell === 2,
								})}
							>
								{t(statusList[cell])}
							</span>
						</>
					)
				},
			},
			{
				text: t('Actions'),
				isDummyField: true,
				dataField: 'action',
				style: {
					minWidth: '50px',
				},
				formatter(cell, row) {
					return (
						<div className='d-inline-flex gap-3'>
							{
								permissions?.update?(
									<Button size='sm' color='success' onClick={() => updateHandler(row.id)}>
										<i id='edittooltip' className='mdi mdi-pencil font-size-16' />
									</Button>
								):''
							}
							{
								permissions?.delete?(
									<Button size='sm' color='danger' onClick={() => setDeleteAlert(row.id)}>
										<i id='deletetooltip' className='mdi mdi-delete font-size-16' />
									</Button>
								):""
							}
						</div>
					)
				},
			},
		]
	}, [statusList, t, users, permissions])

	return (
		<>
			{deleteAlert && (
				<Sweetalert
					info
					showCancel
					closeOnClickOutside
					title={t('Are you sure')}
					cancelBtnCssClass='btn btn-danger text-white'
					onCancel={() => {
						setDeleteAlert(false)
					}}
					onConfirm={() => {
						deleteHandler(deleteAlert)
						setDeleteAlert(false)
					}}
				/>
			)}
			<div className='d-flex mb-4 align-items-center justify-content-between'>
				<div className='h4 m-0 card-title'>{t('Users')}</div>
				{
					permissions?.create?(
						<Button onClick={() => dispatch(setModal(true))} className='btn btn-success text-white'>
							<i className='fas fa-plus' />
						</Button>
					):''
				}
			</div>
			<Table 
				remote
				data={users}
				keyField='key'
				columns={columns}
				pagination={pagination}
				loadingState={loadingState}
				actionCallback={getUsersList}
				tableProps={{
					filter: filterFactory(),
				}} />
		</>
	)
}

UsersTable.propTypes = {
	setEdit: PropTypes.func,
}

export default memo(UsersTable)
