import { Card, Button, CardBody } from 'reactstrap'
import { useTranslation } from 'react-i18next'
import React, { memo, useState } from 'react'

import ModalForm from './ModalForm'
import Table from './Table'
import {useDispatch, useSelector} from "react-redux";
import { setModal } from '../../store/users/actions'

const Users = () => {
	const [t] = useTranslation()
	const dispatch = useDispatch()

	return (
		<div className='page-content'>
			<ModalForm />
			<Card>
				<CardBody>
					<Table />
				</CardBody>
			</Card>
		</div>
	)
}

export default memo(Users)
