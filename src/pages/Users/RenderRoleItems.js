import { Card, Button, CardBody, ButtonGroup } from 'reactstrap'
import { Fragment } from 'react'

const RenderRoleItems = ({ roles, deleteHandler }) => {
	return (
		<Card
			style={{
				border: '1px solid #ced4da',
			}}
		>
			<CardBody className='p-2 d-inline-flex flex-wrap gap-1 font-size-16'>
				{Array.isArray(roles) &&
					roles.map((role, index) => {
						return (
							<Fragment key={index}>
								<ButtonGroup size='sm'>
									<Button type='button' color='secondary' style={{ pointerEvents: 'none' }}>
										{role.label ?? role.value}
									</Button>
									<Button type='button' color='secondary' onClick={() => deleteHandler(role.value)}>
										X
									</Button>
								</ButtonGroup>
							</Fragment>
						)
					})}
			</CardBody>
		</Card>
	)
}

export default RenderRoleItems
