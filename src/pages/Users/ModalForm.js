import { Col, Row, Form, Input, Label, Modal, Button, Spinner, ModalBody, ModalHeader, FormFeedback } from 'reactstrap'
import React, { memo, useState, useEffect, useCallback, useLayoutEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import _, { filter, isEmpty } from 'lodash'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import * as Yup from 'yup'

import { usersUpdate, sendCreateUser, usersUpdateEnd, fetchEmployeesAction, usersUpdatingValuesSet, setModal } from '../../store/users/actions'
import RequiredSelect from '../../components/Select/RequiredSelect'
import { USER_ROLE_SEARCH_URL } from '../../helpers/url_helper'
import AsyncSearch from '../../components/Select/AsyncSearch'
import RenderRoleItems from './RenderRoleItems'
import { api } from '../../helpers/api_helper'

const ModalForm = ( ) => {
	const dispatch = useDispatch()

	const { roles, errors, message, employees, errorType, isUpdating, formLoading, updatingValues, isOpen } = useSelector(s => ({
		roles: s.Users.roles,
		users: s.Users.users,
		isOpen: s.Users.isModal,
		errors: s.Users.errors,
		message: s.Users.result,
		employees: s.Users.employees,
		errorType: s.Users.errorType,
		roleTypes: s.Users.roleTypes,
		isUpdating: s.Users.isUpdating,
		formLoading: s.Users.formLoading,
		updatingValues: s.Users.updatingValues,
	}))
	const formik = useFormik({
		enableReinitialize: true,
		initialValues: isUpdating
			? {
					roles: [],
					username: '',
					hr_employee_id: '',
			}
			: {
					roles: [],
					username: '',
					password: '',
					hr_employee_id: '',
					confirm_password: '',
			},
		validationSchema: Yup.object(
			isUpdating
				? {
						id: Yup.number().required(),
						username: Yup.string().min(3, 'Too Short!').max(16, 'Too Long!').required('Usernameni kiritish shart'),
						hr_employee_id: Yup.string().required('Hodimni tanlash shart'),
				}
				: {
						username: Yup.string().min(3, 'Too Short!').max(16, 'Too Long!').required('Usernameni kiritish shart'),
						hr_employee_id: Yup.string().required('Hodimni tanlash shart'),
						password: Yup.string().required('Parolni kiritish shart').min(1, 'Too Short!'),
						confirm_password: Yup.string().when('password', (password, field) => {
							return formik.values.password && formik.values.confirm_password ? field.oneOf([Yup.ref('password')], 'Parolni takrorlang').required() : field
						}),
						roles: Yup.array(
							Yup.object({
								key: Yup.string(),
								value: Yup.string(),
							})
						).required(),
				}
		),
		onSubmit: values => {
			if (isUpdating) {
				dispatch(
					usersUpdate({
						...values,
						roles: [...values.roles, ...updatingValues.userRoles],
					})
				)
			} else {
				dispatch(sendCreateUser(values))
			}
		},
	})

	const [t] = useTranslation()

	useLayoutEffect(() => {
		if (isOpen && !isUpdating) {
			dispatch(fetchEmployeesAction())
		}
	}, [isOpen])


	useEffect(() => {
		if (!_.isEmpty(message)) {
			if (errorType === 'SUCCESS') {
				formik.resetForm()
				toast.success(message, { autoClose: 2000, theme: 'colored' })
			} else {
				formik.setErrors({
					username: errors?.username?.[0],
					confirm_password: errors?.confirm_password?.[0],
				})
				toast.error(message, { autoClose: 2000, theme: 'colored' })
			}
		}
	}, [message])

	useEffect(() => {
		if (!_.isEmpty(updatingValues)) {
			formik.setFieldValue('id', updatingValues.id)
			formik.setFieldValue('username', updatingValues.username)
			formik.setFieldValue('roles', updatingValues.roles ?? [])
			formik.setFieldValue('hr_employee_id', updatingValues.hr_employee_id)
		}
	}, [updatingValues])

	const [searchTimeOut, setSearchTimeOut] = useState(false)
	const [selectLoading, setSelectLoading] = useState(false)

	const searchRoles = useCallback(async value => {
		return new Promise((resolve, reject) => {
			try {
				if (searchTimeOut) clearTimeout(searchTimeOut)
				setSelectLoading(true)

				const cb = async val => {
					const params = new URLSearchParams()

					if (isUpdating) {
						params.append('user_id', updatingValues.id)
					}

					params.append('name', val)

					const { data } = await api.get(`${USER_ROLE_SEARCH_URL}?${params}`)
					resolve(data)
					setSelectLoading(false)
				}

				if (!_.isEmpty(value)) {
					setSearchTimeOut(setTimeout(cb, 300, value))
				} else {
					return resolve(roles)
				}
			} catch (error) {
				return reject(error)
			}
		})
	}, [])

	const handleRoleDelete = name => {
		const found = filter(updatingValues.userRoles, role => {
			return role.value !== name
		})

		dispatch(usersUpdatingValuesSet({ ...updatingValues, userRoles: found }))
	}

	return (
		<Modal
			centered
			size='xl'
			scrollable
			isOpen={isOpen}
			unmountOnClose={true}
			onClosed={() => {
				dispatch(setModal(false))
				formik.resetForm()
			}}
		>
			{formLoading && (
				<div className='form-loading'>
					<Spinner />
				</div>
			)}
			<ModalHeader
				toggle={() => {
					dispatch(setModal(false))
				}}
			>
				{t('User')}
			</ModalHeader>
			<ModalBody>
				<Form
					autoComplete='off'
					onSubmit={e => {
						e.preventDefault()
						formik.handleSubmit()
					}}
				>
					<Row className='mb-3' style={{ rowGap: '1em' }}>
						<Col md={6}>
							<Label htmlFor='hr_employee_id'>{t('Employee')}</Label>
							<RequiredSelect
								isClearable
								options={employees}
								id='hr_employee_id'
								name='hr_employee_id'
								classNamePrefix='select2-selection'
								errorMessage={formik.errors.hr_employee_id}
								onBlur={() => formik.setFieldTouched('hr_employee_id', true)}
								value={_.find(employees, p => p.value === formik.values.hr_employee_id)}
								invalid={!!(formik.touched.hr_employee_id && formik.errors.hr_employee_id)}
								onChange={e => formik.setFieldValue('hr_employee_id', e?.value ?? formik.initialValues.hr_employee_id)}
							/>
						</Col>
						<Col md={6}>
							<Label htmlFor='form-row-username-input'>{t('Username')}</Label>
							<Input
								type='text'
								name='username'
								className='form-control'
								onBlur={formik.handleBlur}
								id='form-row-username-input'
								value={formik.values.username}
								onChange={formik.handleChange}
								placeholder={t('Enter Your Username')}
								invalid={!!(formik.touched.username && formik.errors.username)}
							/>
							{!!(formik.touched.username && formik.errors.username) && <FormFeedback type='invalid'>{formik.errors.username}</FormFeedback>}
						</Col>
						{!isUpdating && (
							<>
								<Col md={6}>
									<Label htmlFor='form-row-password-input'>{t('Password')}</Label>
									<Input
										type='password'
										name='password'
										className='form-control'
										onBlur={formik.handleBlur}
										id='form-row-password-input'
										value={formik.values.password}
										onChange={formik.handleChange}
										placeholder='Enter Your Password'
										invalid={!!(formik.touched.password && formik.errors.password)}
									/>
									{!!(formik.touched.password && formik.errors.password) && <FormFeedback type='invalid'>{formik.errors.password}</FormFeedback>}
								</Col>
								<Col md={6}>
									<Label htmlFor='form-row-confirm-input'>{t('Confirm Password')}</Label>
									<Input
										type='password'
										name='confirm_password'
										className='form-control'
										onBlur={formik.handleBlur}
										id='form-row-confirm-input'
										onChange={formik.handleChange}
										placeholder={t('Reenter password')}
										disabled={!formik.values.password}
										value={formik.values.confirm_password}
										invalid={!!(formik.touched.password && formik.errors.confirm_password)}
									/>
									{!!(formik.touched.confirm_password && formik.errors.confirm_password) && <FormFeedback type='invalid'>{formik.errors.confirm_password}</FormFeedback>}
								</Col>
							</>
						)}
					</Row>
					<hr />
					<Row>
						<Col md={12}>
							{isUpdating && !isEmpty(updatingValues.userRoles) && <RenderRoleItems roles={updatingValues.userRoles} deleteHandler={handleRoleDelete} />}
							<AsyncSearch
								isMulti
								defaultOptions={roles}
								isLoading={selectLoading}
								loadOptions={searchRoles}
								onChange={values => {
									formik.setFieldValue('roles', values)
								}}
							/>
						</Col>
					</Row>
					<div className='mt-3 text-end'>
						<Button type='submit' color='success'>
							{t('Save')}
						</Button>
					</div>
				</Form>
			</ModalBody>
		</Modal>
	)
}
export default memo(ModalForm)
