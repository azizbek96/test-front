import { Form, Input, Label, Modal, Button, Spinner, FormGroup, ModalBody, ModalHeader, FormFeedback } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import _, { find } from 'lodash'
import * as Yup from 'yup'

import { userRoleUpdateAction, setUserRoleUpdatedData, createOrUpdateUserRoles } from '../../store/userRoles/actions'
import RequiredSelect from '../../components/Select/RequiredSelect'

const ModalForm = ({ setModal, modal }) => {
	const dispatch = useDispatch()

	const { role, roleName, options, formLoading, updatingValues } = useSelector(s => ({
		role: s.UserRoles.result,
		roles: s.UserRoles.roles,
		options: s.UserRoles.roleTypes,
		roleName: s.UserRoles.roleName,
		formLoading: s.UserRoles.formLoading,
		updatingValues: s.UserRoles.updatingValues,
	}))

	const formik = useFormik({
		validateOnChange: false,
		enableReinitialize: true,
		initialValues: {
			name: '',
			name_for_user: '',
		},
		validationSchema: Yup.object({
			name: Yup.string().required(),
			name_for_user: Yup.string().required(),
		}),
		onSubmit: values => {
			values.old_name = updatingValues.name
			if (_.isEmpty(updatingValues)) {
				dispatch(createOrUpdateUserRoles(values))
			} else {
				dispatch(userRoleUpdateAction(values))
			}
		},
	})

	const [t] = useTranslation()

	useEffect(() => {
		if (roleName) {
			setModal(true)
		}
	}, [roleName])

	useEffect(() => {
		if (!_.isEmpty(role)) {
			formik.setValues({
				...role,
			})
		}
	}, [role])

	useEffect(() => {
		if (!_.isEmpty(updatingValues)) {
			formik.setValues(updatingValues)
		}
	}, [updatingValues])

	return (
		<Modal
			centered
			size='lg'
			isOpen={modal}
			onClosed={() => {
				formik.resetForm()
			}}
		>
			{formLoading && (
				<div className='form-loading'>
					<Spinner />
				</div>
			)}
			<ModalHeader
				toggle={() => {
					if (roleName) {
						const data = { role: {}, roleName: null }
						dispatch(setUserRoleUpdatedData(data))
						formik.resetForm()
					}
					setModal(false)
				}}
			>
				{t('User Role')}
			</ModalHeader>
			<ModalBody>
				<Form
					autoComplete='off'
					onSubmit={e => {
						e.preventDefault()
						formik.handleSubmit()
					}}
				>
					<FormGroup>
						<Label htmlFor='form-row-name-input'>{t('Name')}</Label>
						<Input
							name='name'
							type='text'
							id='form-row-name-input'
							className='form-control'
							value={formik.values.name}
							onBlur={formik.handleBlur}
							onChange={formik.handleChange}
							placeholder={t('Ener Ytour Role Name')}
							invalid={!!(formik.touched.name && formik.errors.name)}
						/>
						{!!(formik.touched.name && formik.errors.name) && <FormFeedback type='invalid'>{formik.errors.name}</FormFeedback>}
					</FormGroup>
					<FormGroup>
						<Label htmlFor='form-row-name_for_user-input'>{t('Name For User')}</Label>
						<Input
							type='text'
							name='name_for_user'
							className='form-control'
							onBlur={formik.handleBlur}
							onChange={formik.handleChange}
							id='form-row-name_for_user-input'
							value={formik.values.name_for_user}
							placeholder={t('Enter Your Role Name For User')}
							invalid={!!(formik.touched.name_for_user && formik.errors.name_for_user)}
						/>
						{!!(formik.touched.name_for_user && formik.errors.name_for_user) && <FormFeedback type='invalid'>{formik.errors.name_for_user}</FormFeedback>}
					</FormGroup>

					<div className='mb-5'></div>
					<div className='mt-3 text-end'>
						<Button type='submit' color='success'>
							{t('Save')}
						</Button>
					</div>
				</Form>
			</ModalBody>
		</Modal>
	)
}

ModalForm.propTypes = {
	modal: PropTypes.bool.isRequired,
	setModal: PropTypes.func.isRequired,
}

export default ModalForm
