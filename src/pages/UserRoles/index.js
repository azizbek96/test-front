import React, { memo, useMemo, useEffect, useLayoutEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Card, Button, CardBody } from 'reactstrap'
import { useTranslation } from 'react-i18next'
import { isNull } from 'lodash'
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter'
import Sweetalert from 'react-bootstrap-sweetalert'

import { fetchUserRoleList, userRoleModalStateSet, setUserRoleUpdatedData, userRoleDeleteStartAction, userRoleUpdateStartAction } from '../../store/actions'
import Table from '../../components/Tables/RemoteTable'
import ModalForm from './ModalForm'
import {NavLink} from "react-router-dom";

const UserRoles = () => {
	const [deleteAlert, setDeleteAlert] = useState(false)
	const TextFilter = textFilter({
		delay: 300,
	})
	const dispatch = useDispatch()
	useLayoutEffect(() => {
		dispatch(fetchUserRoleList(1))
	}, [])

	const [t] = useTranslation()

	const { roles, pagination, isLoading, closeModal, deletingRole, modal,permissions ,roleTypes} = useSelector(s => ({
		roles: s.UserRoles.roles,
		modal: s.UserRoles.modalState,
		isLoading: s.UserRoles.loading,
		closeModal: s.UserRoles.closeModal,
		pagination: s.UserRoles.pagination,
		permissions: s.UserRoles.permissions,
		deletingRole: s.UserRoles.deleting,
		roleTypes: s.UserRoles.roleTypes,
	}))
	const deleteHandler = name => {
		dispatch(userRoleDeleteStartAction(name))
	}

	const updateHandler = name => {
		dispatch(userRoleUpdateStartAction(name))
	}

	useEffect(() => {
		if (closeModal) {
			userRoleModalStateSet(false)
		}
	}, [closeModal])

	const columns = useMemo(() => {
		return [
			{
				text: '#',
				align: 'center',
				dataField: 'order',
				isDummyField: true,
				headerAlign: 'center',
				style: { width: '50px' },
				formatter: (cell, row, rowIndex) => rowIndex + 1,
			},
			{
				sort: true,
				dataField: 'name',
				text: t('Name'),
				filter: TextFilter,
			},
			{
				sort: true,
				dataField: 'name_for_user',
				text: t('Name'),
				filter: TextFilter,
			},
			{
				sort: true,
				text:t('Role type'),
				filter: TextFilter,
				dataField: 'role_type',
				formatter(cell, row) {
					return (
						<div className='d-inline-flex gap-3'>
							{
								(roleTypes.length>0)?(
									<span >
										{roleTypes.find(item=>+item.value === +cell)?.label}
									</span>
								):''
							}
						</div>
					)
				},
			},
			{
				text: t('Actions'),
				isDummyField: true,
				dataField: 'action',
				formatter(cell, row) {
					return (
						<div className='d-inline-flex gap-3'>
							{
								permissions?.update?(
									<>
										{/*<NavLink to={{pathname: '/user-roles/update-per', state: {name: row.name}}} className='btn btn-outline-info'>*/}
										{/*	{t('Permissions')}*/}
										{/*</NavLink>*/}
										<Button size='sm' color='success' onClick={() => updateHandler(row.name)}>
											<i id='edittooltip' className='mdi mdi-pencil font-size-16'></i>
										</Button>
									</>
								):''
							}
							{
								permissions?.delete?(
									<Button size='sm' color='danger' onClick={() => setDeleteAlert(row.name)}>
										<i id='deletetooltip' className='mdi mdi-delete font-size-16'></i>
									</Button>
								):''
							}
						</div>
					)
				},
			},
		]
	}, [permissions])

	return (
		<div className='page-content'>
			<ModalForm modal={modal} setModal={state => dispatch(userRoleModalStateSet(state))} />
			<Card>
				<CardBody>
					<div className='d-flex mb-4 align-items-center justify-content-between'>
						<div className='h4 m-0 card-title'>{t('User Role')}</div>
						{permissions?.create?(
							<Button type='button' color='success' onClick={() => dispatch(userRoleModalStateSet(true))}>
								<i className='fas fa-plus' />
							</Button>
						):""}
					</div>
					{deleteAlert && (
				<Sweetalert
					info
					showCancel
					closeOnClickOutside
					title={t('Are you sure')}
					cancelBtnCssClass='btn btn-danger text-white'
					onCancel={() => {
						setDeleteAlert(false)
					}}
					onConfirm={() => {
						deleteHandler(deleteAlert)
						setDeleteAlert(false)
					}}
				/>
			)}
					<Table
						remote={{ sort: true, pagination: true, filter: true }}
						data={roles}
						keyField='key'
						columns={columns}
						pagination={pagination}
						loadingState={isLoading}
						actionCallback={fetchUserRoleList}
						sortAction={fetchUserRoleList}
						filterAction={fetchUserRoleList}
						tableProps={{
							filter: filterFactory(),
						}}
						// tableProps={{
						// 	rowStyle: !isNull(deletingRole)
						// 		? {
						// 				backgroundColor: 'rgba(66,66,66,0.5)',
						// 		  }
						// 		: {},
						// }}
					/>
				</CardBody>
			</Card>
		</div>
	)
}

export default memo(UserRoles)
