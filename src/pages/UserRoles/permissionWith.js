import { Form, Input, Label, Modal, Button, Spinner, FormGroup, ModalBody, ModalHeader, FormFeedback } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import _, { find } from 'lodash'
import * as Yup from 'yup'

import { userRoleUpdateAction, setUserRoleUpdatedData, createOrUpdateUserRoles } from '../../store/userRoles/actions'
import RequiredSelect from '../../components/Select/RequiredSelect'

const ModalForm = ({ setModal, modal }) => {
    const dispatch = useDispatch()

    const { role, roleName, options, formLoading, updatingValues } = useSelector(s => ({
        role: s.UserRoles.result,
        roles: s.UserRoles.roles,
        options: s.UserRoles.roleTypes,
        roleName: s.UserRoles.roleName,
        formLoading: s.UserRoles.formLoading,
        updatingValues: s.UserRoles.updatingValues,
    }))

    const formik = useFormik({
        validateOnChange: false,
        enableReinitialize: true,
        initialValues: {
            name: '',
            name_for_user: '',
        },
        validationSchema: Yup.object({
            name: Yup.string().required(),
            name_for_user: Yup.string().required(),
        }),
        onSubmit: values => {
            values.old_name = updatingValues.name
            if (_.isEmpty(updatingValues)) {
                dispatch(createOrUpdateUserRoles(values))
            } else {
                dispatch(userRoleUpdateAction(values))
            }
        },
    })

    const [t] = useTranslation()

    useEffect(() => {
        if (roleName) {
            setModal(true)
        }
    }, [roleName])

    useEffect(() => {
        if (!_.isEmpty(role)) {
            formik.setValues({
                ...role,
            })
        }
    }, [role])

    useEffect(() => {
        if (!_.isEmpty(updatingValues)) {
            formik.setValues(updatingValues)
        }
    }, [updatingValues])

    return (
                <Form
                    autoComplete='off'
                    onSubmit={e => {
                        e.preventDefault()
                        formik.handleSubmit()
                    }}
                >
                    {/*<FormGroup>*/}

                    {/*</FormGroup>*/}
                    {/*<div className='mb-5'></div>*/}
                    {/*<div className='mt-3 text-end'>*/}
                    {/*    <Button type='submit' color='success'>*/}
                    {/*        {t('Save')}*/}
                    {/*    </Button>*/}
                    {/*</div>*/}
                </Form>
    )
}

ModalForm.propTypes = {
    modal: PropTypes.bool.isRequired,
    setModal: PropTypes.func.isRequired,
}

export default ModalForm
