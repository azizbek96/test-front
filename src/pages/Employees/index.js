import { Card, CardBody, CardHeader } from 'reactstrap'
import { NavLink } from 'react-router-dom'

import EmployeesTable from './Table'
import { memo } from 'react'
const Employees = () => {
	return (
		<>
			<Card>
				<CardBody>
					<EmployeesTable />
				</CardBody>
			</Card>
		</>
	)
}

export default memo(Employees)
