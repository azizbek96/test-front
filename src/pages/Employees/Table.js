import filterFactory, {textFilter, customFilter, FILTER_TYPES} from 'react-bootstrap-table2-filter'
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css'
import React, {memo, useMemo, useState, useCallback, useLayoutEffect} from 'react'
import {BiFilter, BiSortUp, BiSortDown} from 'react-icons/bi'
import {useDispatch, useSelector} from 'react-redux'
import SweetAlert from 'react-bootstrap-sweetalert'
import {useTranslation} from 'react-i18next'
import {NavLink, useHistory} from 'react-router-dom'
import {Button} from 'reactstrap'

import {fetchEmployeesReq, employeesDelete, employeesIsUpdating} from '../../store/employees/actions'
import CustomFilter from '../../components/CustomFilter'
import Table from '../../components/Tables/RemoteTable'

const TextFilter = textFilter({
    delay: 300,
})

const EmployeesTable = () => {
    const [t] = useTranslation()
    const history = useHistory()
    const dispatch = useDispatch()

    useLayoutEffect(() => {
        dispatch(fetchEmployeesReq(1, '', '', {}))
        dispatch(employeesIsUpdating(false))
    }, [])

    const {
        employees,
        pagination,
        permissions,
        isLoading: loadingState,
    } = useSelector(s => ({
        employees: s.Employees.employees,
        isLoading: s.Employees.isLoading,
        pagination: s.Employees.pagination,
        permissions: s.Employees.permissions,
    }))
    const [deleteAlert, setDeleteAlert] = useState(null)

    const deleteHandler = useCallback(id => {
        dispatch(employeesDelete(id))
    }, [])

    const updateHandler = id => {
        history.push({pathname: '/employees/modal', state: {id: id}})
    }

    /**
     * @type {({formatter(): *, dataField: string, headerAlign: string, isDummyField: boolean, style: {width: string}, text: string, align: string}|{filter: , dataField: string, sort: boolean, text: TFuncReturn<"translation", string, string, undefined>}|{filter: , dataField: string, sort: boolean, text: TFuncReturn<"translation", string, string, undefined>}|{filter: , dataField: string, sort: boolean, text: TFuncReturn<"translation", string, string, undefined>}|{filter: , dataField: string, sort: boolean, text: TFuncReturn<"translation", string, string, undefined>})[]}
     */
    const columns = useMemo(() => {
        return [
            {
                text: '#',
                align: 'center',
                dataField: 'index',
                isDummyField: true,
                headerAlign: 'center',
                style: {
                    width: '50px',
                },
                formatter() {
                    return arguments[2] + 1
                },
            },
            {
                sort: true,
                text: t('Name'),
                filter: TextFilter,
                dataField: 'first_name',
            },
            {
                sort: true,
                filter: TextFilter,
                text: t('Last name'),
                dataField: 'last_name',
            },
            {
                sort: true,
                filter: TextFilter,
                text: t('Father name'),
                dataField: 'father_name',
            },
            {
                sort: true,
                text: t('Address'),
                filter: TextFilter,
                dataField: 'address',
            },
            {
                text: t('Actions'),
                isDummyField: true,
                dataField: 'action',
                formatter(cell, row) {
                    return (
                        <div className='d-inline-flex gap-3'>
                            {/* <Link to={'/employees/' + row.id}>
								<Button size='sm' color='primary' className='employees-action-btn'>
									<i className='mdi mdi-eye font-size-16' />
								</Button>
							</Link> */}
                            {
                                permissions?.update?(
                                    <Button
                                        size='sm'
                                        color='success'
                                        className='employees-action-btn'
                                        onClick={() => updateHandler(row.id)}
                                    >
                                        <i className='mdi mdi-pencil font-size-16'/>
                                    </Button>
                                ):''
                            }
                            {
                                permissions?.delete?(
                                    <Button
                                        size='sm'
                                        color='danger'
                                        className='employees-action-btn'
                                        onClick={() => setDeleteAlert(row.id)}
                                    >
                                        <i className='mdi mdi-delete font-size-16'/>
                                    </Button>
                                ):''
                            }
                        </div>
                    )
                },
            },
        ].map(col => {
            if (col.sort) {
                col.sortCaret = order => {
                    if (!order)
                        return (
                            <span>
								&nbsp;&nbsp;
                                <BiFilter size={20}/>
							</span>
                        )
                    else if (order === 'asc')
                        return (
                            <span>
								&nbsp;&nbsp;
                                <BiSortDown size={20}/>
							</span>
                        )
                    else if (order === 'desc')
                        return (
                            <span>
								&nbsp;&nbsp;
                                <BiSortUp size={20}/>
							</span>
                        )
                    return null
                }
            }
            return col
        })
    }, [t,permissions])

    return (
        <>
            {deleteAlert && (
                <SweetAlert
                    info
                    showCancel
                    title={t('Are you sure')}
                    closeOnClickOutside={false}
                    onCancel={() => setDeleteAlert(null)}
                    onConfirm={() => {
                        deleteHandler(deleteAlert)
                        setDeleteAlert(null)
                    }}
                />
            )}
            <div className='text-end px-5'>
                {
                    permissions?.create?(
                        <NavLink to={{pathname: '/employees/modal', state: {id: null}}} className='btn btn-success'>
                            <i className='fas fa-plus' />
                        </NavLink>
                    ):''
                }
            </div>
            <Table
                data={employees}
                columns={columns}
                pagination={pagination}
                loadingState={loadingState}
                sortAction={fetchEmployeesReq}
                tableProps={{
                    filter: filterFactory(),
                }}
                remote={{sort: true, pagination: true, filter: true}}
                actionCallback={(...params) => {
                    return fetchEmployeesReq(params[0], departmentId, params[1], params[2], params[3])
                }}
            />
        </>
    )
}

export default memo(EmployeesTable)
