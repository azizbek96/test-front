import React, {memo, useState, useEffect, useCallback} from 'react'
import {Col, Row, Form, Button, CardHeader, CardBody} from 'reactstrap'
import {useTranslation} from 'react-i18next'
import Lightbox from 'react-image-lightbox'
import {useDispatch, useSelector} from 'react-redux'
import {useFormik} from 'formik'
import 'react-image-lightbox/style.css'
import {toast} from 'react-toastify'
import {useHistory} from 'react-router'
import DropZone from 'react-dropzone'
import classNames from 'classnames'
import * as Yup from 'yup'
import _, {isEmpty} from 'lodash'

import {employeesUpdateReq, employeesUpdatePutReq, employeesCreatePostReq} from '../../store/employees/actions'

import {RenderInputField, RenderDatePickerField} from './RenderField'
import {useLocation} from "react-router-dom";

const EmployeesModal = () => {
    const dispatch = useDispatch()
    const state = useLocation().state
    const history = useHistory()

    useEffect(() => {
        if (state?.id !== null) {
            dispatch(employeesUpdateReq(state?.id))
        }
    }, [state]);

    const {
        formErrors,
        message,
        errorType,
        updatingValues,
        isUpdating
    } = useSelector(s => ({
        formErrors: s.Employees.formErrors,
        message: s.Employees.message,
        errorType: s.Employees.errorType,
        updatingValues: s.Employees.updatingValues,
        isUpdating: s.Employees.isUpdating,
    }))

    const [t] = useTranslation()
    const translationErrorsKey = 'employees.form.errors'
    const formik = useFormik({
        validateOnChange: false,
        enableReinitialize: false,
        initialValues: {
            pnfl: undefined,
            address: undefined,
            last_name: undefined,
            birth_date: undefined,
            first_name: undefined,
            father_name: undefined,
        },
        validationSchema: Yup.object({
            address: Yup.string(),
            birth_date: Yup.date().required(translationErrorsKey.concat('.birth_date.required')),
            last_name: Yup.string().required(translationErrorsKey.concat('.last_name.required')),
            first_name: Yup.string().required(translationErrorsKey.concat('.first_name.required')),
            father_name: Yup.string().required(translationErrorsKey.concat('.father_name.required')),
        }),
        onSubmit: values => {
            if (isUpdating) {
                dispatch(employeesUpdatePutReq(values));
                console.log("update", values)
            } else {
                dispatch(employeesCreatePostReq(values));
                console.log("create", values)
            }
        },
    })


    useEffect(() => {
        if (!_.isEmpty(message)) {
            if (errorType === 'SUCCESS') {
                formik.resetForm()
                toast.success(message, {autoClose: 2000, theme: 'colored'})
                history.push('/employees')
            } else {
                toast.error(message, {autoClose: 2000, theme: 'colored'})
            }
        }
    }, [message])

    useEffect(() => {
        if (!_.isEmpty(updatingValues)) {
            formik.setFieldValue('id', updatingValues.id)
            formik.setFieldValue('address', updatingValues.address)
            formik.setFieldValue('last_name', updatingValues.last_name)
            formik.setFieldValue('birth_date', new Date(updatingValues.birth_date))
            formik.setFieldValue('first_name', updatingValues.first_name)
            formik.setFieldValue('father_name', updatingValues.father_name)
            formik.setFieldValue('phone_number', updatingValues.phone_number)
        }
    }, [updatingValues])

    return (
        <div className='page-content'>
            <CardHeader>{isUpdating ? t('Update'): t('Create')}</CardHeader>
            <CardBody>
                <Form
                    autoComplete='off'
                    onSubmit={e => {
                        e.preventDefault()
                        formik.handleSubmit(e);
                    }}
                >
                    <Row>
                        <Col md={4}>
                            <RenderInputField
                                type='text'
                                tabIndex={1}
                                label={t('Last name')}
                                labelClass='required-field'
                                meta={formik.getFieldMeta('last_name')}
                                field={formik.getFieldProps('last_name')}
                                invalid={!!formik.touched.last_name && !!formik.errors.last_name}
                            />
                            <RenderDatePickerField
                                tabIndex={4}
                                label={t('Birthdate')}
                                labelClass='required-field'
                                setFieldValue={formik.setFieldValue}
                                meta={formik.getFieldMeta('birth_date')}
                                setFieldTouched={formik.setFieldTouched}
                                field={formik.getFieldProps('birth_date')}
                                invalid={!!formik.touched.birth_date && !!formik.errors.birth_date}
                            />

                        </Col>
                        <Col md={4}>
                            <RenderInputField
                                type='text'
                                tabIndex={2}
                                label={t('First name')}
                                labelClass='required-field'
                                meta={formik.getFieldMeta('first_name')}
                                field={formik.getFieldProps('first_name')}
                                invalid={!!formik.touched.first_name && !!formik.errors.first_name}
                            />
                            <RenderInputField
                                type='tel'
                                alwaysShowMask
                                tabIndex={5}
                                label={t('Phone number')}
                                mask='+998-__-___-__-__'
                                labelClass='required-field'
                                meta={formik.getFieldMeta('phone_number')}
                                field={formik.getFieldProps('phone_number')}
                                formatChars={{
                                    _: '[0-9]',
                                }}
                                placeholder={t('Enter your phone...')}
                                invalid={!!formik.touched.phone_number && !!formik.errors.phone_number}
                            />

                        </Col>
                        <Col md={4}>
                            <RenderInputField
                                type='text'
                                tabIndex={3}
                                label={t('Father name')}
                                labelClass='required-field'
                                meta={formik.getFieldMeta('father_name')}
                                field={formik.getFieldProps('father_name')}
                                invalid={!!formik.touched.father_name && !!formik.errors.father_name}
                            />

                            <RenderInputField
                                tabIndex={7}
                                type='textarea'
                                label={t('Address')}
                                meta={formik.getFieldMeta('address')}
                                field={formik.getFieldProps('address')}
                                invalid={!!formik.touched.address && !!formik.errors.address}
                            />
                        </Col>
                    </Row>
                    <hr/>

                    <div className='text-end mt-3'>
                        <Button type='submit' color='success'>
                            {t('Save')}
                        </Button>
                    </div>
                </Form>
            </CardBody>
        </div>
    )
}

export default memo(EmployeesModal)
