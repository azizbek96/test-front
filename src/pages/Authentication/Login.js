import { Col, Row, Card, Form,Input, Label, CardBody, Container, FormFeedback } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import React, { useState, useEffect } from 'react'
import { useFormik } from 'formik'
import { isEmpty } from 'lodash'
import * as Yup from 'yup'

import FormLoader from '../../components/Loaders/FormLoader'
import { adminLoginUser } from '../../store/actions'
import { useTranslation } from 'react-i18next'

const Login = () => {
	const dispatch = useDispatch()

	const { errors, formLoading } = useSelector(s => ({
		errors: s.Auth.loginErrors,
		formLoading: s.Auth.formLoading,
	}))

	const [t] = useTranslation()

	const formik = useFormik({
		enableReinitialize: false,
		initialValues: {
			username: '',
			password: '',
		},
		validationSchema: Yup.object({
			username: Yup.string().required('Please Enter Your Username'),
			password: Yup.string().required('Please Enter Your Jeton number'),
		}),
		onSubmit: values => {
			formik.setSubmitting(false)
			dispatch(adminLoginUser(values))
		},
	})

	useEffect(() => {
		if (!isEmpty(errors)) {
			formik.setErrors(errors)
		}
	}, [errors])

	return (
		<>
			<div className='account-pages my-5 pt-sm-5'>
				<Container>
					<Row className='justify-content-center'>
						<Col md={8} lg={6} xl={5}>
							<Card className='overflow-hidden'>
								<CardBody>
									<div className='p-2'>
										<Form
											className='form-horizontal'
											onSubmit={e => {
												e.preventDefault()
												formik.handleSubmit()
											}}
										>
											<FormLoader loading={formLoading} />
											<div className='mb-3'>
												<Label className='form-label'>{ t('Username') }</Label>
												<Input
													type='text'
													name='username'
													className='form-control'
													onBlur={formik.handleBlur}
													placeholder={t('Enter username')}
													value={formik.values.username}
													onChange={formik.handleChange}
													invalid={!!(formik.touched.username && formik.errors.username)}
												/>
												{formik.touched.username && formik.errors.username && <FormFeedback type='invalid'>{formik.errors.username}</FormFeedback>}
											</div>

											<div className='mb-3'>
												<Label className='form-label'>{ t('Jeton number')}</Label>
												<Input
													name='password'
													type='password'
													onBlur={formik.handleBlur}
													placeholder={t('Enter Jeton number')}
													value={formik.values.password}
													onChange={formik.handleChange}
													invalid={!!(formik.touched.password && formik.errors.password)}
												/>
												{formik.touched.password && formik.errors.password && <FormFeedback type='invalid'>{formik.errors.password}</FormFeedback>}
											</div>

											<div className='mt-3 d-grid'>
												<button type='submit' className='btn btn-primary btn-block'>
													{t('Log In')}
												</button>
											</div>
										</Form>
									</div>
								</CardBody>
							</Card>
						</Col>
					</Row>
				</Container>
			</div>
		</>
	)
}

export default Login
