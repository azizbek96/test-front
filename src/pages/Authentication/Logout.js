import React, { useLayoutEffect } from 'react'
import { useDispatch } from 'react-redux'

import { logoutUser } from '../../store/actions'

const Logout = () => {
	const dispatch = useDispatch()
	useLayoutEffect(() => {
		dispatch(logoutUser())
	}, [dispatch])

	return <></>
}

export default Logout
