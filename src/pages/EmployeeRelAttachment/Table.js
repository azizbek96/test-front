import filterFactory, {textFilter, customFilter, FILTER_TYPES} from 'react-bootstrap-table2-filter'
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css'
import React, {memo, useMemo, useState, useCallback, useLayoutEffect} from 'react'
import {BiFilter, BiSortUp, BiSortDown} from 'react-icons/bi'
import {useDispatch, useSelector} from 'react-redux'
import SweetAlert from 'react-bootstrap-sweetalert'
import {useTranslation} from 'react-i18next'
import {NavLink, useHistory} from 'react-router-dom'
import {Button} from 'reactstrap'

import {
    fetchEmployeeRelAttachmentReq,
    employeeRelAttachmentDelete,
    employeeRelAttachmentIsUpdating
} from '../../store/employeeRelAttachment/actions'
import CustomFilter from '../../components/CustomFilter'
import Table from '../../components/Tables/RemoteTable'

const TextFilter = textFilter({
    delay: 300,
})

const EmployeeRelAttachmentTable = () => {
    const [t] = useTranslation()
    const history = useHistory()
    const dispatch = useDispatch()

    useLayoutEffect(() => {
        dispatch(fetchEmployeeRelAttachmentReq(1, '', '', {}))
        dispatch(employeeRelAttachmentIsUpdating(false))
    }, [])

    const {
        employeeRelAttachment,
        pagination,
        permissions,
        isLoading: loadingState,
    } = useSelector(s => ({
        employeeRelAttachment: s.EmployeeRelAttachment.employeeRelAttachment,
        isLoading: s.EmployeeRelAttachment.isLoading,
        pagination: s.EmployeeRelAttachment.pagination,
        permissions: s.EmployeeRelAttachment.permissions,
    }))
    const [deleteAlert, setDeleteAlert] = useState(null)
    const deleteHandler = useCallback(id => {
        dispatch(employeeRelAttachmentDelete(id))
    }, [])
    const updateHandler = id => {
        history.push({pathname: '/employee-rel-attachemnt/create', state: {id: id}})
    }

    /**
     *
     * @type {({formatter(): *, dataField: string, headerAlign: string, isDummyField: boolean, style: {width: string}, text: string, align: string}|{filter: , dataField: string, sort: boolean, text: TFuncReturn<"translation", string, string, undefined>}|{filter: , dataField: string, sort: boolean, text: TFuncReturn<"translation", string, string, undefined>}|{filter: , dataField: string, sort: boolean, text: TFuncReturn<"translation", string, string, undefined>}|{formatter(*, *): *, dataField: string, isDummyField: boolean, text: TFuncReturn<"translation", string, string, undefined>})[]}
     */
    const columns = useMemo(() => {
        return [
            {
                text: '#',
                align: 'center',
                dataField: 'index',
                isDummyField: true,
                headerAlign: 'center',
                style: {
                    width: '50px',
                },
                formatter() {
                    return arguments[2] + 1
                },
            },
            {
                sort: true,
                text: t('User name'),
                filter: TextFilter,
                dataField: 'name',
            },
            {
                sort: true,
                filter: TextFilter,
                text: t('File name'),
                dataField: 'file_name',
            },
            {
                sort: true,
                filter: TextFilter,
                text: t('File path'),
                dataField: 'file_path',
                formatter(cell, row) {
                    return (
                        <div className='d-inline-flex gap-3'>
                            {
                                permissions?.view?(
                                    <a href={row?.file_path} download={row?.file_name}>
                                        <i className='mdi mdi-download font-size-16'/>
                                    </a>
                                ):''
                            }
                        </div>
                    )
                }
            },
            {
                text: t('Actions'),
                isDummyField: true,
                dataField: 'action',
                formatter(cell, row) {
                    return (
                        <div className='d-inline-flex gap-3'>
                            {
                                permissions?.delete?(
                                    <Button
                                        size='sm'
                                        color='danger'
                                        className='employees-action-btn'
                                        onClick={() => setDeleteAlert(row.id)}
                                    >
                                        <i className='mdi mdi-delete font-size-16'/>
                                    </Button>
                                ):''
                            }
                        </div>
                    )
                },
            },
        ].map(col => {
            if (col.sort) {
                col.sortCaret = order => {
                    if (!order)
                        return (
                            <span>
								&nbsp;&nbsp;
                                <BiFilter size={20}/>
							</span>
                        )
                    else if (order === 'asc')
                        return (
                            <span>
								&nbsp;&nbsp;
                                <BiSortDown size={20}/>
							</span>
                        )
                    else if (order === 'desc')
                        return (
                            <span>
								&nbsp;&nbsp;
                                <BiSortUp size={20}/>
							</span>
                        )
                    return null
                }
            }
            return col
        })
    }, [t, employeeRelAttachment, permissions])

    return (
        <>
            {deleteAlert && (
                <SweetAlert
                    info
                    showCancel
                    title={t('Are you sure')}
                    closeOnClickOutside={false}
                    onCancel={() => setDeleteAlert(null)}
                    onConfirm={() => {
                        deleteHandler(deleteAlert)
                        setDeleteAlert(null)
                    }}
                />
            )}
            <div className='text-end px-5'>
                {
                    permissions?.create?(
                        <NavLink to={{pathname: '/employee-rel-attachment/create', state: {id: null}}} className='btn btn-success'>
                            <i className='fas fa-plus' />
                        </NavLink>
                    ):''
                }
            </div>
            <Table
                data={employeeRelAttachment}
                columns={columns}
                pagination={pagination}
                loadingState={loadingState}
                sortAction={fetchEmployeeRelAttachmentReq}
                tableProps={{
                    filter: filterFactory(),
                }}
                remote={{sort: true, pagination: true, filter: true}}
                actionCallback={(...params) => {
                    return fetchEmployeeRelAttachmentReq(params[0], params[1], params[2], params[3])
                }}
            />
        </>
    )
}

export default memo(EmployeeRelAttachmentTable)
