import { Card, CardBody, CardHeader } from 'reactstrap'

import EmployeeRelAttachmentTable from './Table'
import { memo } from 'react'

const EmployeeRelAttachment = () => {
	return (
		<>
			<Card >
				<br/>
				<br/>
				<br/>
				<CardBody >
					<EmployeeRelAttachmentTable/>
				</CardBody>
			</Card>
		</>
	)
}

export default memo(EmployeeRelAttachment)
