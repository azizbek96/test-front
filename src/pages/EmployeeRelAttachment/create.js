import React, {memo, useState, useEffect, useCallback} from 'react'
import {Col, Row, Form, Button, CardHeader, CardBody, Input,Label} from 'reactstrap'
import {useTranslation} from 'react-i18next'
import Lightbox from 'react-image-lightbox'
import {useDispatch, useSelector} from 'react-redux'
import {useFormik} from 'formik'
import 'react-image-lightbox/style.css'
import {toast} from 'react-toastify'
import {useHistory} from 'react-router'
import { formatBytes } from '../../utils'
import classNames from 'classnames'
import * as Yup from 'yup'
import _, {isEmpty} from 'lodash'

import {employeeRelAttachmentUpdateReq, employeeRelAttachmentUpdatePutReq, employeeRelAttachmentCreatePostReq} from '../../store/employeeRelAttachment/actions'
import {useLocation} from "react-router-dom";
import DropZone from "react-dropzone";

const Create = () => {
    const dispatch = useDispatch()
    const state = useLocation().state
    const history = useHistory()

    useEffect(() => {
        if (state?.id !== null) {
            dispatch(employeeRelAttachmentUpdateReq(state?.id))
        }
    }, [state]);

    const {
        formErrors,
        message,
        errorType,
        updatingValues,
        isUpdating
    } = useSelector(s => ({
        formErrors: s.EmployeeRelAttachment.formErrors,
        message: s.EmployeeRelAttachment.message,
        errorType: s.EmployeeRelAttachment.errorType,
        updatingValues: s.EmployeeRelAttachment.updatingValues,
        isUpdating: s.EmployeeRelAttachment.isUpdating,
    }))

    const [t] = useTranslation()
    const formik = useFormik({
        validateOnChange: false,
        enableReinitialize: false,
        initialValues: {
            user_files: undefined,
        },
        onSubmit: values => {
            if (isUpdating) {
                dispatch(employeeRelAttachmentUpdatePutReq(values));
                console.log("update", values)
            } else {
                dispatch(employeeRelAttachmentCreatePostReq(values));
                console.log("create", values)
            }
        },
    })


    useEffect(() => {
        if (!_.isEmpty(message)) {
            if (errorType === 'SUCCESS') {
                formik.resetForm()
                toast.success(message, {autoClose: 2000, theme: 'colored'})
                history.push('/employee-rel-attachment')
            } else {
                toast.error(message, {autoClose: 2000, theme: 'colored'})
            }
        }
    }, [message])
    const handleAcceptedUserFiles = useCallback(files => {
        const array = []

        files.forEach(file => {
            const path = URL.createObjectURL(file)
            const data = {
                path,
                name: file.name,
                formattedSize: formatBytes(file.size),
            }
            array.push(data)
        })
        formik.setFieldValue('user_files', array)
    }, [])
    const deleteSop = value => {
        const dataSop = formik.values.user_files.filter(item => item !== value)
        formik.setFieldValue('user_files', dataSop)
    }

    useEffect(() => {
        if (!_.isEmpty(updatingValues)) {
            formik.setFieldValue('user_files', updatingValues.user_files)
        }
    }, [updatingValues])
    return (
        <div className='page-content'>
            <CardHeader>{isUpdating ? t('Update'): t('Create')}</CardHeader>
            <CardBody>
                <Form
                    autoComplete='off'
                    onSubmit={e => {
                        e.preventDefault()
                        formik.submitForm(e);
                    }}
                >
                    <Row>
                        <Col md={6}>
                            <Label htmlFor='fileJes'>{t('File uploaded') }</Label>
                            <DropZone
                                minSize={0}
                                multiple={true}
                                onDrop={files => {
                                    if (files) {
                                        handleAcceptedUserFiles(files)
                                    }
                                }}
                            >
                                {({ getRootProps, getInputProps }) => (
                                    <div
                                        style={{
                                            height: '150px',
                                            border: '2px dashed grey',
                                            borderRadius: '10px',
                                        }}
                                    >
                                        <div {...getRootProps()} className='dz-message needsclick'>
                                            <input {...getInputProps()} />
                                            <div>
                                                <i className='display-5 text-muted bx bxs-cloud-upload' />
                                            </div>
                                            <h4>{t('Drop files here or click to upload')}</h4>
                                        </div>
                                    </div>
                                )}
                            </DropZone>
                        </Col>
                        <Col md={6}>
                            <br/>
                            <ul className='list-group mt-3'>
                                {!isEmpty(formik.values.user_files) ? (
                                    <>
                                        <li className='list-group-item active'>{t('Files')}</li>
                                        {formik.values.user_files.map((values, index) => (
                                            <li key={index} className='list-group-item'>
                                                <span>{index + 1} - </span>
                                                <a target='_blank' rel='noreferrer' href={values.path}>
                                                    {values.name}
                                                </a>
                                                <Button
                                                    size='sm'
                                                    type='button'
                                                    color='danger'
                                                    className='float-end'
                                                    onClick={() => deleteSop(values)}
                                                >
                                                    <i id='deletetooltip' className='mdi mdi-delete font-size-16' />
                                                </Button>
                                            </li>
                                        ))}
                                    </>
                                ) : (
                                    ''
                                )}
                            </ul>
                        </Col>
                    </Row>

                    <hr/>
                    <div className='text-end mt-3'>
                        <Button type='submit' color='success'>
                            {t('Save')}
                        </Button>
                    </div>
                </Form>
            </CardBody>
        </div>
    )
}

export default memo(Create)
