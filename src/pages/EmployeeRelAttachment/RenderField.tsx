import {FormikErrors, FieldMetaProps, FieldInputProps} from 'formik'
import {Input, Label, FormGroup, FormFeedback} from 'reactstrap'
import {InputType} from 'reactstrap/types/lib/Input'
// @ts-ignore
import InputMask from 'react-input-mask'
import classNames from 'classnames'
import React, {FC} from 'react'

import RequiredSelect from '../../components/Select/RequiredSelect'
import {FormDatePicker} from '../../components/DatePicker'
import {useTranslation} from 'react-i18next'

interface BaseRenderField {
    label: string
    invalid: boolean
    tabIndex?: number
    showLabel?: boolean
    labelClass?: string
    meta: FieldMetaProps<any>
    field: FieldInputProps<any>
}

interface IRenderInputField extends BaseRenderField {
    mask?: boolean
    type: InputType
}

const placeholderTranslationKey = 'employees.form.placeholders'

const RenderInputField: FC<IRenderInputField> = ({
                                                     type,
                                                     mask,
                                                     field,
                                                     label,
                                                     invalid,
                                                     tabIndex,
                                                     labelClass,
                                                     meta: {error},
                                                     showLabel = true,
                                                 }) => {
    const [t] = useTranslation()

    return (
        <FormGroup>
            {showLabel === true && (
                <Label className={classNames(labelClass)} htmlFor={`employee-${field.name}-input`}>
                    {t(label)}
                </Label>
            )}
            {mask ? (
                <InputMask
                    type='text'
                    alwaysShowMask
                    name={field.name}
                    tabIndex={tabIndex}
                    onBlur={field.onBlur}
                    mask='+998-__-___-__-__'
                    value={field.value ?? ''}
                    onChange={field.onChange}
                    formatChars={{
                        _: '[0-9]',
                    }}
                    className={classNames('form-control', {'is-invalid': invalid})}
                    placeholder={t(placeholderTranslationKey.concat('.', field.name.replace(/\[\d\]/, '')))}
                />
            ) : (
                <Input
                    type={type}
                    name={field.name}
                    invalid={invalid}
                    value={field.value}
                    tabIndex={tabIndex}
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                    placeholder={t(placeholderTranslationKey.concat('.', field.name.replace(/\[\d\]/, '')))}
                />
            )}
            {invalid && <FormFeedback>{t(error ?? '')}</FormFeedback>}
        </FormGroup>
    )
}

interface IRenderSelectField extends BaseRenderField {
    multiple?: boolean
    disabled?: boolean
    options: Array<{ value: number | string; label: string }>
    setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean | undefined
    ) => Promise<FormikErrors<any>> | Promise<void> | void
}

const RenderSelectField: FC<IRenderSelectField> = ({
                                                       field,
                                                       label,
                                                       invalid,
                                                       disabled,
                                                       tabIndex,
                                                       labelClass,
                                                       options = [],
                                                       setFieldValue,
                                                       meta: {error},
                                                       multiple = false,
                                                       showLabel = true,
                                                   }) => {
    const [t] = useTranslation()

    return (
        <FormGroup>
            {showLabel === true && (
                <Label className={classNames(labelClass)} htmlFor={`employee-${field.name}-select`}>
                    {t(label)}
                </Label>
            )}
            <RequiredSelect
                invalid={invalid}
                options={options}
                isMulti={multiple}
                tabIndex={tabIndex}
                isDisabled={disabled}
                errorMessage={t(error ?? '')}
                closeMenuOnSelect={!multiple}
                id={`employee-${field.name}-select`}
                placeholder={t(placeholderTranslationKey.concat('.', field.name.replace(/\[\d\]/, '')))}
                // @ts-ignore
                onChange={selected => {
                    setFieldValue(field.name, multiple ? selected.map((s: any) => s.value) : selected.value)
                }}
                value={
                    multiple === true
                        ? options.filter(({value}) => field.value.includes(value))
                        : options.find(({value}) => {
                            return field.value === value
                        })
                }
            />
        </FormGroup>
    )
}

interface IRenderDatePickerField extends BaseRenderField {
    label: string
    invalid: boolean
    labelClass?: string
    meta: FieldMetaProps<any>
    field: FieldInputProps<any>
    setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean | undefined
    ) => Promise<FormikErrors<any>> | Promise<void> | void
    setFieldTouched: (
        field: string,
        touched?: boolean,
        shouldValidate?: boolean | undefined
    ) => Promise<FormikErrors<any>> | Promise<void> | void
}

const RenderDatePickerField: FC<IRenderDatePickerField> = ({
                                                               label,
                                                               field,
                                                               invalid,
                                                               tabIndex,
                                                               labelClass,
                                                               setFieldValue,
                                                               meta: {error},
                                                               setFieldTouched,
                                                               showLabel = true,
                                                           }) => {
    const [t] = useTranslation()

    return (
        <FormGroup>
            {showLabel === true && (
                <Label className={classNames(labelClass)} htmlFor={`employee-${field.name}-datepicker`}>
                    {t(label)}
                </Label>
            )}
            <FormDatePicker.WithYear
                tabIndex={tabIndex}
                showPopperArrow={false}
                selected={field.value ?? null}
                id={`employee-${field.name}-datepicker`}
                onBlur={() => {
                    setFieldTouched(field.name, true)
                }}
                onChange={(date: Date) => {
                    setFieldValue(field.name, date ?? null)
                }}
                placeholderText={t(placeholderTranslationKey.concat('.', field.name.replace(/\[\d\]/, '')))}
                customInput={
                    <FormDatePicker.CustomInput
                        // @ts-ignore
                        invalid={invalid}
                    />
                }
            />
            {invalid && <FormFeedback style={invalid && {display: 'block'}}>{t(error ?? '')}</FormFeedback>}
        </FormGroup>
    )
}

export {RenderInputField, RenderSelectField, RenderDatePickerField}
