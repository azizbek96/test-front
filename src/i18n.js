import detector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'
import i18n from 'i18next'

import translationENG from './locales/eng/translation.json'
import translationRU from './locales/ru/translation.json'
import translationUZ from './locales/uz/translation.json'

// the translations
const resources = {
	uz: {
		translation: translationUZ,
	},
	rs: {
		translation: translationRU,
	},
	eng: {
		translation: translationENG,
	},
}

const language = localStorage.getItem('I18N_LANGUAGE')
if (!language) {
	localStorage.setItem('I18N_LANGUAGE', 'uz')
}

i18n
	.use(detector)
	.use(initReactI18next) // passes i18n down to react-i18next
	.init({
		resources,
		lng: localStorage.getItem('I18N_LANGUAGE') || 'uz',
		fallbackLng: 'en', // use en if detected lng is not available

		keySeparator: '.', // we do not use keys in form messages.welcome

		interpolation: {
			escapeValue: false, // react already safes from xss
		},
	})

export default i18n
