// Crypto Redux States
import { all, put, fork, takeEvery } from 'redux-saga/effects'

import { GET_CHARTS_DATA } from './actionTypes'
import { apiFail, apiSuccess } from './actions'

function* getChartsData() {
	try {
		yield put(apiSuccess(GET_CHARTS_DATA))
	} catch (error) {
		yield put(apiFail(GET_CHARTS_DATA, error))
	}
}

export function* watchGetChartsData() {
	yield takeEvery(GET_CHARTS_DATA, getChartsData)
}

function* dashboardSaga() {
	yield all([fork(watchGetChartsData)])
}

export default dashboardSaga
