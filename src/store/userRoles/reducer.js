import {
	USER_ROLE_API_ERROR,
	USER_ROLE_DELETE_END,
	USER_ROLE_UPDATE_END,
	USER_ROLE_SET_LOADING,
	USER_ROLE_ROLE_ERRORS,
	USER_ROLE_DELETE_START,
	USER_ROLE_SET_USER_ROLE,
	USER_ROLE_SET_ROLE_TYPE,
	USER_ROLE_ADD_PAGINATION,
	USER_ROLE_SET_USER_ROLES,
	USER_ROLE_MODAL_STATE_SET,
	USER_ROLE_SET_FORM_LOADING,
	USER_ROLE_SET_UPDATED_DATA,
	USER_ROLE_UPDATING_VALUES_SET,
} from './actionTypes'

const initialState = {
	roles: [],
	result: {},
	apiError: {},
	roleTypes: [],
	loading: false,
	rolesError: {},
	pagination: {},
	permissions: {},
	deleting: null,
	modalState: false,
	closeModal: false,
	formLoading: false,
	updatingValues: {},
}

const reducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case USER_ROLE_SET_LOADING:
			return { ...state, loading: payload }
		case USER_ROLE_API_ERROR:
			return { ...state, apiError: payload }
		case USER_ROLE_SET_USER_ROLES:
			return { ...state, roles: payload.data.map((d, i) => {
					d.key = i
					return d
				}), permissions: payload.permissions }
		case USER_ROLE_UPDATE_END:
			return { ...state, updatingValues: initialState.updatingValues }
		case USER_ROLE_SET_USER_ROLE:
			return {
				...state,
				closeModal: true,
				roles: payload.roles,
				result: payload.role,
			}
		case USER_ROLE_ADD_PAGINATION:
			return { ...state, pagination: payload }
		case USER_ROLE_ROLE_ERRORS:
			return { ...state, rolesError: payload }
		case USER_ROLE_SET_FORM_LOADING:
			return { ...state, formLoading: payload }
		case USER_ROLE_SET_UPDATED_DATA:
			return {
				...state,
				result: payload.role,
				roleName: payload.roleName,
			}
		case USER_ROLE_SET_ROLE_TYPE:
			return {
				...state,
				roleTypes: payload,
			}
		case USER_ROLE_DELETE_START:
			return {
				...state,
				deleting: payload,
			}
		case USER_ROLE_DELETE_END:
			return { ...state, deleting: initialState.deleting }
		case USER_ROLE_MODAL_STATE_SET:
			return { ...state, modalState: payload }
		case USER_ROLE_UPDATING_VALUES_SET:
			return { ...state, updatingValues: payload }
		default:
			return state
	}
}

export default reducer
