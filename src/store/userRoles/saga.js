import { put, call, select, takeLeading } from 'redux-saga/effects'

import {
	setLoading,
	setUserRole,
	setUserRoles,
	addPagination,
	setFormLoading,
	apiUserRoleError,
	setUserRoleErrors,
	userRoleModalStateSet,
	setUserRoleUpdatedData,
	userRoleDeleteEndAction,
	userRoleUpdateEndAction,
	userRoleSetRoleTypeAction,
	userRoleUpdatingValuesSetAction,
} from './actions'
import { USER_ROLE_UPDATE, USER_ROLE_UPDATE_GET, USER_ROLE_DELETE_START, USER_ROLE_UPDATE_START, USER_ROLE_FETCH_ROLE_LIST, USER_ROLE_CREATE_OR_UPDATE_ROLE } from './actionTypes'
import { USER_ROLE_INDEX_URL, USER_ROLE_CREATE_URL, USER_ROLE_UPDATE_URL, USER_ROLE_DELETE_URL } from '../../helpers/url_helper'
import { api } from '../../helpers/api_helper'

const getRoles = async page => {
	return await api.get(USER_ROLE_INDEX_URL + '?page=' + page, true)
}

function* fetchUserRoles({ payload: page = 1 }) {
	try {
		yield put(setLoading(true))
		const { data: response, pagination } = yield call(getRoles, page)

		if (response.code === 200) {
			yield put(addPagination(pagination))
			yield put(
				setUserRoles(
					response
				)
			)
			yield put(userRoleSetRoleTypeAction(response.role_type))
		} else {
			yield put(setUserRoleErrors(response.data))
		}
	} catch (error) {
		yield put(apiUserRoleError(error))
	} finally {
		yield put(setLoading(false))
	}
}

const createOrUpdate = async data => {
	const config = { url: USER_ROLE_CREATE_URL, method: 'post' }

	if (data && data.action === 'UPDATE') {
		config.url = USER_ROLE_UPDATE_URL + '?name=' + data.old_name
		config.method = 'put'
	}
	return await api[config.method](config.url, data)
}

function* createWorker({ payload }) {
	try {
		yield put(setFormLoading(true))
		const { data: response } = yield call(createOrUpdate, payload)
		if (response.code === 200) {
			const roles = yield select(s => s.UserRoles.roles)
			let result = {
				roles: [],
				role: response.data,
			}

			if (response.data.action === 'UPDATE') {
				const key = roles.findIndex(item => item.name === response.data.old_name)
				roles[key] = response.data
				result.roles = [...roles]
			} else {
				result.roles = [...roles, response.data]
			}
			yield put(setUserRole(result))
			yield put(userRoleModalStateSet(false))
		} else {
			yield put(setUserRoleErrors(response))
		}
	} catch (error) {
		yield put(apiUserRoleError(error))
	} finally {
		yield put(setFormLoading(false))
	}
}

const updateGetReq = async name => await api.get(USER_ROLE_UPDATE_URL + '?name=' + name)

function* updateGet({ payload }) {
	try {
		yield put(setFormLoading(true))
		const { data: response } = yield call(updateGetReq, payload)
		if (response.code === 200) {
			let result = {
				role: {},
				roleName: null,
			}
			if (response.data && response.data.role) {
				result.role = response.data.role
				result.roleName = response.data.role.old_name
			}

			yield put(setUserRoleUpdatedData(result))
		} else {
			yield put(setUserRoleErrors(response))
		}
	} catch (error) {
		yield put(apiUserRoleError(error))
	} finally {
		yield put(setFormLoading(false))
	}
}

const userRoleDeleteReq = async name => await api.delete(`${USER_ROLE_DELETE_URL}?name=${name}`)

function* userRoleDeleteWorker({ payload: name }) {
	try {
		const { data: response } = yield call(userRoleDeleteReq, name)

		if (response.code === 200) {
			const roles = yield select(s => s.UserRoles.roles.filter(role => role.name !== name))

			yield put(setUserRoles(roles))
		}
	} catch (error) {
		yield put(apiUserRoleError(error.toString()))
	} finally {
		yield put(userRoleDeleteEndAction())
	}
}

function* userRoleUpdateStartWorker({ payload: name }) {
	try {
		yield put(userRoleModalStateSet(true))
		yield put(setFormLoading(true))

		const { data: response } = yield call(updateGetReq, name)

		if (response.code === 200) {
			yield put(userRoleUpdatingValuesSetAction(response.data.role))
		} else {
			yield put(userRoleModalStateSet(false))
		}
	} catch (error) {
		yield put(apiUserRoleError(error.toString()))
	} finally {
		yield put(setFormLoading(false))
	}
}

const updateReq = async (values, name) => await api.put(`${USER_ROLE_UPDATE_URL}?name=${name}`, values)

function* userRoleUpdateWorker({ payload: values }) {
	try {
		yield put(setFormLoading(true))

		const { data: response } = yield call(updateReq, values, yield select(s => s.UserRoles.updatingValues.name))

		if (response.code === 200) {
			const userRoles = yield select(s =>
				s.UserRoles.roles.map(role => {
					if (role.name === response.data.old_name) {
						delete response.data.description
						delete response.data.action
						return response.data
					}
					return role
				})
			)
			yield put(userRoleModalStateSet(false))
			yield put(setUserRoles(userRoles))
		}

		yield put(userRoleUpdateEndAction())
	} catch (error) {
		yield put(apiUserRoleError(error.toString()))
	} finally {
		yield put(setFormLoading(false))
	}
}

function* userRolesSaga() {
	yield takeLeading(USER_ROLE_UPDATE_GET, updateGet)
	yield takeLeading(USER_ROLE_UPDATE, userRoleUpdateWorker)
	yield takeLeading(USER_ROLE_FETCH_ROLE_LIST, fetchUserRoles)
	yield takeLeading(USER_ROLE_DELETE_START, userRoleDeleteWorker)
	yield takeLeading(USER_ROLE_CREATE_OR_UPDATE_ROLE, createWorker)
	yield takeLeading(USER_ROLE_UPDATE_START, userRoleUpdateStartWorker)
}

export default userRolesSaga
