import {
	USER_ROLE_UPDATE,
	USER_ROLE_API_ERROR,
	USER_ROLE_UPDATE_GET,
	USER_ROLE_DELETE_END,
	USER_ROLE_UPDATE_END,
	USER_ROLE_SET_LOADING,
	USER_ROLE_ROLE_ERRORS,
	USER_ROLE_DELETE_START,
	USER_ROLE_UPDATE_START,
	USER_ROLE_SET_USER_ROLE,
	USER_ROLE_SET_ROLE_TYPE,
	USER_ROLE_ADD_PAGINATION,
	USER_ROLE_SET_USER_ROLES,
	USER_ROLE_FETCH_ROLE_LIST,
	USER_ROLE_MODAL_STATE_SET,
	USER_ROLE_SET_FORM_LOADING,
	USER_ROLE_SET_UPDATED_DATA,
	USER_ROLE_UPDATING_VALUES_SET,
	USER_ROLE_CREATE_OR_UPDATE_ROLE,
} from './actionTypes'

const fetchUserRoleList = page => ({
	type: USER_ROLE_FETCH_ROLE_LIST,
	payload: page,
})
const apiUserRoleError = error => ({
	type: USER_ROLE_API_ERROR,
	payload: error,
})
const setLoading = loading => ({
	type: USER_ROLE_SET_LOADING,
	payload: loading,
})
const setFormLoading = loading => ({
	type: USER_ROLE_SET_FORM_LOADING,
	payload: loading,
})
const addPagination = pagination => ({
	type: USER_ROLE_ADD_PAGINATION,
	payload: pagination,
})
const setUserRoles = roles => ({
	type: USER_ROLE_SET_USER_ROLES,
	payload: roles,
})
const setUserRole = role => ({ type: USER_ROLE_SET_USER_ROLE, payload: role })
const createOrUpdateUserRoles = value => ({
	type: USER_ROLE_CREATE_OR_UPDATE_ROLE,
	payload: value,
})
const setUserRoleErrors = error => ({
	type: USER_ROLE_ROLE_ERRORS,
	payload: error,
})
const userRoleUpdateGet = name => ({
	type: USER_ROLE_UPDATE_GET,
	payload: name,
})
const setUserRoleUpdatedData = data => ({
	type: USER_ROLE_SET_UPDATED_DATA,
	payload: data,
})

const userRoleSetRoleTypeAction = roleTypes => ({
	type: USER_ROLE_SET_ROLE_TYPE,
	payload: roleTypes,
})

const userRoleDeleteStartAction = name => ({
	type: USER_ROLE_DELETE_START,
	payload: name,
})

const userRoleDeleteEndAction = () => ({ type: USER_ROLE_DELETE_END })

const userRoleUpdateStartAction = name => ({
	type: USER_ROLE_UPDATE_START,
	payload: name,
})

const userRoleUpdateEndAction = () => ({
	type: USER_ROLE_UPDATE_END,
})

const userRoleModalStateSet = state => ({
	type: USER_ROLE_MODAL_STATE_SET,
	payload: state,
})

const userRoleUpdatingValuesSetAction = values => ({
	type: USER_ROLE_UPDATING_VALUES_SET,
	payload: values,
})

const userRoleUpdateAction = values => ({
	type: USER_ROLE_UPDATE,
	payload: values,
})

export {
	setLoading,
	setUserRole,
	setUserRoles,
	addPagination,
	setFormLoading,
	apiUserRoleError,
	fetchUserRoleList,
	setUserRoleErrors,
	userRoleUpdateGet,
	userRoleUpdateAction,
	userRoleModalStateSet,
	setUserRoleUpdatedData,
	createOrUpdateUserRoles,
	userRoleDeleteEndAction,
	userRoleUpdateEndAction,
	userRoleSetRoleTypeAction,
	userRoleDeleteStartAction,
	userRoleUpdateStartAction,
	userRoleUpdatingValuesSetAction,
}
