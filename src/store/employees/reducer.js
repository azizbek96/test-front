import {
    SET_LOADING,
    ADD_PAGINATION,
    PUSH_EMPLOYEES,
    EMPLOYEES_MESSAGE,
    EMPLOYEES_MESSAGE_CLEAR,
    EMPLOYEES_UPDATE_SET_VALUES,
    EMPLOYEES_IS_UPDATING
} from './actionTypes'

const initialState = {
    error: {},
    result: {},
    apiError: {},
    employees: [],
    message: '',
    errorType: 'ERROR', //SUCCESS
    urlParams: {},
    pagination: {},
    permissions: {},
    isUpdating: false,
    formErrors: {},
    deleting: null,
    currentStep: 1,
    maxPassedStep: 1,
    isLoading: false,
    updatingValues: {},
    formLoading: false,
}

const reducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_LOADING:
            return {...state, isLoading: payload}
        case ADD_PAGINATION:
            return {...state, pagination: payload}
        case PUSH_EMPLOYEES:
            return {...state, employees: payload.data, permissions : payload.permissions}
        case EMPLOYEES_IS_UPDATING:
            return { ...state, isUpdating: payload}
        case EMPLOYEES_MESSAGE:
            return {...state, message: payload.message, errorType: payload.errorType}
        case EMPLOYEES_MESSAGE_CLEAR:
            return {...state, message: initialState.message, errorType: initialState.errorType}
        case EMPLOYEES_UPDATE_SET_VALUES:
            return { ...state, updatingValues: payload}
        default:
            return state
    }
}

export default reducer
