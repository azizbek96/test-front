import {put, call, select, takeLatest, takeLeading} from 'redux-saga/effects'
import {isDate, isNull, isEmpty, isNumber} from 'lodash'
import {format} from 'date-fns'

import {
    pushEmployees, setLoading, addPagination, employeesMessage,
    employeesMessageClear, employeesUpdateSetValues, employeesIsUpdating
} from './actions'
import {
    EMPLOYEES_DELETE,
    EMPLOYEES_UPDATE_REQ,
    FETCH_EMPLOYEES_REQUEST,
    EMPLOYEES_UPDATE_PUT_REQ,
    EMPLOYEES_CREATE_POST_REQ
} from './actionTypes'
import {
    FETCH_EMPLOYEES_URL,
    EMPLOYEES_DELETE_URL,
    EMPLOYEES_UPDATE_URL,
    CREATE_EMPLOYEES_URL

} from '../../helpers/url_helper'
import {api} from '../../helpers/api_helper'
import {Url} from '../../utils/url'
import {apiUsersError, setCreateResult, setFormLoading, usersError} from "../users/actions";

//*** EMPLOYEES GET START ***//
const fetchEmployeesReq = async (page, sortField, sortOrder, filters = {}) => {
    const filterParams = {}

    Object.keys(filters).forEach(key => {
        if (filters[key].filterType === 'TEXT') {
            filterParams[key] = filters[key].filterVal
        } else if (filters[key].filterType === 'DATE' && isDate(filters[key].filterVal.date)) {
            filterParams[key] = format(filters[key].filterVal.date, 'dd.MM.yyyy')
            filterParams[`${key}Comparator`] = filters[key].filterVal.comparator ?? '='
        }
    })

    const params = {
        page,
        sort: sortField && (sortOrder === 'asc' ? sortField : '-' + sortField),
    }

    return await api.get(Url.buildUrl(FETCH_EMPLOYEES_URL, params))
}

function* fetchEmployeesSaga({payload: {page = 1, sortField, sortOrder, filters}}) {
    try {
        yield put(setLoading(true))
        const {
            data: response,
            pagination
        } = yield call(fetchEmployeesReq, page, sortField, sortOrder, filters)

        if (response.code === 200) {
            yield put(addPagination(pagination))
            yield put(pushEmployees(response))
        } else {
            // yield put(employeesError(response))
        }
    } catch (error) {
        // yield put(employeesApiError(error))
    } finally {
        yield put(setLoading(false))
    }
}

//*** EMPLOYEES GET END ***//

//*** EMPLOYEES CREATE START ***//
const employeeCreateReq = async values => await api.post(CREATE_EMPLOYEES_URL, values)

function* employeeCreate({ payload }) {
    try {
        console.log("saga", payload)
        const { data: response } = yield call(employeeCreateReq, {...payload})

        if (response.code === 200) {
            yield put(employeesMessage({message: response.message, errorType: 'SUCCESS'}))
        } else {
            yield put(employeesMessage({message: response.message, errorType: 'ERROR'}))
        }
    } catch (error) {
    } finally {
        yield put(employeesMessageClear())
    }
}
//*** EMPLOYEES CREATE END ***//

//*** EMPLOYEES UPDATE START ***//
const employeesUpdateReq = async id => await api.get(EMPLOYEES_UPDATE_URL + '?id=' + id)
// GET
function* employeesUpdateGetSaga({payload: id}) {
    yield put(employeesIsUpdating(true))
    try {
        const {data: response} = yield call(employeesUpdateReq, id)
        if (response.code === 200) {
            yield put(employeesUpdateSetValues(response.data.model))
        } else {
        }
    } catch (error) {
    } finally {
        yield put(employeesUpdateSetValues({}))
    }
}

// SEND

const employeesUpdateSetReq = async (id, values) => await api.put(EMPLOYEES_UPDATE_URL + '?id=' + id, values)

function* employeesUpdateSendSaga({payload: values}) {
    try {
        const {data: response} = yield call(employeesUpdateSetReq, values.id, {
            ...values,
        })

        if (response.code === 200) {
            yield put(employeesIsUpdating(false))
            yield put(employeesMessage({message: response.message, errorType: 'SUCCESS'}))
        } else {
            yield put(employeesMessage({message: response.message, errorType: 'ERROR'}))
        }
    } catch (error) {
    } finally {
        yield put(employeesMessageClear())
    }
}

//*** EMPLOYEES UPDATE END ***//

//*** EMPLOYEES DELETE START ***//
const employeesDeleteReq = async id => await api.delete(EMPLOYEES_DELETE_URL + '?id=' + id)

function* employeesDeleteSaga({payload: id}) {
    try {
        const {data: response} = yield call(employeesDeleteReq, id)

        if (response.code === 200) {
            yield put(pushEmployees(response.data))
            yield put(employeesMessage({message: response.message, errorType: 'SUCCESS'}))
        } else {
            yield put(employeesMessage({message: response.message, errorType: 'ERROR'}))
        }
    } catch (error) {
        yield put(employeesMessageClear())
    }
}

//*** EMPLOYEES DELETE END ***//

function* employeesSaga() {
    yield takeLeading(FETCH_EMPLOYEES_REQUEST, fetchEmployeesSaga)
    yield takeLeading(EMPLOYEES_UPDATE_REQ, employeesUpdateGetSaga)
    yield takeLeading(EMPLOYEES_UPDATE_PUT_REQ, employeesUpdateSendSaga)
    yield takeLeading(EMPLOYEES_DELETE, employeesDeleteSaga)
    yield takeLeading(EMPLOYEES_CREATE_POST_REQ, employeeCreate)
}

export default employeesSaga
