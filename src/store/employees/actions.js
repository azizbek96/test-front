import {
    SET_LOADING,
    ADD_PAGINATION,
    PUSH_EMPLOYEES,
    EMPLOYEES_DELETE,
    EMPLOYEES_MESSAGE,
    EMPLOYEES_UPDATE_REQ,
    EMPLOYEES_IS_UPDATING,
    FETCH_EMPLOYEES_REQUEST,
    EMPLOYEES_MESSAGE_CLEAR,
    EMPLOYEES_UPDATE_PUT_REQ,
    EMPLOYEES_UPDATE_SET_VALUES, EMPLOYEES_CREATE_POST_REQ
} from './actionTypes'

const setLoading = status => ({type: SET_LOADING, payload: status})
const fetchEmployeesReq = (page, sortField, sortOrder, filters) => ({
    type: FETCH_EMPLOYEES_REQUEST,
    payload: {page, sortField, sortOrder, filters},
})
const setFormLoading = loading => ({
    type: SET_EMPLOYEES_FORM_LOADING,
    payload: loading,
})
const employeesIsUpdating = boolean => ({
    type: EMPLOYEES_IS_UPDATING,
    payload: boolean,
})
const employeesUpdateReq = id => ({
    type: EMPLOYEES_UPDATE_REQ,
    payload: id,
})
const employeesUpdateSetValues = data => ({
    type: EMPLOYEES_UPDATE_SET_VALUES,
    payload: data,
})
const employeesUpdatePutReq = data => ({
    type: EMPLOYEES_UPDATE_PUT_REQ,
    payload: data,
})
const employeesDelete = id => ({
    type: EMPLOYEES_DELETE,
    payload: id,
})
const employeesMessage = message => ({
    type: EMPLOYEES_MESSAGE,
    payload: message,
})
const employeesMessageClear = () => ({
    type: EMPLOYEES_MESSAGE_CLEAR,
})
const addPagination = pagination => ({
    type: ADD_PAGINATION,
    payload: pagination,
})
const pushEmployees = data => ({
    type: PUSH_EMPLOYEES,
    payload: data,
})

const employeesCreatePostReq = data => ({
    type: EMPLOYEES_CREATE_POST_REQ,
    payload: data,
})
export {
    setLoading,
    addPagination,
    pushEmployees,
    setFormLoading,
    employeesDelete,
    employeesMessage,
    fetchEmployeesReq,
    employeesUpdateReq,
    employeesIsUpdating,
    employeesMessageClear,
    employeesCreatePostReq,
    employeesUpdatePutReq,
    employeesUpdateSetValues
}
