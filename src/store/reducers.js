import { reducer as formReducer } from 'redux-form'
import { combineReducers } from 'redux'

import UserPermissions from './userPermissions/reducer'
import EmployeeRelAttachment from './employeeRelAttachment/reducer'
import Employees from './employees/reducer'
import Dashboard from './dashboard/reducer'
import UserRoles from './userRoles/reducer'
import Layout from './layout/reducer'
import Users from './users/reducer'
import Auth from './auth/reducer'
import App from './app/reducer'

const rootReducer = combineReducers({
	App,
	Auth,
	Users,
	Layout,
	Dashboard,
	UserRoles,
	Employees,
	UserPermissions,
	EmployeeRelAttachment

})

export default rootReducer
