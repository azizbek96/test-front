import { all, call, fork } from 'redux-saga/effects'

import employeeRelAttachmentSaga from './employeeRelAttachment/saga'
import userPermissionsSaga from './userPermissions/saga.ts'
import userRolesSaga from './userRoles/saga'
import employeesSaga from './employees/saga'
import dashboardSaga from './dashboard/saga'
import LayoutSaga from './layout/saga'
import usersSaga from './users/saga'
import authSaga from './auth/saga'
import appSaga from './app/saga'

export default function* rootSaga() {
	yield all([
		call(authSaga),
		call(appSaga),
		fork(usersSaga),
		fork(LayoutSaga),
		fork(employeesSaga),
		fork(dashboardSaga),
		fork(userRolesSaga),
		fork(userPermissionsSaga),
		fork(employeeRelAttachmentSaga),
	])
}
