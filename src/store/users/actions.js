import {
	API_ERROR,
	PUSH_USERS,
	USER_CREATE,
	SET_LOADING,
	USERS_ERROR,
	USER_DELETE,
	USER_UPDATE,
	ADD_PAGINATION,
	USERS_GET_LIST,
	SET_USER_ROLES,
	SET_STATUS_LIST,
	USERS_GET_PARAMS,
	SET_FORM_LOADING,
	USERS_UPDATE_END,
	SET_EMPLOYEE_LIST,
	USER_START_UPDATE,
	USER_CREATE_RESULT,
	SET_USER_ROLE_TYPE,
	USER_DELETE_RESULT,
	USER_UPDATE_RESULT,
	USERS_CLEAR_MESSAGES,
	USERS_SET_UPDATING_VALUES, SET_MODAL,
} from './actionTypes'

const usersUpdateEnd = () => ({ type: USERS_UPDATE_END })

const getUsersList = (page, sortField, sortOrder, filters) => ({
	type: USERS_GET_LIST,
	payload: { page, sortField, sortOrder, filters },
})

const usersDelete = id => ({ type: USER_DELETE, payload: id })

const fetchEmployeesAction = () => ({ type: USERS_GET_PARAMS })

const sendCreateUser = values => ({ type: USER_CREATE, values })

const pushUsers = users => ({ type: PUSH_USERS, payload: users })

const usersClearMessages = () => ({ type: USERS_CLEAR_MESSAGES })

const usersError = error => ({ type: USERS_ERROR, payload: error })

const apiUsersError = error => ({ type: API_ERROR, payload: error })

const usersUpdate = values => ({ type: USER_UPDATE, payload: values })

const setLoading = loading => ({ type: SET_LOADING, payload: loading })

const usersStartUpdate = id => ({ type: USER_START_UPDATE, payload: id })

const setModal = boolean => ({ type: SET_MODAL, payload: boolean })

const setFormLoading = loading => ({ type: SET_FORM_LOADING, payload: loading })

const usersUpdatingValuesSet = values => ({
	type: USERS_SET_UPDATING_VALUES,
	payload: values,
})

const usersUpdateResultSet = message => ({
	type: USER_UPDATE_RESULT,
	payload: message,
})

const usersDeleteResult = message => ({
	type: USER_DELETE_RESULT,
	payload: message,
})

const addPagination = pagination => ({
	type: ADD_PAGINATION,
	payload: pagination,
})

const setUserRoleType = roleType => ({
	type: SET_USER_ROLE_TYPE,
	payload: roleType,
})

const setEmployeesList = values => ({
	type: SET_EMPLOYEE_LIST,
	payload: values,
})

const setCreateResult = values => ({
	type: USER_CREATE_RESULT,
	payload: values,
})

const setStatusList = values => ({
	type: SET_STATUS_LIST,
	payload: values,
})

const setUserRoles = roles => ({
	type: SET_USER_ROLES,
	payload: roles,
})

export {
	pushUsers,
	setModal,
	setLoading,
	usersError,
	usersDelete,
	usersUpdate,
	setUserRoles,
	getUsersList,
	addPagination,
	apiUsersError,
	setStatusList,
	setFormLoading,
	sendCreateUser,
	usersUpdateEnd,
	setCreateResult,
	setUserRoleType,
	setEmployeesList,
	usersStartUpdate,
	usersDeleteResult,
	usersClearMessages,
	usersUpdateResultSet,
	fetchEmployeesAction,
	usersUpdatingValuesSet,
}
