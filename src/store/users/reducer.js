import {
	API_ERROR,
	PUSH_USERS,
	SET_LOADING,
	USERS_ERROR,
	ADD_PAGINATION,
	SET_USER_ROLES,
	SET_STATUS_LIST,
	SET_FORM_LOADING,
	USERS_UPDATE_END,
	SET_EMPLOYEE_LIST,
	USER_START_UPDATE,
	SET_USER_ROLE_TYPE,
	USER_CREATE_RESULT,
	USER_DELETE_RESULT,
	USER_UPDATE_RESULT,
	USERS_CLEAR_MESSAGES,
	USERS_SET_UPDATING_VALUES, SET_MODAL,
} from './actionTypes'

const initialState = {
	users: [],
	roles: [],
	errors: [],
	result: {},
	apiError: {},
	roleTypes: {},
	employees: [],
	loading: false,
	isModal: false,
	statusList: [],
	usersError: {},
	pagination: {},
	permissions: {},
	isUpdating: false,
	errorType: 'ERROR', //SUCCESS
	formLoading: false,
	updatingValues: {},
}

const reducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case PUSH_USERS:
			return { ...state, users: payload.data, permissions: payload.permissions }

		case SET_USER_ROLES:
			return { ...state, roles: payload }

		case SET_LOADING:
			return { ...state, loading: payload }

		case API_ERROR:
			return { ...state, apiError: payload }

		case SET_EMPLOYEE_LIST:
			return { ...state, employees: payload }

		case SET_USER_ROLE_TYPE:
			return { ...state, roleTypes: payload }

		case SET_STATUS_LIST:
			return { ...state, statusList: payload }

		case SET_MODAL:
			if (!payload)
				return { ...state, isModal: payload,isUpdating:false}
			else
				return { ...state, isModal: payload }

		case ADD_PAGINATION:
			return { ...state, pagination: payload }

		case USER_START_UPDATE:
			return { ...state, isUpdating: payload }

		case SET_FORM_LOADING:
			return { ...state, formLoading: payload }

		case USERS_SET_UPDATING_VALUES:
			return { ...state, updatingValues: payload }

		case USERS_CLEAR_MESSAGES:
			return { ...state, result: initialState.result }

		case USER_UPDATE_RESULT:
			return { ...state, result: payload.message, errorType: 'SUCCESS' }

		case USER_DELETE_RESULT:
			return { ...state, errorType: payload.type, result: payload.message }

		case USER_CREATE_RESULT:
			return {
				...state,
				errorType: 'SUCCESS',
				result: payload.message,
				users: [payload.data, ...state.users].filter(Boolean),
			}

		case USERS_UPDATE_END:
			return {
				...state,
				isUpdating: initialState.isUpdating,
				updatingValues: initialState.updatingValues,
			}

		case USERS_ERROR:
			return {
				...state,
				errorType: 'ERROR',
				result: payload.message,
				errors: payload.data.errors,
			}

		default:
			return state
	}
}

export default reducer
