import { put, call, takeLeading } from 'redux-saga/effects'
import { isDate } from 'lodash'

import {
	pushUsers,
	setLoading,
	usersError,
	setUserRoles,
	addPagination,
	apiUsersError,
	setStatusList,
	setFormLoading,
	usersUpdateEnd,
	setUserRoleType,
	setCreateResult,
	setEmployeesList,
	usersDeleteResult,
	usersClearMessages,
	usersUpdateResultSet,
	usersUpdatingValuesSet, setModal,
} from './actions'
import { USER_CREATE, USER_DELETE, USER_UPDATE, USERS_GET_LIST, USERS_GET_PARAMS, USER_START_UPDATE } from './actionTypes'
import { USERS_INDEX_URL, USERS_CREATE_URL, USERS_DELETE_URL, USERS_UPDATE_URL } from '../../helpers/url_helper'
import { api } from '../../helpers/api_helper'
import { Url } from '../../utils/url'
import format from 'date-fns/format'

const usersGetRequest = async (page, sortField, sortOrder, filters = {}) => {
	const filterParams = {}

	Object.keys(filters).forEach(key => {
		if (filters[key].filterType === 'TEXT') {
			filterParams[key] = filters[key].filterVal
		} else if (filters[key].filterType === 'DATE' && isDate(filters[key].filterVal.date)) {
			filterParams[key] = format(filters[key].filterVal.date, 'dd.MM.yyyy')
			filterParams[`${key}Comparator`] = filters[key].filterVal.comparator ?? '='
		}
	})

	const params = {
		page,
		...filterParams,
		sort: sortField && (sortOrder === 'asc' ? sortField : '-' + sortField),
	}
	return await api.get(Url.buildUrl(USERS_INDEX_URL, params))
}

function* usersGetList({ payload: { page = 1, sortField, sortOrder, filters } }) {
	try {
		yield put(setLoading(true))
		const { data: response, pagination } = yield call(usersGetRequest, page, sortField, sortOrder, filters)

		if (response.code === 200) {
			yield put(pushUsers(response))
			yield put(addPagination(pagination))
			yield put(setStatusList(response.statusList))
		} else {
			yield put(usersError(response.data))
		}
	} catch (error) {
		console.log(error)
		yield put(apiUsersError(error))
	} finally {
		yield put(setLoading(false))
	}
}

const params = async () => {
	return await api.get(USERS_CREATE_URL)
}

function* getParams() {
	try {
		const { data: response } = yield call(params)

		if (response.code === 200) {
			yield put(setUserRoles(response.data.roles))
			yield put(setStatusList(response.data.statusList))
			yield put(setUserRoleType(response.data.roleTypes))
			yield put(setEmployeesList(response.data.employeeList))
		} else {
			yield put(usersError(response))
		}
	} catch (error) {
		yield put(apiUsersError(error))
	}
}

const create = async userData => await api.post(USERS_CREATE_URL, userData)

function* userCreate({ values }) {
	try {
		yield put(setFormLoading(true))

		const roles = {}

		values.roles.forEach(role => {
			Object.defineProperty(roles, role.value, {
				writable: false,
				enumerable: true,
				value: role.label,
			})
		})

		const { data: response } = yield call(create, { ...values, roles })

		if (response.code === 200) {
			yield put(
				setCreateResult({
					data: response.data,
					message: response.message,
				})
			)
			yield put(setModal(false))
		} else if (response.code === 403) {
			yield put(usersError(response))
		}
	} catch (error) {
		yield put(apiUsersError(error.toString()))
	} finally {
		yield put(setFormLoading(false))
	}
}

const deleteReq = async id => await api.delete(USERS_DELETE_URL + '?id=' + id)

function* deleteWorker({ payload: id }) {
	try {
		const { data: response } = yield call(deleteReq, id)

		if (response.code === 200) {
			yield put(pushUsers(response.data))
			yield put(usersDeleteResult({ type: 'SUCCESS', message: response.message }))
		} else {
			yield put(usersError(response))
		}
	} catch (error) {
		yield put(apiUsersError(error.toString()))
	}
}

const startUpdateReq = async id => await api.get(USERS_UPDATE_URL + '?id=' + id)

function* startUpdateWorker({ payload: id }) {
	try {
		yield put(setFormLoading(true))
		const { data: response } = yield call(startUpdateReq, id)

		if (response.code === 200) {
			yield put(
				usersUpdatingValuesSet({
					...response.data.values,
					userRoles: response.data.userRoles,
				})
			)
			yield put(setUserRoles(response.data.roles))
			yield put(setEmployeesList(response.data.employeeList))
		} else {
			yield put(usersError(response))
		}
	} catch (error) {
		yield put(apiUsersError(error.toString()))
	} finally {
		yield put(setFormLoading(false))
	}
}

const updateReq = async (id, values) => await api.put(USERS_UPDATE_URL + '?id=' + id, values)

function* updateWorker({ payload: values }) {
	try {
		yield put(usersClearMessages())

		const roles = {}

		_.uniqBy(values.roles, ({ value }) => value).forEach(role => {
			Object.defineProperty(roles, role.value, {
				writable: false,
				enumerable: true,
				value: role.label,
			})
		})

		const { data: response, pagination } = yield call(updateReq, values.id, {
			...values,
			roles,
		})

		if (response.code === 200) {
			yield put(pushUsers(response.data))
			yield put(addPagination(pagination))
			yield put(usersUpdateResultSet({ message: response.message }))
			yield put(usersUpdateEnd())
			yield put(setModal(false))
		} else {
			yield put(usersError(response))
		}
	} catch (error) {
		yield put(apiUsersError(error.toString()))
	}
}

function* usersSaga() {
	yield takeLeading(USER_CREATE, userCreate)
	yield takeLeading(USER_DELETE, deleteWorker)
	yield takeLeading(USER_UPDATE, updateWorker)
	yield takeLeading(USERS_GET_PARAMS, getParams)
	yield takeLeading(USERS_GET_LIST, usersGetList)
	yield takeLeading(USER_START_UPDATE, startUpdateWorker)
}

export default usersSaga
