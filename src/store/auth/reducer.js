import { API_ERROR, LOGIN_ERROR, ADMIN_LOGIN_ERROR, CLEAR_ERRORS, AUTH_SET_FORM_LOADING } from './actionTypes'

const initialState = {
	apiError: {},
	loginErrors: {},
	formLoading: false,
}

const reducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case API_ERROR:
			return { ...state, apiError: payload }
		case LOGIN_ERROR:
		case ADMIN_LOGIN_ERROR:
			return { ...state, loginErrors: payload }
		case CLEAR_ERRORS:
			return {
				...state,
				apiError: initialState.apiError,
				loginError: initialState.loginError,
			}
		case AUTH_SET_FORM_LOADING:
			return { ...state, formLoading: payload }
		default:
			return state
	}
}

export default reducer
