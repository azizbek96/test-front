import { API_ERROR, AUTH_USER, LOGIN_USER, ADMIN_LOGIN_USER, LOGIN_ERROR, LOGOUT_USER, CLEAR_ERRORS, LOGIN_SUCCESS, LOGOUT_USER_SUCCESS, AUTH_SET_FORM_LOADING } from './actionTypes'

const loginUser = user => ({
	type: LOGIN_USER,
	payload: { user },
})

const adminLoginUser = user => ({
	type: ADMIN_LOGIN_USER,
	payload: { user },
})

const loginSuccess = user => ({
	type: LOGIN_SUCCESS,
	payload: user,
})

const loginError = error => ({ type: LOGIN_ERROR, payload: error })

const logoutUser = history => ({
	type: LOGOUT_USER,
	payload: { history },
})

const logoutUserSuccess = () => ({
	type: LOGOUT_USER_SUCCESS,
	payload: {},
})

const authUser = () => ({
	type: AUTH_USER,
})

const apiError = error => ({
	type: API_ERROR,
	payload: error,
})

const clearErrors = () => ({ type: CLEAR_ERRORS })

const authSetFormLoadingAction = loading => ({
	type: AUTH_SET_FORM_LOADING,
	payload: loading,
})

export { apiError, 
	authUser, 
	adminLoginUser,
	loginUser, 
	logoutUser, 
	loginError, 
	clearErrors, 
	loginSuccess, 
	logoutUserSuccess, 
	authSetFormLoadingAction }
