const AUTH_USER = 'AUTH::AUTH_USER'
const ADMIN_LOGIN_USER = 'AUTH::ADMIN_LOGIN_USER'
const LOGIN_USER = 'AUTH::LOGIN_USER'
const CLEAR_ERRORS = 'AUTH::CLEAR_ERROR'
const LOGOUT_USER = 'AUTH::LOGOUT_USER'
const LOGIN_ERROR = 'AUTH::LOGIN_ERROR'
const ADMIN_LOGIN_ERROR = 'AUTH::ADMIN_LOGIN_ERROR'
const API_ERROR = 'AUTH::LOGIN_API_ERROR'
const LOGIN_SUCCESS = 'AUTH::LOGIN_SUCCESS'
const AUTH_SET_FORM_LOADING = 'AUTH::SET_FORM_LOADING'
const LOGOUT_USER_SUCCESS = 'AUTH::LOGOUT_USER_SUCCESS'

export { API_ERROR, 
    AUTH_USER, 
    LOGIN_USER, 
    LOGIN_ERROR, 
    LOGOUT_USER, 
    CLEAR_ERRORS, 
    LOGIN_SUCCESS, 
    ADMIN_LOGIN_ERROR,
    LOGOUT_USER_SUCCESS, 
    AUTH_SET_FORM_LOADING , 
    ADMIN_LOGIN_USER}
