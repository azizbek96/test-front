import { put, call, takeLeading } from 'redux-saga/effects'

import { apiError, loginError, clearErrors, loginSuccess, authSetFormLoadingAction } from './actions'
import { hideLoader, showLoader, appMenuGet, changeAuthTrue, changeAuthFalse } from '../app/actions'
import { LOGIN, ADMIN_LOGIN, AUTH_URL } from '../../helpers/url_helper'
import { LOGIN_USER,ADMIN_LOGIN_USER,LOGOUT_USER } from './actionTypes'
import { api } from '../../helpers/api_helper'

function* logoutUser() {
	try {
		localStorage.removeItem('token')
		localStorage.removeItem('UserName')
		sessionStorage.removeItem('roles')
		sessionStorage.removeItem('menu')
		yield put(changeAuthFalse())
	} catch (error) {
		yield put(apiError(error))
	}
}

const auth = async () => await api.get(AUTH_URL)

function* authUser() {
	try {
		yield put(showLoader())
		const response = yield call(auth)
		if (response.status === 200) {
			// localStorage.setItem('token', response.data.token)
			yield put(changeAuthTrue())
			yield put(loginSuccess())
		}
	} catch (error) {
		yield put(apiError(error))
	} finally {
		yield put(hideLoader())
	}
}
async function adminLogin(username, password) {
	return await api.post(ADMIN_LOGIN, { username, password }, false)
}

function* adminLoginUser({ payload: { user } }) {
	try {
		yield put(authSetFormLoadingAction(true))
		yield put(clearErrors())
		const { data: response } = yield call(adminLogin, user.username, user.password)
		if (response.code === 200) {
			localStorage.setItem('token', response.data.token)
			localStorage.setItem('UserName', response.data?.user?.username)
			sessionStorage.setItem('roles', btoa(JSON.stringify(response.data?.user?.roles)))
			yield put(changeAuthTrue())
			yield put(loginSuccess())
			yield put(appMenuGet())
		} else {
			localStorage.removeItem('token')
			localStorage.removeItem('UserName')
			sessionStorage.removeItem('roles')
			yield put(loginError(response.data))
		}
	} catch (error) {
		yield put(changeAuthFalse())
		yield put(apiError(error.toString()))
	} finally {
		yield put(authSetFormLoadingAction(false))
	}
}
function* authSaga() {
	yield takeLeading(ADMIN_LOGIN_USER, adminLoginUser)
	yield takeLeading(LOGOUT_USER, logoutUser)
}

export default authSaga
