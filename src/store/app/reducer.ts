import { Reducer } from 'redux'

import { AUTH_TRUE, AUTH_FALSE, HIDE_LOADER, SHOW_LOADER, APP_MENU_SET, APP_MENU_ACTIVE_SET } from './actionTypes'

export interface MenuItem {
	to: string
	icon: string
	label: string
}

export interface Menu {
	label: string
	content: MenuItem[]
}

interface IAppState {
	menu: Menu[]
	isAuth: boolean
	isLoading: boolean
	activeMenu: string[]
}

const initialState = {
	menu: [],
	activeMenu: [],
	isLoading: false,
	isAuth: !!localStorage.getItem('token'),
}

const app: Reducer<IAppState, { type: string; payload: any }> = (state = initialState, { type, payload }): IAppState => {
	switch (type) {
		case AUTH_TRUE:
			return { ...state, isAuth: true }
		case AUTH_FALSE:
			return { ...state, isAuth: false }
		case APP_MENU_SET:
			return { ...state, menu: payload }
		case SHOW_LOADER:
			return { ...state, isLoading: true }
		case HIDE_LOADER:
			return { ...state, isLoading: false }
		case APP_MENU_ACTIVE_SET:
			return { ...state, activeMenu: payload }
		default:
			return state
	}
}

export default app
