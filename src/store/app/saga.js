import { put, call, takeLeading } from 'redux-saga/effects'

import { USER_ROLE_MENU_URL } from '../../helpers/url_helper'
import { APP_MENU_GET, APP_MENU_UPDATE } from './actionTypes'
import { api } from '../../helpers/api_helper'
import { appMenuSetAction } from './actions'

const menuReq = async () => await api.get(USER_ROLE_MENU_URL)

function* menuWorker() {
	try {
		let newMenu

		if (!sessionStorage.getItem('menu')) {
			const { data: response } = yield call(menuReq)
			if (response.code === 200) {
				newMenu = response.data
				sessionStorage.setItem('menu', btoa(JSON.stringify(newMenu)))
			}
		} else {
			newMenu = JSON.parse(atob(sessionStorage.getItem('menu')))
		}
		yield put(appMenuSetAction(newMenu))
	} catch (error) {
		console.log(error)
	}
}

function* menuUpdateWorker({ payload }) {
	try {
		yield put(appMenuSetAction(payload))

		sessionStorage.setItem('menu', btoa(JSON.stringify(payload)))
	} catch (error) {
		console.log(error)
	}
}

function* appSaga() {
	yield takeLeading(APP_MENU_GET, menuWorker)
	yield takeLeading(APP_MENU_UPDATE, menuUpdateWorker)
}

export default appSaga
