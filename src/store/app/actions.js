import { AUTH_TRUE, AUTH_FALSE, HIDE_LOADER, SHOW_LOADER, APP_MENU_GET, APP_MENU_SET, APP_MENU_UPDATE, APP_MENU_ACTIVE_SET } from './actionTypes'

const showLoader = () => ({ type: SHOW_LOADER })
const hideLoader = () => ({ type: HIDE_LOADER })
const appMenuGet = () => ({ type: APP_MENU_GET })
const changeAuthTrue = () => ({ type: AUTH_TRUE })
const changeAuthFalse = () => ({ type: AUTH_FALSE })
const appMenuSetAction = menu => ({ type: APP_MENU_SET, payload: menu })
const appMenuUpdateAction = payload => ({ type: APP_MENU_UPDATE, payload })
const appMenuActiveSetAction = activeMenu => ({
	type: APP_MENU_ACTIVE_SET,
	payload: activeMenu,
})

export { appMenuGet, hideLoader, showLoader, changeAuthTrue, changeAuthFalse, appMenuSetAction, appMenuUpdateAction, appMenuActiveSetAction }
