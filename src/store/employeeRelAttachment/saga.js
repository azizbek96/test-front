import {put, call, select, takeLatest, takeLeading} from 'redux-saga/effects'
import {isDate, isNull, isEmpty, isNumber} from 'lodash'
import {format} from 'date-fns'

import {
    pushEmployeeRelAttachment,
    setLoading,
    addPagination,
    employeeRelAttachmentMessage,
    employeeRelAttachmentMessageClear
} from './actions'
import {
    EMPLOYEE_REL_ATTACHMENT_DELETE,
    FETCH_EMPLOYEE_REL_ATTACHMENT_REQUEST,
    EMPLOYEE_REL_ATTACHMENT_CREATE_POST_REQ
} from './actionTypes'
import {
    FETCH_EMPLOYEE_REL_ATTACHMENT_URL,
    EMPLOYEE_REL_ATTACHMENT_DELETE_URL,
    EMPLOYEE_REL_ATTACHMENT_UPDATE_URL,
    CREATE_EMPLOYEE_REL_ATTACHMENT_URL

} from '../../helpers/url_helper'
import {api} from '../../helpers/api_helper'
import {Url} from '../../utils/url'
import {apiUsersError, setCreateResult, setFormLoading, usersError} from "../users/actions";

//*** EMPLOYEE Rel attachment GET START ***//
const fetchEmployeeRelAttachmentReq = async (page, sortField, sortOrder, filters = {}) => {
    const filterParams = {}

    Object.keys(filters).forEach(key => {
        if (filters[key].filterType === 'TEXT') {
            filterParams[key] = filters[key].filterVal
        } else if (filters[key].filterType === 'DATE' && isDate(filters[key].filterVal.date)) {
            filterParams[key] = format(filters[key].filterVal.date, 'dd.MM.yyyy')
            filterParams[`${key}Comparator`] = filters[key].filterVal.comparator ?? '='
        }
    })

    const params = {
        page,
        sort: sortField && (sortOrder === 'asc' ? sortField : '-' + sortField),
    }

    return await api.get(Url.buildUrl(FETCH_EMPLOYEE_REL_ATTACHMENT_URL, params))
}

function* fetchEmployeeRelAttaChmentSaga({payload: {page = 1, sortField, sortOrder, filters}}) {
    try {
        yield put(setLoading(true))
        const {
            data: response,
            pagination
        } = yield call(fetchEmployeeRelAttachmentReq, page, sortField, sortOrder, filters)

        if (response.code === 200) {
            yield put(addPagination(pagination))
            yield put(pushEmployeeRelAttachment(response))
        } else {
            // yield put(employeesError(response))
        }
    } catch (error) {
        // yield put(employeesApiError(error))
    } finally {
        yield put(setLoading(false))
    }
}

//*** EMPLOYEES GET END ***//
//
//*** EMPLOYEES CREATE START ***//
const create = async values => {
    const formData = new FormData()

    if (values?.user_files?.length > 0) {
        for (let i = 0; i < values.user_files.length; i++) {
            const file = await fetch(values.user_files[i].path)
            formData.append('user_files'.concat('[', i, ']'), await file.blob(), `${values.user_files[i].name}`)
        }

        delete values.fileSop
    }
    formData.append('json', JSON.stringify(values))

    return await api.post(CREATE_EMPLOYEE_REL_ATTACHMENT_URL, formData, true, {
        headers: { 'Content-Type': 'multipart/form-data' },
    })
}
function* employeeRelAttachmentCreate({ payload }) {
    try {
        console.log("saga", payload)
        const { data: response } = yield call(create, {...payload})

        if (response.code === 200) {
            yield put(employeeRelAttachmentMessage({message: response.message, errorType: 'SUCCESS'}))
        } else {
            yield put(employeeRelAttachmentMessage({message: response.message, errorType: 'ERROR'}))
        }
    } catch (error) {
    } finally {
        yield put(employeeRelAttachmentMessageClear())
    }
}

//*** EMPLOYEES DELETE START ***//
const employeesDeleteReq = async id => await api.delete(EMPLOYEE_REL_ATTACHMENT_DELETE_URL + '?id=' + id)

function* employeesDeleteSaga({payload: id}) {
    try {
        const {data: response} = yield call(employeesDeleteReq, id)

        if (response.code === 200) {
            yield put(pushEmployeeRelAttachment(response))
            yield put(employeeRelAttachmentMessage({message: response.message, errorType: 'SUCCESS'}))
        } else {
            yield put(employeeRelAttachmentMessage({message: response.message, errorType: 'ERROR'}))
        }
    } catch (error) {
        yield put(employeeRelAttachmentMessageClear())
    }
}

//*** EMPLOYEES DELETE END ***//

function* employeeRelAttachmentSaga() {
    yield takeLeading(FETCH_EMPLOYEE_REL_ATTACHMENT_REQUEST, fetchEmployeeRelAttaChmentSaga)
    yield takeLeading(EMPLOYEE_REL_ATTACHMENT_DELETE, employeesDeleteSaga)
    yield takeLeading(EMPLOYEE_REL_ATTACHMENT_CREATE_POST_REQ, employeeRelAttachmentCreate)
}

export default employeeRelAttachmentSaga
