import { isNull, isNumber } from 'lodash'

const dateRegExp = /^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/

const clearIncomingValues = values => {
	Object.keys(values).forEach(key => {
		if (!isNumber(values[key])) {
			if ((typeof values[key] === 'string' && values[key].length === 0) || isNull(values[key])) {
				values[key] = undefined
				return
			} else if (dateRegExp.test(values[key])) {
				values[key] = new Date(values[key])
			} else if (Array.isArray(values[key])) {
				values[key].forEach((field, i) => {
					if (typeof field === 'object') {
						Object.keys(values[key][i]).forEach(fieldKey => {
							if (dateRegExp.test(values[key][i][fieldKey])) {
								values[key][i][fieldKey] = new Date(values[key][i][fieldKey])
							}
						})
					}
				})
			}
		}
	})

	return values
}

const normalizeSendingValues = (values, isUpdate = false) => {
	Object.keys(values).forEach(fieldKey => {
		if (!isNumber(values[fieldKey]) && !values[fieldKey] instanceof Date) {
			return delete values[fieldKey]
		} else if (isUpdate && fieldKey === 'avatar') {
			if (values[fieldKey]?.path?.startsWith('http')) return delete values[fieldKey]
		}
	})

	return values
}

export { clearIncomingValues, normalizeSendingValues }
