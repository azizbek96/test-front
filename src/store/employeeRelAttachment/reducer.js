import {
    SET_LOADING,
    ADD_PAGINATION,
    PUSH_EMPLOYEE_REL_ATTACHMENT,
    EMPLOYEE_REL_ATTACHMENT_MESSAGE,
    EMPLOYEE_REL_ATTACHMENT_MESSAGE_CLEAR,
    EMPLOYEE_REL_ATTACHMENT_UPDATE_SET_VALUES,
    EMPLOYEE_REL_ATTACHMENT_IS_UPDATING
} from './actionTypes'

const initialState = {
    error: {},
    result: {},
    apiError: {},
    employeeRelAttachment: [],
    message: '',
    errorType: 'ERROR', //SUCCESS
    urlParams: {},
    pagination: {},
    permissions: {},
    isUpdating: false,
    formErrors: {},
    deleting: null,
    currentStep: 1,
    maxPassedStep: 1,
    isLoading: false,
    updatingValues: {},
    formLoading: false,
}

const reducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_LOADING:
            return {...state, isLoading: payload}
        case ADD_PAGINATION:
            return {...state, pagination: payload}
        case PUSH_EMPLOYEE_REL_ATTACHMENT:
            return {...state, employeeRelAttachment: payload.data, permissions: payload.permissions}
        case EMPLOYEE_REL_ATTACHMENT_IS_UPDATING:
            return { ...state, isUpdating: payload}
        case EMPLOYEE_REL_ATTACHMENT_MESSAGE:
            return {...state, message: payload.message, errorType: payload.errorType}
        case EMPLOYEE_REL_ATTACHMENT_MESSAGE_CLEAR:
            return {...state, message: initialState.message, errorType: initialState.errorType}
        case EMPLOYEE_REL_ATTACHMENT_UPDATE_SET_VALUES:
            return { ...state, updatingValues: payload}
        default:
            return state
    }
}

export default reducer
