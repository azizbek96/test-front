import {
    SET_LOADING,
    ADD_PAGINATION,
    PUSH_EMPLOYEE_REL_ATTACHMENT,
    EMPLOYEE_REL_ATTACHMENT_DELETE,
    EMPLOYEE_REL_ATTACHMENT_MESSAGE,
    EMPLOYEE_REL_ATTACHMENT_UPDATE_REQ,
    EMPLOYEE_REL_ATTACHMENT_IS_UPDATING,
    FETCH_EMPLOYEE_REL_ATTACHMENT_REQUEST,
    EMPLOYEE_REL_ATTACHMENT_MESSAGE_CLEAR,
    EMPLOYEE_REL_ATTACHMENT_UPDATE_PUT_REQ,
    EMPLOYEE_REL_ATTACHMENT_UPDATE_SET_VALUES,
    EMPLOYEE_REL_ATTACHMENT_CREATE_POST_REQ
} from './actionTypes'

const setLoading = status => ({type: SET_LOADING, payload: status})
const fetchEmployeeRelAttachmentReq = (page, sortField, sortOrder, filters) => ({
    type: FETCH_EMPLOYEE_REL_ATTACHMENT_REQUEST,
    payload: {page, sortField, sortOrder, filters},
})
const setFormLoading = loading => ({
    type: SET_EMPLOYEE_REL_ATTACHMENT_FORM_LOADING,
    payload: loading,
})
const employeeRelAttachmentIsUpdating = boolean => ({
    type: EMPLOYEE_REL_ATTACHMENT_IS_UPDATING,
    payload: boolean,
})
const employeeRelAttachmentUpdateReq = id => ({
    type: EMPLOYEE_REL_ATTACHMENT_UPDATE_REQ,
    payload: id,
})
const employeeRelAttachmentUpdateSetValues = data => ({
    type: EMPLOYEE_REL_ATTACHMENT_UPDATE_SET_VALUES,
    payload: data,
})
const employeeRelAttachmentUpdatePutReq = data => ({
    type: EMPLOYEE_REL_ATTACHMENT_UPDATE_PUT_REQ,
    payload: data,
})
const employeeRelAttachmentDelete = id => ({
    type: EMPLOYEE_REL_ATTACHMENT_DELETE,
    payload: id,
})
const employeeRelAttachmentMessage = message => ({
    type: EMPLOYEE_REL_ATTACHMENT_MESSAGE,
    payload: message,
})
const employeeRelAttachmentMessageClear = () => ({
    type: EMPLOYEE_REL_ATTACHMENT_MESSAGE_CLEAR,
})
const addPagination = pagination => ({
    type: ADD_PAGINATION,
    payload: pagination,
})
const pushEmployeeRelAttachment = data => ({
    type: PUSH_EMPLOYEE_REL_ATTACHMENT,
    payload: data,
})

const employeeRelAttachmentCreatePostReq = data => ({
    type: EMPLOYEE_REL_ATTACHMENT_CREATE_POST_REQ,
    payload: data,
})
export {
    setLoading,
    addPagination,
    pushEmployeeRelAttachment,
    setFormLoading,
    employeeRelAttachmentDelete,
    employeeRelAttachmentMessage,
    fetchEmployeeRelAttachmentReq,
    employeeRelAttachmentUpdateReq,
    employeeRelAttachmentIsUpdating,
    employeeRelAttachmentMessageClear,
    employeeRelAttachmentCreatePostReq,
    employeeRelAttachmentUpdatePutReq,
    employeeRelAttachmentUpdateSetValues
}
