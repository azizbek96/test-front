import type { Reducer } from 'redux'

import {
	USER_PERMISSIONS_API_ERROR,
	USER_PERMISSIONS_MODAL_SET,
	USER_PERMISSIONS_CREATE_END,
	USER_PERMISSIONS_DELETE_END,
	USER_PERMISSIONS_UPDATE_END,
	USER_PERMISSIONS_CATEGORY_SET,
	USER_PERMISSIONS_DELETE_START,
	USER_PERMISSIONS_PAGINATION_SET,
	USER_PERMISSIONS_PERMISSIONS_SET,
	USER_PERMISSIONS_FORM_LOADING_SET,
	USER_PERMISSIONS_TABLE_LOADING_SET,
	USER_PERMISSIONS_UPDATING_VALUES_SET,
} from './actionTypes'
import { IUserPermissions } from './index'
import type { IAction } from './actions'

const initialState: IUserPermissions = {
	category: [],
	modal: false,
	pagination: {},
	permissionsRole: {},
	apiError: null,
	permissions: [],
	deletingRow: null,
	formLoading: false,
	updatingValues: {},
	tableLoading: false,
}

const reducer: Reducer<IUserPermissions, IAction> = (state = initialState, { type, payload }): IUserPermissions => {
	switch (type) {
		case USER_PERMISSIONS_CREATE_END:
			return { ...state, modal: false }
		case USER_PERMISSIONS_MODAL_SET:
			return { ...state, modal: payload }
		case USER_PERMISSIONS_CATEGORY_SET:
			return { ...state, category: payload }
		case USER_PERMISSIONS_API_ERROR:
			return { ...state, apiError: payload }
		case USER_PERMISSIONS_PAGINATION_SET:
			return { ...state, pagination: payload }
		case USER_PERMISSIONS_PERMISSIONS_SET:
			return { ...state, permissions: payload.data, permissionsRole: payload.permissions }
		case USER_PERMISSIONS_DELETE_START:
			return { ...state, deletingRow: payload }
		case USER_PERMISSIONS_FORM_LOADING_SET:
			return { ...state, formLoading: payload }
		case USER_PERMISSIONS_TABLE_LOADING_SET:
			return { ...state, tableLoading: payload }
		case USER_PERMISSIONS_UPDATING_VALUES_SET:
			return { ...state, updatingValues: payload }
		case USER_PERMISSIONS_DELETE_END:
			return { ...state, deletingRow: initialState.deletingRow }
		case USER_PERMISSIONS_UPDATE_END:
			return {
				...state,
				modal: false,
				updatingValues: initialState.updatingValues,
			}
		default:
			return state
	}
}

export default reducer
