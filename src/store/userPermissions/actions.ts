import { values } from 'lodash'

import {
	USER_PERMISSIONS_FETCH,
	USER_PERMISSIONS_MODAL_SET,
	USER_PERMISSIONS_API_ERROR,
	USER_PERMISSIONS_CREATE_END,
	USER_PERMISSIONS_DELETE_END,
	USER_PERMISSIONS_UPDATE_END,
	USER_PERMISSIONS_CREATE_SEND,
	USER_PERMISSIONS_UPDATE_SEND,
	USER_PERMISSIONS_CREATE_START,
	USER_PERMISSIONS_CATEGORY_SET,
	USER_PERMISSIONS_DELETE_START,
	USER_PERMISSIONS_UPDATE_START,
	USER_PERMISSIONS_PAGINATION_SET,
	USER_PERMISSIONS_PERMISSIONS_SET,
	USER_PERMISSIONS_FORM_LOADING_SET,
	USER_PERMISSIONS_TABLE_LOADING_SET,
	USER_PERMISSIONS_UPDATING_VALUES_SET,
} from './actionTypes'
import { IPermissionsForm } from '../../pages/UserPermissions/types'
import { IPagination } from './index'

interface IAction<P = any> {
	type: string
	payload?: P
}

type UPFT = number | 'all'

const userPermissionsFetchAction = (page: UPFT): IAction<UPFT> => ({
	type: USER_PERMISSIONS_FETCH,
	payload: page,
})

const userPermissionPaginationSetAction = (pagination: IPagination): IAction<IPagination> => ({
	type: USER_PERMISSIONS_PAGINATION_SET,
	payload: pagination,
})

const userPermissionsPermissionsSetAction = (permission: unknown): IAction<unknown> => ({
	type: USER_PERMISSIONS_PERMISSIONS_SET,
	payload: permission,
})

const userPermissionsApiErrorAction = (error: string): IAction<string> => ({
	type: USER_PERMISSIONS_API_ERROR,
	payload: error,
})

const userPermissionsFormLoadingSetAction = (loading: boolean): IAction<boolean> => ({
	type: USER_PERMISSIONS_FORM_LOADING_SET,
	payload: loading,
})

const userPermissionsTableLoadingSetAction = (loading: boolean): IAction => ({
	type: USER_PERMISSIONS_TABLE_LOADING_SET,
	payload: loading,
})

const userPermissionsModalSetAction = (open: boolean): IAction<boolean> => ({
	type: USER_PERMISSIONS_MODAL_SET,
	payload: open,
})

const userPermissionsCreateStartAction = (): IAction<undefined> => ({
	type: USER_PERMISSIONS_CREATE_START,
})

const userPermissionsCreateEndAction = (): IAction<undefined> => ({
	type: USER_PERMISSIONS_CREATE_END,
})

const userPermissionsCreateSendAction = (values: object): IAction<object> => ({
	type: USER_PERMISSIONS_CREATE_SEND,
	payload: values,
})

const userPermissionsCategorySetAction = (categories: []): IAction<[]> => ({
	type: USER_PERMISSIONS_CATEGORY_SET,
	payload: categories,
})

const userPermissionsDeleteStartAction = (name: string): IAction<string> => ({
	type: USER_PERMISSIONS_DELETE_START,
	payload: name,
})

const userPermissionsDeleteEndAction = (): IAction<undefined> => ({
	type: USER_PERMISSIONS_DELETE_END,
})

const userPermissionsUpdateStartAction = (name: string): IAction<string> => ({
	type: USER_PERMISSIONS_UPDATE_START,
	payload: name,
})

const userPermissionsUpdateEndAction = (): IAction<undefined> => ({
	type: USER_PERMISSIONS_UPDATE_END,
})

const userPermissionsUpdateSendAction = (values: IPermissionsForm): IAction<IPermissionsForm> => ({
	type: USER_PERMISSIONS_UPDATE_SEND,
	payload: values,
})

const userPermissionsUpdatingValuesSetAction = (values: IPermissionsForm): IAction<IPermissionsForm> => ({
	type: USER_PERMISSIONS_UPDATING_VALUES_SET,
	payload: values,
})

export type { UPFT, IAction }
export {
	userPermissionsFetchAction,
	userPermissionsModalSetAction,
	userPermissionsApiErrorAction,
	userPermissionsUpdateEndAction,
	userPermissionsDeleteEndAction,
	userPermissionsCreateEndAction,
	userPermissionsUpdateSendAction,
	userPermissionsCreateSendAction,
	userPermissionsUpdateStartAction,
	userPermissionsCreateStartAction,
	userPermissionsDeleteStartAction,
	userPermissionsCategorySetAction,
	userPermissionPaginationSetAction,
	userPermissionsFormLoadingSetAction,
	userPermissionsPermissionsSetAction,
	userPermissionsTableLoadingSetAction,
	userPermissionsUpdatingValuesSetAction,
}
