import { put, call, select, takeLeading } from 'redux-saga/effects'
import { AxiosResponse } from 'axios'
import { filter } from 'lodash'

import {
	UPFT,
	IAction,
	userPermissionsApiErrorAction,
	userPermissionsModalSetAction,
	userPermissionsCreateEndAction,
	userPermissionsDeleteEndAction,
	userPermissionsUpdateEndAction,
	userPermissionsCategorySetAction,
	userPermissionPaginationSetAction,
	userPermissionsFormLoadingSetAction,
	userPermissionsPermissionsSetAction,
	userPermissionsTableLoadingSetAction,
	userPermissionsUpdatingValuesSetAction,
} from './actions'
import {
	USER_PERMISSIONS_FETCH,
	USER_PERMISSIONS_CREATE_SEND,
	USER_PERMISSIONS_UPDATE_SEND,
	USER_PERMISSIONS_CREATE_START,
	USER_PERMISSIONS_DELETE_START,
	USER_PERMISSIONS_UPDATE_START,
} from './actionTypes'
import { USER_PERMISSION_INDEX_URL, USER_PERMISSION_CREATE_URL, USER_PERMISSION_DELETE_URL, USER_PERMISSION_UPDATE_URL } from '../../helpers/url_helper'
import { IPermissionsForm } from '../../pages/UserPermissions/types'
import { api } from '../../helpers/api_helper'

const fetchRequest = async (page: UPFT): Promise<AxiosResponse> => {
	const url = USER_PERMISSION_INDEX_URL

	if (page !== 'all') {
		url.concat('?page=', page.toString())
	}

	return await api.get(url)
}

function* fetchWorker({ payload: page }: IAction<UPFT>) {
	try {
		yield put(userPermissionsTableLoadingSetAction(true))
		// @ts-ignore
		const { data: response, pagination } = yield call(fetchRequest, page)

		if (response.code === 200) {
			yield put(userPermissionsPermissionsSetAction(response))
			yield put(userPermissionPaginationSetAction(pagination))
		}
	} catch (error: any) {
		yield put(userPermissionsApiErrorAction(error.toString()))
	} finally {
		yield put(userPermissionsTableLoadingSetAction(false))
	}
}

const createStartRequest = async () => await api.get(USER_PERMISSION_CREATE_URL)

function* createStartWorker() {
	try {
		yield put(userPermissionsModalSetAction(true))

		const { data: response } = yield call(createStartRequest)

		if (response.code === 200) {
			yield put(userPermissionsCategorySetAction(response.data.categories))
		}
	} catch (error: any) {
		yield put(userPermissionsApiErrorAction(error.toString()))
	}
}

const createSendRequest = async (values: any) => {
	return await api.post(USER_PERMISSION_CREATE_URL, values)
}

function* createSendWorker({ payload: values }: IAction) {
	try {
		yield put(userPermissionsFormLoadingSetAction(true))

		const { data: response } = yield call(createSendRequest, values)

		if (response.code === 200) {
			yield put(userPermissionsCreateEndAction())
			// @ts-ignore
			const permissions = yield select(s => ({data: [...response.data, ...s.UserPermissions.permissions], permissions: s.UserPermissions.permissionsRole}))
			yield put(userPermissionsPermissionsSetAction(permissions))
		}
	} catch (error: any) {
		yield put(userPermissionsApiErrorAction(error.toString()))
	} finally {
		yield put(userPermissionsFormLoadingSetAction(false))
	}
}

const deleteStartRequest = async (name: string): Promise<AxiosResponse> => {
	return await api.delete(`${USER_PERMISSION_DELETE_URL}?name=${name}`)
}

function* deleteStartWorker({ payload: name }: IAction<string>) {
	try {
		// @ts-ignore
		const { data: response } = yield call(deleteStartRequest, name)

		if (response.code === 200) {
			yield put(userPermissionsDeleteEndAction())

			// @ts-ignore
			const permissions = yield select(s => filter(s.UserPermissions.permissions, p => p.name !== name))

			yield put(userPermissionsPermissionsSetAction(permissions))
		}
	} catch (error: any) {
		yield put(userPermissionsApiErrorAction(error.toString()))
	}
}

const updateStartRequest = async (name: string) => {
	return await api.get(`${USER_PERMISSION_UPDATE_URL}?name=${name}`)
}

function* updateStartWorker({ payload: name }: IAction<string>) {
	try {
		yield put(userPermissionsFormLoadingSetAction(true))
		yield put(userPermissionsModalSetAction(true))

		// @ts-ignore
		const { data: response } = yield call(updateStartRequest, name)

		if (response.code === 200) {
			yield put(userPermissionsUpdatingValuesSetAction(response.data.values))
			yield put(userPermissionsCategorySetAction(response.data.categories))
		}
	} catch (error: any) {
		yield put(userPermissionsApiErrorAction(error.toString()))
	} finally {
		yield put(userPermissionsFormLoadingSetAction(false))
	}
}

const updateSendRequest = async (values: IPermissionsForm, name: string) => {
	return await api.put(`${USER_PERMISSION_UPDATE_URL}?name=${name}`, values)
}

function* updateSendWorker({ payload: values }: IAction<IPermissionsForm>) {
	try {
		yield put(userPermissionsFormLoadingSetAction(true))

		const { data: response } = yield call(
			// @ts-ignore
			updateSendRequest,
			values,
			yield select(s => s.UserPermissions.updatingValues.name)
		)

		if (response.code === 200) {
			yield put(userPermissionsUpdateEndAction())
			yield put(userPermissionsPermissionsSetAction(response))
		}
	} catch (error: any) {
		yield put(userPermissionsApiErrorAction(error.toString()))
	} finally {
		yield put(userPermissionsFormLoadingSetAction(false))
	}
}

function* userPermissionSaga() {
	yield takeLeading(USER_PERMISSIONS_FETCH, fetchWorker)
	yield takeLeading(USER_PERMISSIONS_CREATE_SEND, createSendWorker)
	yield takeLeading(USER_PERMISSIONS_UPDATE_SEND, updateSendWorker)
	yield takeLeading(USER_PERMISSIONS_CREATE_START, createStartWorker)
	yield takeLeading(USER_PERMISSIONS_UPDATE_START, updateStartWorker)
	yield takeLeading(USER_PERMISSIONS_DELETE_START, deleteStartWorker)
}

export default userPermissionSaga
