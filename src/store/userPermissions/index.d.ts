import { IPermissionsForm } from '../../pages/UserPermissions/types'

type IPagination = {
	page: number
	pageCount: number
	totalSize: number
	sizePerPage: number
}
type IPermissionsRole = {
	create: boolean
	update: boolean
	delete: boolean
	view: boolean
}

interface IUserPermissions {
	modal: boolean
	formLoading: boolean
	tableLoading: boolean
	category: Array<object>
	apiError: string | null
	permissions: Array<object>
	deletingRow: string | null
	pagination: IPagination | object
	permissionsRole: IPermissionsRole | object
	updatingValues: IPermissionsForm | object
}

export type { IPagination, IUserPermissions ,IPermissionsRole}
