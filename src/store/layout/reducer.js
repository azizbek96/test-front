// @flow
//constants

import {
	SHOW_SIDEBAR,
	CHANGE_LAYOUT,
	TOGGLE_LEFTMENU,
	CHANGE_PRELOADER,
	SHOW_RIGHT_SIDEBAR,
	CHANGE_LAYOUT_WIDTH,
	CHANGE_SIDEBAR_TYPE,
	CHANGE_TOPBAR_THEME,
	CHANGE_SIDEBAR_THEME,
	CHANGE_SIDEBAR_THEME_IMAGE,
} from './actionTypes'
import { layoutTypes, layoutWidthTypes, leftSidebarTypes, topBarThemeTypes, leftSideBarThemeTypes, leftBarThemeImageTypes } from '../../constants/layout'

const INIT_STATE = {
	isMobile: false,
	leftMenu: false,
	showSidebar: true,
	isPreloader: false,
	showRightSidebar: false,
	layoutType: layoutTypes.VERTICAL,
	layoutWidth: layoutWidthTypes.FLUID,
	topbarTheme: topBarThemeTypes.LIGHT,
	leftSideBarType: leftSidebarTypes.DEFAULT,
	leftSideBarTheme: leftSideBarThemeTypes.LIGHT,
	leftSideBarThemeImage: leftBarThemeImageTypes.NONE,
}

const Layout = (state = INIT_STATE, action) => {
	switch (action.type) {
		case CHANGE_LAYOUT:
			return {
				...state,
				layoutType: action.payload,
			}
		case CHANGE_PRELOADER:
			return {
				...state,
				isPreloader: action.payload,
			}

		case CHANGE_LAYOUT_WIDTH:
			return {
				...state,
				layoutWidth: action.payload,
			}
		case CHANGE_SIDEBAR_THEME:
			return {
				...state,
				leftSideBarTheme: action.payload,
			}
		case CHANGE_SIDEBAR_THEME_IMAGE:
			return {
				...state,
				leftSideBarThemeImage: action.payload,
			}
		case CHANGE_SIDEBAR_TYPE:
			return {
				...state,
				leftSideBarType: action.payload.sidebarType,
			}
		case CHANGE_TOPBAR_THEME:
			return {
				...state,
				topbarTheme: action.payload,
			}
		case SHOW_RIGHT_SIDEBAR:
			return {
				...state,
				showRightSidebar: action.payload,
			}
		case SHOW_SIDEBAR:
			return {
				...state,
				showSidebar: action.payload,
			}
		case TOGGLE_LEFTMENU:
			return {
				...state,
				leftMenu: action.payload,
			}

		default:
			return state
	}
}

export default Layout
