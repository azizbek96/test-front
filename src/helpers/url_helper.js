import { Url } from '../utils/url'

const API = 'api'

// const url = new Url(['auth', 'login'])
//
// url.appendParams({ id: 5 })

/**
 * Auth
 */
const LOGIN = Url.join(API, 'auth/login')
const ADMIN_LOGIN = Url.join(API, 'auth/admin-login')
const LOGOUT = Url.join(API, 'auth/refresh-token')
const AUTH_URL = Url.join(API, 'auth/refresh-token')

/**
 * Users
 */
export const USERS_INDEX_URL = API + '/users/index'
export const USERS_CREATE_URL = API + '/users/create'
export const USERS_UPDATE_URL = API + '/users/update'
export const USERS_DELETE_URL = API + '/users/delete'

/**
 * Employee
 */
const FETCH_EMPLOYEES_URL = API + '/employee/index'
const CREATE_EMPLOYEES_URL = API + '/employee/create'
const EMPLOYEES_DELETE_URL = API + '/employee/delete'
const EMPLOYEES_UPDATE_URL = API + '/employee/update'
const FETCH_POSITION_LIST_URL = API + '/employee/create'
const EMPLOYEES_DETAIL_URL = Url.join(API, 'employee/detail')

/**
 * file uploaded
 */
const FETCH_EMPLOYEE_REL_ATTACHMENT_URL = API + '/employee-rel-attachment/index'
const CREATE_EMPLOYEE_REL_ATTACHMENT_URL = API + '/employee-rel-attachment/create'
const EMPLOYEE_REL_ATTACHMENT_DELETE_URL = API + '/employee-rel-attachment/delete'
const EMPLOYEE_REL_ATTACHMENT_UPDATE_URL = API + '/employee-rel-attachment/update'
/**
 * UserRoles
 */

const USER_ROLE_INDEX_URL = API + '/auth-item/index-role'
const USER_ROLE_CREATE_URL = API + '/auth-item/create-role'
const USER_ROLE_UPDATE_URL = API + '/auth-item/update-role'
const USER_ROLE_VIEW_URL = API + '/auth-item/view-role'
const USER_ROLE_DELETE_URL = API + '/auth-item/delete'
const USER_ROLE_SEARCH_URL = API + '/auth-item/search'
const USER_ROLE_MENU_URL = API + '/auth-item/menu'
// const USER_ROLE_LIST_URL = API + '/auth-item/list'

/**
 * UserPermissions
 */

const USER_PERMISSION_DELETE_URL = Url.join(API, 'auth-item/delete')
const USER_PERMISSION_INDEX_URL = Url.join(API, 'auth-item/index-permission')
const USER_PERMISSION_CREATE_URL = Url.join(API, 'auth-item/create-permission')
const USER_PERMISSION_UPDATE_URL = Url.join(API, 'auth-item/update-permission')


export {
	LOGIN,
	ADMIN_LOGIN,
	LOGOUT,
	AUTH_URL,
	FETCH_EMPLOYEES_URL,
	CREATE_EMPLOYEES_URL,
	EMPLOYEES_DELETE_URL,
	EMPLOYEES_UPDATE_URL,
	USER_ROLE_VIEW_URL,
	USER_ROLE_MENU_URL,
	USER_ROLE_INDEX_URL,
	FETCH_POSITION_LIST_URL,
	USER_ROLE_CREATE_URL,
	USER_ROLE_UPDATE_URL,
	USER_ROLE_SEARCH_URL,
	USER_ROLE_DELETE_URL,
	EMPLOYEES_DETAIL_URL,
	USER_PERMISSION_INDEX_URL,
	USER_PERMISSION_CREATE_URL,
	USER_PERMISSION_DELETE_URL,
	USER_PERMISSION_UPDATE_URL,
	FETCH_EMPLOYEE_REL_ATTACHMENT_URL,
	CREATE_EMPLOYEE_REL_ATTACHMENT_URL,
	EMPLOYEE_REL_ATTACHMENT_DELETE_URL,
	EMPLOYEE_REL_ATTACHMENT_UPDATE_URL
}
