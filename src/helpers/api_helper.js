import axios from 'axios'

import { appMenuUpdateAction } from '../store/app/actions'
import store from '../store'

const API_URL = process.env.API_URL

const axiosApi = axios.create({
	baseURL: API_URL,
})

// const paginationRegExp =
// 	/^X-Pagination-(Current-Page|Page-Count|Per-Page|Total-Count)$/i

axiosApi.interceptors.response.use(
	res => {
		if (res.data.pagination) {
			res.pagination = res.data.pagination
		}

		if (res.data.menu) {
			store.dispatch(appMenuUpdateAction(res.data.menu))
		}

		return res
	},
	error => Promise.reject(error)
)

const api = {
	get _token() {
		return 'Bearer ' + localStorage.getItem('token')
	},
	/**
	 *
	 * @param {string} url
	 * @param {boolean} token
	 * @param {import('axios').AxiosRequestConfig} config
	 * @returns {Promise<import('axios').AxiosResponse<any>>}
	 */
	async get(url, token = true, config = {}) {
		if (token) (config.headers = {}).Authorization = this._token

		return axiosApi.get(url, config)
	},
	/**
	 *
	 * @param {string} url
	 * @param {boolean} token
	 * @param {any} body
	 * @param {import('axios').AxiosRequestConfig} config
	 * @returns {Promise<import('axios').AxiosResponse<any>>}
	 */
	async post(url, body, token = true, config = {}) {
		if (token) (config.headers = {}).Authorization = this._token

		return axiosApi.post(url, body, config)
	},

	/**
	 *
	 * @param {string} url
	 * @param {any} body
	 * @param {boolean} token
	 * @param {import('axios').AxiosRequestConfig} config
	 * @returns {Promise<import('axios').AxiosResponse<any>>}
	 */
	async put(url, body, token = true, config = {}) {
		if (token) (config.headers = {}).Authorization = this._token

		return axiosApi.put(url, body, config)
	},

	/**
	 *
	 * @param {string} url
	 * @param {boolean} token
	 * @param {import('axios').AxiosRequestConfig} config
	 * @returns {Promise<import('axios').AxiosResponse<any>>}
	 */
	async delete(url, token = true, config = {}) {
		if (token) (config.headers = {}).Authorization = this._token

		return axiosApi.delete(url, config)
	},
}

export { api, API_URL }
