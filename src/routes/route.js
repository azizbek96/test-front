import { Route, Redirect, useLocation } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

import { employeesCreateStartAction } from '../store/actions'

const AuthMiddleware = ({ component: Component, isAuthProtected, layout: Layout, isAuth, ...rest }) => {
	const location = useLocation()
	const dispatch = useDispatch()

	useEffect(() => {
		if (isAuth) {
			if (location.pathname === '/employees/create') {
				dispatch(employeesCreateStartAction())
			}
		}
	}, [location])

	return (
		<Route
			{...rest}
			render={props => {
				if (isAuthProtected && !isAuth) {
					return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
				}

				return (
					<Layout>
						<Component {...props} />
					</Layout>
				)
			}}
		/>
	)
}

AuthMiddleware.propTypes = {
	layout: PropTypes.any,
	isAuth: PropTypes.bool,
	component: PropTypes.any,
	location: PropTypes.object,
	isAuthProtected: PropTypes.bool,
}

export default AuthMiddleware
