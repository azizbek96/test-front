import { Redirect } from 'react-router-dom'
import loadable from '@loadable/component'
import React from 'react'


const UserPermissions = loadable(() => import('../pages/UserPermissions'))
const Logout = loadable(() => import('../pages/Authentication/Logout'))
const Login = loadable(() => import('../pages/Authentication/Login'))
const Dashboard = loadable(() => import('../pages/Dashboard'))
const UserRoles = loadable(() => import('../pages/UserRoles'))
const UserRolesPermissions = loadable(() => import('../pages/UserRoles/permissionWith'))
const Users = loadable(() => import('../pages/Users'))

const Employees = loadable(() => import('../pages/Employees'))
const EmployeesModal = loadable(() => import('../pages/Employees/employeesModal'))

const EmployeeRelAttachment = loadable(() => import('../pages/EmployeeRelAttachment'))
const EmployeeRelAttachmentCreate = loadable(() => import('../pages/EmployeeRelAttachment/create'))


/**
 * @type {import('react-router-dom').RouteProps[]}
 */
const authProtectedRoutes = [
	{ path: '/logout', component: Logout },

	{ path: '/users', component: Users },
	{ path: '/user-roles', component: UserRoles },
	{ path: '/user-roles/update-per', component: UserRolesPermissions },
	{ path: '/user-permissions', component: UserPermissions },
	{ path: '/employees', component: Employees },
	{ path: '/employees/modal', component: EmployeesModal },
	{ path: '/dashboard', component: Dashboard },
	{ path: '/employee-rel-attachment', component: EmployeeRelAttachment },
	{ path: '/employee-rel-attachment/create', component: EmployeeRelAttachmentCreate },

	{ path: '*', exact: true, component: () => <Redirect to='/dashboard' /> },

]

/**
 * @type {import('react-router-dom').RouteProps[]}
 */
const publicRoutes = [
	{ path: '/login', component: Login },
	// eslint-disable-next-line react/display-name
	{ path: '*', exact: true, component: () => <Redirect to='/login' /> },
]

export { publicRoutes, authProtectedRoutes }
