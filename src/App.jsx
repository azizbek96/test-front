import { Switch, BrowserRouter as Router } from 'react-router-dom'
import React, { memo, useMemo, useLayoutEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import _ from 'lodash'

import { publicRoutes, authProtectedRoutes } from './routes'
import NonAuthLayout from './components/NonAuthLayout'
import { authUser, appMenuGet } from './store/actions'
import ApiToast from './components/Toasts/ApiToast'
import Loader from './components/Loaders/Loader'
import AuthMiddleware from './routes/route'
import Layout from './components/Layout'
import './assets/scss/theme.scss'
import './utils'

const App = () => {
	const dispatch = useDispatch()

	useLayoutEffect(() => {
		dispatch(authUser())
		dispatch(appMenuGet())
	}, [])

	const { isAuth, isLoading, apiErrors } = useSelector(s => ({
		isAuth: s.App.isAuth,
		isLoading: s.App.isLoading,
		apiErrors: [],
	}))

	const apiError = useMemo(() => apiErrors.filter(f => !_.isEmpty(f)), [apiErrors])

	return (
		<>
			<ApiToast errors={apiError} />
			{/*<FormSuccessToast messages={formMessages} />*/}
			<Router>
				<Switch>
					{isLoading && <Loader />}
					{isAuth
						? authProtectedRoutes.map((route, idx) => (
								<AuthMiddleware exact key={idx} isAuth={isAuth} layout={Layout} path={route.path} isAuthProtected={true} component={route.component} />
						  ))
						: publicRoutes.map((route, idx) => (
								<AuthMiddleware exact key={idx} isAuth={isAuth} path={route.path} layout={NonAuthLayout} isAuthProtected={false} component={route.component} />
						  ))}
				</Switch>
			</Router>
		</>
	)
}

export default memo(App)
