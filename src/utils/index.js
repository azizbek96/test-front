/**
 *
 * @param {string[]} keys
 * @param {boolean} not
 * @returns {any}
 */
const getInKeysArray = (obj, keys, not = false) => {
	const r = {}
	Object.keys(obj).forEach(_ => {
		const result = keys.includes(_)

		if (!not ? result : !result) {
			r[_] = obj[_]
		}
	})

	return r
}

Array.prototype.deepFind = function (deepKey, cb) {
	let found

	this.some(function someCb(elem) {
		if (cb(elem)) {
			found = elem
			return true
		}

		return Array.isArray(elem[deepKey]) && elem[deepKey].some(someCb)
	})

	return found
}

Array.prototype.deepEach = function (deepKey, cb) {
	this.some(function someCb(elem) {
		cb(elem)
		return Array.isArray(elem[deepKey]) && elem[deepKey].some(someCb)
	})
}

Array.prototype.generatePath = function (deepKey, cb) {
	const path = []
	for (const item of this) {
		const res = cb(item)

		if (res) {
			break
		}

		if (item[deepKey]) {
			const child = item[deepKey].generatePath(deepKey, cb)
			console.log(child)
			if (child?.length) path.push(child)
		}
	}

	return path
}

const getNestedPath = (arr, name) => {
	const array = []
	for (const item of arr) {
		if (item.name !== name) break
		if (item.subcategories) {
			const child = getNestedPath(item.subcategories, name)
			if (child) return `/${item.name}${child}`
		}
	}
}

const getArrayDepth = value => {
	return Array.isArray(value) ? Math.max(...value.map(getArrayDepth), 0) + 1 : 0
}

/**
 * @param {string} str
 * @return {boolean}
 */
const isBase64 = str => {
	return str?.length % 4 === 0 && /^[A-Za-z\d+/]+={0,2}$/.test(str)
}

/**
 * 
 * @param {number} bytes 
 * @param {number} decimals 
 * @returns 
 */
const formatBytes = (bytes, decimals = 2) => {
	if (bytes === 0) return '0 Bytes'

	const k = 1024
	const dm = decimals < 0 ? 0 : decimals
	const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

	const i = Math.floor(Math.log(bytes) / Math.log(k))

	return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
}

export { isBase64, getArrayDepth, getInKeysArray, formatBytes }
