import { isEmpty } from 'lodash'

import { API_URL } from '../helpers/api_helper'

class Url extends String {
	#url = ''
	#base = ''
	#params = {}

	/**
	 *
	 * @param {string[]} chunks
	 */
	constructor(chunks) {
		const joined = Url.join(...chunks)
		super(joined)
		this.#base = joined
		new URL(this.toString())
	}

	/**
	 *
	 * @param {string} chunks
	 * @return {string}
	 */
	static join(...chunks) {
		return chunks
			.map(chunk => {
				return chunk.replace(/^\/+|\/+$/g, '')
			})
			.join('/')
	}

	/**
	 *
	 * @param {Object} params
	 */
	appendParams(params) {
		const url = new URLSearchParams()

		Object.keys(params).forEach(key => {
			url.append(key, params[key])
		})

		console.log(decodeURIComponent(url.toString()))
	}

	static buildUrl(url, params) {
		const urlObj = new URL(url, API_URL)

		Object.keys(params).forEach(key => {
			if (!isEmpty(params[key]?.toString())) {
				urlObj.searchParams.append(key, params[key])
			}
		})

		return urlObj.toString()
	}
}

export { Url }
