const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const ESLintWebpackPlugin = require('eslint-webpack-plugin')
const { ProvidePlugin, ProgressPlugin } = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const { resolve, join, sep } = require('path')
const Dotenv = require('dotenv-webpack')

function getPackageName(modulePath, packageFolder = 'node_modules') {
	if (typeof modulePath === 'string' && modulePath.includes(packageFolder)) {
		const segments = modulePath.split(sep)
		const index = segments.lastIndexOf(packageFolder)

		if (index > -1) {
			const name = segments[index + 1] || ''
			const scopedName = segments[index + 2] || ''

			if (name[0] === '@') {
				scopedName.replace('@', '')
				return scopedName ? `${name}/${scopedName}` : undefined
			}

			if (name) {
				return name
			}
		}
	}
}

/**
 * @type {import('webpack').Configuration}
 */
const config = {
	name: 'index',
	mode: 'production',
	entry: './index.js',
	devtool: 'hidden-source-map',
	context: resolve('src'),
	module: {
		rules: [
			{
				test: /\.(ts|tsx|js|jsx)$/,
				use: {
					loader: 'babel-loader',
					options: {
						plugins: ['@babel/transform-runtime'],
						presets: [
							[
								'@babel/preset-env',
								{
									debug: true,
									loose: true,
									modules: false,
									targets: {
										node: 'current',
										browsers: 'chrome >= 80',
									},
								},
							],
							'@babel/preset-react',
							'@babel/preset-typescript',
						],
					},
				},
			},
			{
				test: /\.s?css$/,
				use: [
					{ loader: MiniCssExtractPlugin.loader },
					{
						loader: 'css-loader',
					},
					{ loader: 'resolve-url-loader' },
					{ loader: 'sass-loader' },
				],
			},
			{
				test: /\.(png|svg|jpeg|jpg)$/,
				type: 'asset/resource',
				generator: {
					filename: 'images/[hash][ext]',
				},
			},
			{
				test: /fonts.+\.(eot|woff|woff2|ttf|svg)$/,
				type: 'asset/resource',
				generator: {
					filename: 'fonts/[hash][ext]',
				},
			},
		],
	},
	resolve: {
		mainFiles: ['index'],
		enforceExtension: false,
		extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
	},
	output: {
		publicPath: '/',
		path: resolve('build'),
		filename: 'js/bundle.[contenthash:8].js',
		chunkFilename: 'js/[name].[contenthash:8].chunk.js',
	},
	plugins: [
		new Dotenv({ path: './.env.production' }),
		new HtmlWebpackPlugin({
			inject: 'body',
			template: resolve('public/index.html'),
			minify: {
				minifyJS: true,
				minifyCSS: true,
				minifyURLs: true,
				removeComments: true,
				useShortDoctype: true,
				keepClosingSlash: true,
				collapseWhitespace: true,
				removeEmptyAttributes: true,
				removeRedundantAttributes: true,
				removeStyleLinkTypeAttributes: true,
			},
		}),
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: 'css/[name].[contenthash:8].css',
			chunkFilename: 'css/[name].[contenthash:8].chunk.css',
		}),
		new ProvidePlugin({
			React: 'react',
		}),
		new ProgressPlugin({ percentBy: 'dependencies', profile: true }),
		process.argv.includes('--analyze') && new BundleAnalyzerPlugin(),
		new ForkTsCheckerWebpackPlugin({
			typescript: {
				configFile: resolve('tsconfig.json'),
			},
		}),
		new ESLintWebpackPlugin({
			useEslintrc: true,
			extensions: ['js', 'jsx', 'ts', 'tsx'],
		}),
	].filter(Boolean),
	optimization: {
		minimize: true,
		minimizer: [
			new CssMinimizerPlugin(),
			new TerserPlugin({
				parallel: true,
				extractComments: false,
				terserOptions: {
					ecma: 2020,
					ie8: false,
					sourceMap: false,
					format: {
						comments: false,
					},
				},
			}),
		],
		splitChunks: {
			minSize: 0,
			chunks: 'all',
			usedExports: true,
			maxInitialRequests: Infinity,
			filename: 'js/[name].[contenthash:8].js',
			cacheGroups: {
				react: {
					priority: 100,
					name: 'react',
					test: /node_modules\/(react-dom|react)\//,
				},
				reactRouter: {
					priority: 99,
					name: 'react-router',
					test: /node_modules\/(react-router|react-router-dom)\//,
				},
				reduxSaga: {
					priority: 98,
					name: 'redux-saga',
					test: /node_modules\/@redux-saga\//,
				},
				reactstrap: {
					priority: 97,
					name: 'reactstrap',
					test: /node_modules\/reactstrap\//,
				},
				reactBootstrapTable: {
					priority: 96,
					name: 'react-bootstrap-table',
					test: /node_modules\/react-bootstrap-table(-next|2-editor|2-overlay|2-paginator|2-toolkit)\//,
				},
				lodash: {
					priority: 95,
					name: 'lodash',
					test: /node_modules\/lodash\//,
				},
				dateFns: {
					priority: 94,
					name: 'date-fns',
					test: /node_modules\/date-fns\//,
				},
				moment: {
					priority: 93,
					name: 'moment',
					test: /node_modules\/moment\//,
				},
				framerMotion: {
					priority: 92,
					name: 'framer-motion',
					test: /node_modules\/framer-motion\//,
				},
				src: {
					priority: 91,
					name: 'source',
					test: /src/,
				},
				reactPackages: {
					priority: 90,
					test: /node_modules\/react-/,
					name: 'react-packages',
				},
				default: {
					name: 'chunk',
					test: /node_modules/,
					reuseExistingChunk: true,
				},
			},
		},
	},
}

module.exports = config
