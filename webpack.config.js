const { ProvidePlugin, ProgressPlugin, HotModuleReplacementPlugin } = require('webpack')
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const CleanTerminalPlugin = require('clean-terminal-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ESLintPlugin = require('eslint-webpack-plugin')
const { resolve } = require('path')
const Dotenv = require('dotenv-webpack')

const devServer = {
	port: 7000,
	compress: true,
	historyApiFallback: true,
	client: {
		overlay: {
			warnings: false,
			errors: true,
		},
		logging: 'none',
		reconnect: false,
	},
	devMiddleware: {
		writeToDisk: true,
	},
}

/**
 * @type {import('webpack').Configuration}
 */
const index = {
	devServer,
	cache: true,
	name: 'index',
	mode: 'development',
	entry: './index.js',
	devtool: 'source-map',
	context: resolve('src'),
	module: {
		rules: [
			{
				test: /\.(ts|tsx|js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [
							[
								'@babel/preset-env',
								{
									debug: true,
									loose: true,
									modules: false,
									targets: {
										browsers: 'chrome >= 95',
										node: 'current',
									},
								},
							],
							'@babel/preset-react',
							'@babel/preset-typescript',
						],
						plugins: ['react-refresh/babel', ['@babel/transform-runtime', { regenerator: true }]],
					},
				},
			},
			{
				test: /\.s?css$/,
				use: [{ loader: MiniCssExtractPlugin.loader }, { loader: 'css-loader' }, { loader: 'resolve-url-loader' }, { loader: 'sass-loader' }],
			},
			{
				test: /\.(png|svg|jpeg|jpg)$/,
				type: 'asset/resource',
				generator: {
					filename: 'images/[hash][ext]',
				},
			},
			{
				test: /fonts.+\.(eot|woff|woff2|ttf|svg)$/,
				type: 'asset/resource',
				generator: {
					filename: 'fonts/[hash][ext]',
				},
			},
		],
	},
	resolve: {
		mainFiles: ['index'],
		enforceExtension: false,
		extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
	},
	output: {
		publicPath: '/',
		path: resolve('dist'),
		filename: 'js/bundle.[contenthash:8].js',
	},
	plugins: [
		new Dotenv({ path: './.env' }),
		new HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			inject: 'body',
			template: resolve('public/index.html'),
		}),
		new MiniCssExtractPlugin({
			filename: 'css/style.[contenthash:8].css',
			chunkFilename: 'css/style.chunk.[contenthash:8].css',
		}),
		new CleanWebpackPlugin({ verbose: true }),
		new CleanTerminalPlugin({
			skipFirstRun: true,
			beforeCompile: true,
		}),
		new ProvidePlugin({
			React: 'react',
		}),
		new ReactRefreshWebpackPlugin({ esModule: true }),
		new ProgressPlugin({ profile: true }),
		process.argv.includes('--analyze') && new BundleAnalyzerPlugin(),
		new ForkTsCheckerWebpackPlugin({
			typescript: {
				configFile: resolve('tsconfig.json'),
			},
		}),
		new ESLintPlugin({
			useEslintrc: true,
			extensions: ['js', 'jsx', 'ts', 'tsx'],
		}),
	].filter(Boolean),
	optimization: {
		usedExports: true,
		concatenateModules: true,
		mergeDuplicateChunks: true,
		removeAvailableModules: true,
		splitChunks: {
			minChunks: 1,
			minSize: 20000,
			chunks: 'async',
			minRemainingSize: 0,
			maxAsyncRequests: 30,
			maxInitialRequests: 30,
			enforceSizeThreshold: 50000,
			filename: 'js/chunk.[contenthash:8].js',
			cacheGroups: {
				defaultVendors: {
					priority: -10,
					reuseExistingChunk: true,
					test: /[\\/]node_modules[\\/]/,
				},
				default: {
					minChunks: 2,
					priority: -20,
					reuseExistingChunk: true,
				},
			},
		},
	},
}

/**
 * @type {import('webpack').Configuration}
 */
const slow = {
	devServer,
	cache: true,
	name: 'slow',
	mode: 'development',
	entry: './index.js',
	devtool: 'source-map',
	context: resolve('src'),
	module: {
		rules: [
			{
				test: /\.(ts|tsx|js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [
							[
								'@babel/preset-env',
								{
									debug: true,
									loose: true,
									modules: false,
									targets: {
										browsers: 'chrome >= 95',
										node: 'current',
									},
								},
							],
							'@babel/preset-react',
							'@babel/preset-typescript',
						],
						plugins: ['react-refresh/babel', ['@babel/transform-runtime', { regenerator: true }]],
					},
				},
			},
			{
				test: /\.s?css$/,
				use: [{ loader: MiniCssExtractPlugin.loader }, { loader: 'css-loader' }, { loader: 'resolve-url-loader' }, { loader: 'sass-loader' }],
			},
			{
				test: /\.(png|svg|jpeg|jpg)$/,
				type: 'asset/resource',
				generator: {
					filename: 'images/[hash][ext]',
				},
			},
			{
				test: /fonts.+\.(eot|woff|woff2|ttf|svg)$/,
				type: 'asset/resource',
				generator: {
					filename: 'fonts/[hash][ext]',
				},
			},
		],
	},
	resolve: {
		mainFiles: ['index'],
		enforceExtension: false,
		extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
	},
	output: {
		publicPath: '/',
		path: resolve('dist'),
		filename: 'js/bundle.[contenthash:8].js',
	},
	plugins: [
		new Dotenv({ path: './.env' }),
		new HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			inject: 'body',
			template: resolve('public/index.html'),
		}),
		new MiniCssExtractPlugin({
			filename: 'css/style.[contenthash:8].css',
			chunkFilename: 'css/style.chunk.[contenthash:8].css',
		}),
		new CleanWebpackPlugin({ verbose: true }),
		new CleanTerminalPlugin({
			skipFirstRun: true,
			beforeCompile: true,
		}),
		new ProvidePlugin({
			React: 'react',
		}),
		new ReactRefreshWebpackPlugin({ esModule: true }),
		new ProgressPlugin({ profile: true }),
		process.argv.includes('--analyze') && new BundleAnalyzerPlugin(),
	].filter(Boolean),
	optimization: {
		usedExports: true,
		concatenateModules: true,
		mergeDuplicateChunks: true,
		removeAvailableModules: true,
		splitChunks: {
			minChunks: 1,
			minSize: 20000,
			chunks: 'async',
			minRemainingSize: 0,
			maxAsyncRequests: 30,
			maxInitialRequests: 30,
			enforceSizeThreshold: 50000,
			filename: 'js/chunk.[contenthash:8].js',
			cacheGroups: {
				defaultVendors: {
					priority: -10,
					reuseExistingChunk: true,
					test: /[\\/]node_modules[\\/]/,
				},
				default: {
					minChunks: 2,
					priority: -20,
					reuseExistingChunk: true,
				},
			},
		},
	},
}

module.exports.devServer = devServer
module.exports.default = [index, slow]
